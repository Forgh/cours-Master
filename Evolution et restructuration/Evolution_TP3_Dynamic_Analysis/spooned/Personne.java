

// default package (CtPackage.TOP_LEVEL_PACKAGE_NAME in Spoon= unnamed package)



public abstract class Personne {
    public java.lang.String nom;

    public java.lang.String prenom;

    public int numSecu;

    public int statut;

    public Personne conjoint;

    public int nombreEnfants;

    public Personne(java.lang.String nom, java.lang.String prenom, int numSecu, int statut, Personne conjoint, int nombreEnfants) {
        Personne.this.nom = nom;
        Personne.this.prenom = prenom;
        Personne.this.numSecu = numSecu;
        Personne.this.statut = statut;
        Personne.this.conjoint = conjoint;
        Personne.this.nombreEnfants = nombreEnfants;
    }

    public Personne() {
    }
}

