

// default package (CtPackage.TOP_LEVEL_PACKAGE_NAME in Spoon= unnamed package)



public class DonneesHistorique {
    double revenu;

    double fraisDeclares;

    double fraisRetenus;

    double montantImpot;

    java.lang.String remarques;

    public DonneesHistorique(double revenu, double fraisDeclares, double fraisRetenus, double montantImpot, java.lang.String remarque) {
        DonneesHistorique.this.revenu = revenu;
        DonneesHistorique.this.fraisDeclares = fraisDeclares;
        DonneesHistorique.this.fraisRetenus = fraisRetenus;
        DonneesHistorique.this.montantImpot = montantImpot;
        DonneesHistorique.this.remarques = remarque;
    }

    DonneesHistorique() {
    }
}

