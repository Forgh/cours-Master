

// default package (CtPackage.TOP_LEVEL_PACKAGE_NAME in Spoon= unnamed package)



public class Revenu {
    double montant;

    double revenuSalaire;

    double revenuFancier;

    double autresRevenus;

    public Revenu() {
    }

    public Revenu(double revenuSalaire, double revenuFancier, double autresRevenus) {
        Revenu.this.revenuSalaire = revenuSalaire;
        Revenu.this.revenuFancier = revenuFancier;
        Revenu.this.autresRevenus = autresRevenus;
        montant = (((revenuSalaire + (0.7 * revenuFancier)) + (0.85 * autresRevenus)) * 2) / 3;
    }

    public double calculerRevenu() {
        return ((((revenuSalaire) + (0.7 * (revenuFancier))) + (0.85 * (autresRevenus))) * 2) / 3;
        System.out.println("Revenu : calculerRevenu");
    }

    double getMontant() {
        return montant;
        System.out.println("Revenu : getMontant");
    }
}

