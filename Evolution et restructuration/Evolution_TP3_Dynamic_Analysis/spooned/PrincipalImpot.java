

// default package (CtPackage.TOP_LEVEL_PACKAGE_NAME in Spoon= unnamed package)



public class PrincipalImpot {
    public static void main(java.lang.String[] params) {
        Revenu revenu1 = new Revenu(10, 0, 5);
        Imposable impos1 = new Imposable("Dupond", "Bernard", 345, 1, null, 3, revenu1);
        impos1.setFrais(4);
        Impot imp = new Impot();
        double montantImpot = imp.calculerImpot(impos1);
        impos1.enregistrerImpot(montantImpot);
        new FeuilleImpot(2).editer(5);
        java.lang.System.out.println(montantImpot);
        System.out.println("PrincipalImpot : main");
    }
}

