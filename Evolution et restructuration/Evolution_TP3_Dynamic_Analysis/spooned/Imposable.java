

// default package (CtPackage.TOP_LEVEL_PACKAGE_NAME in Spoon= unnamed package)



public class Imposable extends Personne {
    public Revenu revenu;

    public int taux;

    double impotApayer;

    double fraisDeclares;

    DonneesHistorique[] hisorique;

    public Imposable(java.lang.String nom, java.lang.String prenom, int numSecu, int statut, Personne conjoint, int nombreEnfants, Revenu revenu) {
        super(nom, prenom, numSecu, statut, conjoint, nombreEnfants);
        Imposable.this.revenu = revenu;
    }

    public Imposable() {
    }

    int calculerTaux() {
        return 0;
        System.out.println("Imposable : calculerTaux");
    }

    public void enregistrerImpot(double imp) {
        impotApayer = imp;
        System.out.println("Imposable : enregistrerImpot");
    }

    public void setFrais(double frais) {
        Imposable.this.fraisDeclares = frais;
        System.out.println("Imposable : setFrais");
    }

    public double getFraisDeclares() {
        return fraisDeclares;
        System.out.println("Imposable : getFraisDeclares");
    }
}

