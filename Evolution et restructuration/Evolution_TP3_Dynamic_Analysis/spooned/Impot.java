

// default package (CtPackage.TOP_LEVEL_PACKAGE_NAME in Spoon= unnamed package)



public class Impot {
    private double calculerNombreParts(Imposable imps) {
        if ((imps.statut) == 1)
            return (1 + (imps.nombreEnfants)) + 1;
        else
            return 1 + (imps.nombreEnfants);
        
        System.out.println("Impot : calculerNombreParts");
    }

    Tranche identifierTranche(Imposable imp) {
        if ((imp.revenu.getMontant()) > 10)
            return new Tranche(1);
        else
            return new Tranche(2);
        
        System.out.println("Impot : identifierTranche");
    }

    double calculerFraisReels(Imposable imp) {
        return (imp.getFraisDeclares()) * 0.8;
        System.out.println("Impot : calculerFraisReels");
    }

    double calculerImpot(Imposable imps) {
        Revenu rev = imps.revenu;
        double nombreParts = calculerNombreParts(imps);
        Tranche tran = identifierTranche(imps);
        double frais = calculerFraisReels(imps);
        double MontantRevenu = (rev.getMontant()) - frais;
        double impot = tran.calculerImpot(MontantRevenu, nombreParts);
        return impot;
        System.out.println("Impot : calculerImpot");
    }
}

