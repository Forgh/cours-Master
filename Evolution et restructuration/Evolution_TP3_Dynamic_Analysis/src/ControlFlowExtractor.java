

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtStatement;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtInterface;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtPackage;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.declaration.CtType;
import spoon.reflect.declaration.CtTypeParameter;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.factory.Factory;
import spoon.reflect.factory.TypeFactory;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;

public class ControlFlowExtractor extends AbstractProcessor<CtClass>{
	
	private static Collection<CtMethod> ColnotExposedMethods = new ArrayList<>();
	
	
	
	public void process(CtClass t)
	{  
			if(!t.isInterface())
			{  
				
				System.out.println("Current class: "+t.getSimpleName());
		
				//now we pass to methods
				//if we want to take inherited methods into account, it is possible to use getAllMethods()
				Collection<CtMethod<?>> methods = t.getMethods();
		
				for (CtMethod<?> meth : methods)
				{ 
					
					
					String str = "\"" + t.getQualifiedName() +" : "+ meth.getSimpleName() + "\"";
		
					CtStatement s = getFactory().Code().createCodeSnippetStatement("System.out.println(" + str + ")");
					
					if(meth.getBody() != null)
						meth.getBody().addStatement(s);

					//CtStatement statment = meth.getFactory().Code().createCodeSnippetStatement("System.out.println();")	;
					System.out.println("---> Methode name " + meth.getSimpleName());
					
					List<CtParameter<?>> par = meth.getParameters();
					
					for(CtParameter<?> p : par)
					{
						System.out.println("------> parameter : " +p.getSimpleName());
					}
					
				}
			}
		
	}
	
	
	public static void main(String[] args) throws Exception   {
		
		 Scanner sc= new Scanner(System.in);
		//Mettre le path du projet a tester
		 String path="C:\\Users\\ghost_000\\OneDrive\\Documents\\Master\\Evolution et restructuration\\TP1\\srcTD1Exo1";
		 
		 System.out.println("");
		 System.out.println("------------------------------");
		 System.out.println("----------Let's do it----------");
		 System.out.println("------------------------------");
		 System.out.println("");

		 //Mettre le lien du jar de spoon
		 String jarFiles= "C:\\Users\\ghost_000\\OneDrive\\Documents\\Master\\Evolution et restructuration\\spoon-core-5.3.0-jar-with-dependencies.jar";
	     spoon.Launcher.main(new String[]
	    		 {
	        "-p", "ControlFlowExtractor",
	        "-i", path,
	        "--source-classpath", jarFiles
	    		 });
	     
}
	
}
