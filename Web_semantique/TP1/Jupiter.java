import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.VCARD;

public class Jupiter {

  public static void main(String[] args) {

    // create an empty Model
    Model model = ModelFactory.createDefaultModel();

      model.setNsPrefix("mo", "http://purl.org/ontology/mo/");
      Property conductorProperty = model.createProperty("mo:conductor");
      Property movementProperty = model.createProperty("mo:movement");
      Property genreProperty = model.createProperty("mo:genre");
      Property composerProperty = model.createProperty("mo:composer");
      Property recordingsessionProperty = model.createProperty("mo:recordingSession");

      
      model.setNsPrefix("time", "http://www.w3.org/2006/time#");
      Property yearProperty = model.createProperty("time:year");
      
      String urlOrchestre = "https://fr.wikipedia.or/wiki/Orchestre_symphonique_de_Londres";
      String urlConductor = "https://fr.wikipedia.org/wiki/Claudio_Abbado";
      String mozartURI = "https://fr.wikipedia.org/wiki/Wolfgang_Amadeus_Mozart";
      String jupiterURI = "https://fr.wikipedia.org/wiki/Symphonie_n%C2%BA_41_de_Mozart";
    String nom = "Symphonie no 41 en ut majeur";
    String alias = "Jupiter";
    String genre = "Symphonie";
    String mouvement1 = "https://fr.wikipedia.org/wiki/Symphonie_n%C2%BA_41_de_Mozart#Allegro_vivace";
    String mouvement2 = "https://fr.wikipedia.org/wiki/Symphonie_n%C2%BA_41_de_Mozart#Andante_cantabile";
    String mouvement3 = "https://fr.wikipedia.org/wiki/Symphonie_n%C2%BA_41_de_Mozart#Menutto";
    String mouvement4 = "https://fr.wikipedia.org/wiki/Symphonie_n%C2%BA_41_de_Mozart#Molto_allegro";
    String allegroVivace = "Allegro Vivace";
    String andanteCantabile = "Andante Cantabile";
    String menuetto = "Menuetto"; 
    String moltoAllegro = "Molto Allegro";

      String personne = "Personne";
    
    
    //NOEUD PRINCIPAL
    Resource symphonie = model.createResource(jupiterURI);
    symphonie.addProperty(FOAF.name, nom);
    symphonie.addProperty(FOAF.name, alias);
    model.add(symphonie, genreProperty, genre);

    //MOVEMENTS
    Resource allegroResource = model.createResource(mouvement1).addProperty(FOAF.name, allegroVivace);
    Resource andanteResource = model.createResource(mouvement2).addProperty(FOAF.name, andanteCantabile);
    Resource menuttoResource = model.createResource(mouvement3).addProperty(FOAF.name, menuetto);
    Resource moltoResource = model.createResource(mouvement4).addProperty(FOAF.name, moltoAllegro);
     
    model.add(symphonie,movementProperty,allegroResource);
    model.add(symphonie,movementProperty,andanteResource);
    model.add(symphonie,movementProperty,menuttoResource);
    model.add(symphonie,movementProperty,moltoResource);
    
    
    
      //NOEUD ORCHESTRE
      Resource orchestre = model.createResource(urlOrchestre)
                  .addProperty(FOAF.name, "Orchestre Symphonique de Londres");
      model.add(orchestre, yearProperty, "1980");
      
      Resource conductorResource = model.createResource(urlConductor)
                .addProperty(FOAF.name, "Claudio Abbado")
                .addProperty(RDF.type, "Personne");
      
      model.add(orchestre, conductorProperty, conductorResource);
      
      model.add(symphonie, recordingsessionProperty, orchestre);
    
    
    //NOEUD MOZART
      Resource mozart = model.createResource(mozartURI).addProperty(RDF.type, personne)
                .addProperty(FOAF.name, "Mozart");
      
      model.add(symphonie, composerProperty, mozart );                    
    
    
      model.write(System.out);
  }

}
