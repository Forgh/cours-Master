path="/auto_home/mcampmas/Téléchargements/webTP2"
file1="ekaw"
file2="sigkdd"

for i in {0..9}
	do
cmd="java -jar lib/procalign.jar -i fr.inrialpes.exmo.align.impl.method.StringDistAlignment -DstringFunction=smoaDistance -t 0.$i file://"$path"/"$file1".owl file://"$path"/"$file2".owl -o classes/SMOA$i.rdf"
	eval $cmd
cmd2="java -jar lib/procalign.jar -i fr.inrialpes.exmo.align.impl.method.StringDistAlignment -DstringFunction=levenshteinDistance -t 0.$i file://"$path"/"$file1".owl file://"$path"/"$file2".owl -o classes/levenshtein$i.rdf"
	eval $cmd2
cmd3="java -jar lib/procalign.jar -i fr.inrialpes.exmo.align.impl.method.StringDistAlignment -DstringFunction=equalDistance -t 0.$i file://"$path"/"$file1".owl file://"$path"/"$file2".owl -o classes/equal$i.rdf"
	eval $cmd3
done

cmd="java -jar lib/procalign.jar -i fr.inrialpes.exmo.align.impl.method.StringDistAlignment -DstringFunction=smoaDistance -t 1 file://"$path"/"$file1".owl file://"$path"/"$file2".owl -o classes/SMOA10.rdf"
	eval $cmd
cmd2="java -jar lib/procalign.jar -i fr.inrialpes.exmo.align.impl.method.StringDistAlignment -DstringFunction=levenshteinDistance -t 1 file://"$path"/"$file1".owl file://"$path"/"$file2".owl -o classes/levenshtein10.rdf"
	eval $cmd2
cmd3="java -jar lib/procalign.jar -i fr.inrialpes.exmo.align.impl.method.StringDistAlignment -DstringFunction=equalDistance -t 1 file://"$path"/"$file1".owl file://"$path"/"$file2".owl -o classes/equal10.rdf"
	eval $cmd3

SMOA="SMOA0"
levenshtein="levenshtein0"
equal="equal0"
for i in {1..10}
do
	SMOA="$SMOA,SMOA$i"
	levenshtein="$levenshtein,levenshtein$i"
	equal="$equal,equal$i"
done

cmd4="java -cp lib/procalign.jar fr.inrialpes.exmo.align.util.GroupEval -r refalign.rdf -l 'refalign,$SMOA,$levenshtein,$equal' -c -f prm -o results/eval-final.xml"
eval $cmd4
