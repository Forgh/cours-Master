/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.etu.niels.jena;

/**
 *
 * @author niels
 */
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.GenericRuleReasonerFactory;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.util.FileManager;
import org.apache.jena.util.PrintUtil;



public class QueryAndReasonOnLocalRDF {

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {

		/**
		 * 
		 * Register Namespaces
		 * 
		 */

		String mcfURI = "http://www.mycorporisfabrica.org/ontology/mcf.owl#";
		PrintUtil.registerPrefix("mcf", mcfURI);

		String rdfURI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		PrintUtil.registerPrefix("rdf", rdfURI);

		String goURI = "http://www.geneontology.org/dtds/go.dtd#";
		PrintUtil.registerPrefix("go", goURI);

		String oboURI = "http://purl.obolibrary.org/obo";
		PrintUtil.registerPrefix("obo", oboURI);

		/**
		 * 
		 * Test queries
		 * 
		 * 
		 */
		int taille = 0;
		// List all anatomical entities involved in the flexion of knee joint
		/*String queryMyCF = " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ " PREFIX  rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ " PREFIX  owl: <http://www.w3.org/2002/07/owl#>"
				+ " PREFIX  mcf: <http://www.mycorporisfabrica.org/ontology/mcf.owl#>" + " SELECT distinct ?y"
				+ " WHERE { ?s mcf:ContributesTo mcf:Flexion_of_knee_joint.  }";

		// List the name of all ontology entities created by the user 'midori'
		String queryGO = " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ " PREFIX  rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ " PREFIX  owl: <http://www.w3.org/2002/07/owl#>"
				+ " PREFIX  go: <http://www.geneontology.org/dtds/go.dtd#>"
				+ " PREFIX  obo: <http://purl.obolibrary.org/obo/>" + " SELECT  ?id ?label"
				+ " WHERE  {?id <http://www.geneontology.org/formats/oboInOwl#created_by>  \"midori\" ."
				+ "         ?id <http://www.w3.org/2000/01/rdf-schema#label> ?label ." + "         } " + " LIMIT 10";

		// measure the size of a dataset
		String howManyTriples = "select (count(*) as ?total_number_of_triples) where {?s ?p ?o}";


		// sample some classes
		String someClasses = " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "SELECT DISTINCT ?c where {?c rdfs:subClassOf ?o}";

		//question 5
		String q51 = " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ " PREFIX  mcf: <http://www.mycorporisfabrica.org/ontology/mcf.owl#>"
				+ " SELECT DISTINCT ?c where {?c rdfs:subClassOf mcf:Bone_subpart}";

		String q52 = " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ " PREFIX  mcf: <http://www.mycorporisfabrica.org/ontology/mcf.owl#>"
				+ " SELECT DISTINCT ?c where {?c rdfs:subClassOf mcf:Movement}";

		//question 6
		String q60 =" PREFIX  go: <http://www.geneontology.org/dtds/go.dtd#>"
					+ " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
					+ " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
					+ " SELECT (count(*) as ?d)"
					+ " WHERE {?a go:is_a  <http://www.geneontology.org/go#GO:0016482> }";

		//connexité du graphe
		String connexite =" PREFIX  foaf: <http://xmlns.com/foaf/0.1/>"
                                    +" PREFIX wss2016: <http://ww2.lirmm.fr/~ulliana/HMIN209Social.rdf#>"
                                    + " ASK "
                                    + " WHERE {?x foaf:knows+ ?a."
                                    + " filter(?x != ?a)}";


		String q4 = "PREFIX wss2016: <http://ww2.lirmm.fr/~ulliana/HMIN209Social.rdf#>\n" +
				"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n" +
				"SELECT DISTINCT ?c (count(DISTINCT *) AS ?connexe)\n" +
				"FROM <http://www.lirmm.fr/~ulliana/HMIN209Social.rdf>\n" +
				"WHERE {\n" +
				"?c foaf:knows ?n\n" +
				"}\n" +
				"GROUP BY ?c";
		String q5 = "PREFIX wss2016: <http://ww2.lirmm.fr/~ulliana/HMIN209Social.rdf#>\n" +
				"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
				"PREFIX foaf:<http://xmlns.com/foaf/0.1/>\n" +
				"SELECT DISTINCT (count(DISTINCT *) AS ?connexe)\n" +
				"FROM <http://www.lirmm.fr/~ulliana/HMIN209Social.rdf>\n" +
				"WHERE {\n" +
				"?c foaf:socialFriend ?n .\n" +
				"?n foaf:socialFriend ?m .\n" +
				"?c foaf:socialFriend ?m .\n" +
				"filter( str(?c) < str(?n)).\n" +
				"filter (str(?n) < str(?m))}\n" +
				"\\end{lstlisting}";
		*/
		String q10 = "PREFIX wss2016: <http://ww2.lirmm.fr/~ulliana/HMIN209Social.rdf#>\n" +
				"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n" +
				"SELECT DISTINCT ?personne ( (?NbTriangle / ?NbCouple) AS ?CoefClustering)\n" +
				"FROM <http://www.lirmm.fr/~ulliana/HMIN209Social.rdf>\n" +
				"WHERE {\n" +
				"?personne wss2016:personalFriend ?x .\n" +
				"{\n" +
				"\tSELECT ?personne (count(DISTINCT *) AS ?NbCouple)\n" +
				"\tWHERE {\n" +
				"\t\t?personne wss2016:personalFriend ?x .\n" +
				"\t\t?personne wss2016:personalFriend ?y .\n" +
				"\t\tfilter(str(?x) < str(?y))\n" +
				"\t}\n" +
				"    GROUP BY ?personne\n" +
				"}\n" +
				"{\n" +
				"\tSELECT ?personne (count(DISTINCT *) AS ?NbTriangle)\n" +
				"\tWHERE{\n" +
				"\t\t?personne wss2016:personalFriend ?x .\n" +
				"\t\t?personne wss2016:personalFriend ?y .\n" +
				"\t\t?x wss2016:personalFriend ?y .\n" +
				"\t\tfilter(str(?x) < str(?y))\n" +
				"\t}\n" +
				"    GROUP BY ?personne\n" +
				"}\n" +
				"}";
		/**
		 * 
		 * Test rules
		 * 
		 */

		StringBuilder rules = new StringBuilder();

		// define rules here

		/**
		 * MyCF2 rules
		 */

		/*
		rules.append("[rule1:  (?x mcf:PartOf ?y), (?y mcf:PartOf ?z) -> (?x mcf:PartOf ?z)] ");
		rules.append("[rule2:  (?x rdfs:subClassOf ?y), (?y rdfs:subClassOf ?z) -> (?x rdfs:subClassOf ?z)] ");
		rules.append("[rule3:  (?a, rdfs:subClassOf, ?c), (?c, mcf:InsertOn, ?b) -> (?a, mcf:InsertOn, ?b)] ");
		rules.append("[rule4:  (?a, mcf:IsInvolvedIn, ?c), (?c, rdfs:subClassOf, ?b) -> (?a, mcf:IsInvolvedIn, ?b)] ");
		rules.append("[rule5:  (?a, mcf:ContributesTo, ?c), (?c, mcf:IsInvolvedIn, ?b) -> (?a, mcf:ContributesTo, ?b)] ");
		rules.append("[rule6:  (?a, mcf:ContributesTo, ?c), (?c, rdfs:subClassOf, ?b) -> (?a, mcf:ContributesTo, ?b)] ");
		rules.append("[rule7:  (?a, mcf:InsertOn, ?c), (?c, mcf:PartOf, ?b) -> (?a, mcf:InsertOn, ?b)] ");
		rules.append("[rule8:  (?a, mcf:IsInvolvedIn, ?c), (?c, mcf:IsInvolvedIn, ?b) -> (?a, mcf:IsInvolvedIn, ?b)] ");
		rules.append("[rule9:  (?a, mcf:LeftSubClassOf, ?b)  -> (?a, rdfs:subClassOf, ?b)]");
		rules.append("[rule10: (?a, mcf:RightSubClassOf, ?b) -> (?a, rdfs:subClassOf, ?b)]");
		rules.append("[rule11: (?a, mcf:HasFunction, ?b) -> (?a, mcf:ContributesTo, ?b)]");
		*/

		/**
		 * GO rules
		 */
		/*
		rules.append("[rule1:  (?a, go:is_a, ?o), (?o, go:is_a, ?c) -> (?a, go:is_a, ?c)] ");
		rules.append("[rule2:  (?a, go:is_a, ?o), (?o, go:part_of, ?c) -> (?a, go:part_of, ?c)] ");
		rules.append("[rule3:  (?a, go:is_a, ?o), (?o, go:regulates, ?c) -> (?a, go:regulates, ?c)] ");
		rules.append("[rule4:  (?a, go:is_a, ?o), (?o, go:has_part, ?c) -> (?a, go:has_part, ?c)] ");
		rules.append("[rule5: (?a, go:part_of, ?o), (?o, go:is_a, ?c) -> (?a, go:part_of, ?c)]");
		rules.append("[rule6: (?a, go:part_of, ?o), (?o, go:part_of, ?c) -> (?a, go:part_of, ?c)]");
		rules.append("[rule7: (?a, go:regulates, ?o), (?o, go:is_a, ?c) -> (?a, go:regulates, ?c)]");
		rules.append("[rule8: (?a, go:regulates, ?o), (?o, go:part_of, ?c) -> (?a, go:regulates, ?c)]");
		rules.append("[rule9: (?a, go:has_part, ?o), (?o, go:is_a, ?c) -> (?a, go:has_part, ?c)]");
		rules.append("[rule10: (?a, go:has_part, ?o), (?o, go:has_part, ?c) -> (?a, go:has_part, ?c)]");
		rules.append("[rule11:  (?x rdfs:subClassOf ?y), (?y rdfs:subClassOf ?z) -> (?x rdfs:subClassOf ?z)] ");
		rules.append("[rule12: (?a, go:is_a, ?o), (?o, go:positively_regulates, ?c) -> (?a, go:positively_regulates, ?c)]");
		rules.append("[rule13: (?a, go:is_a, ?o), (?o, go:negatively_regulates, ?c) -> (?a, go:negatively_regulates, ?c)]");
		rules.append("[rule14: (?a, go:positively_regulates, ?o), (?o, go:is_a, ?c) -> (?a, go:positively_regulates, ?c)]");
		rules.append("[rule15: (?a, go:positively_regulates, ?o), (?o, go:part_of, ?c) -> (?a, go:regulates, ?c)]");
		rules.append("[rule16: (?a, go:negatively_regulates, ?o), (?o, go:is_a, ?c) -> (?a, go:negatively_regulates, ?c)]");
		rules.append("[rule17: (?a, go:negatively_regulates, ?o), (?o, go:part_of, ?c) -> (?a, go:regulates, ?c)]");*/

		/**
		 * 
		 * Create a data model and load file
		 * 
		 */

		Model model = ModelFactory.createDefaultModel();

		String pathToOntology = "soc-parted.txt";
		//String pathToOntology = "/Users/niels/NetBeansProjects/Jena/src/main/java/fr/etu/niels/go_daily-termdb.rdf-xml";
		//String pathToOntology = "/auto_home/nbenichou02/KINGSTON/jena ontologies/MyCF/MyCF2.rdf";
		
		InputStream in = FileManager.get().open(pathToOntology);

		Long start = System.currentTimeMillis();

		model.read(in, null, "TURTLE");

		System.out.println("Import time : " + (System.currentTimeMillis() - start));

		/**
		 * 
		 * Starting a reasoner
		 * 
		 */

		GenericRuleReasoner reasoner = (GenericRuleReasoner) GenericRuleReasonerFactory.theInstance().create(null);

		reasoner.setRules(Rule.parseRules(rules.toString()));

		// change the type of reasoner
		reasoner.setMode(GenericRuleReasoner.HYBRID);

		start = System.currentTimeMillis();

		InfModel inf = ModelFactory.createInfModel(reasoner, model);

		System.out.println("Rules pre-processing time : " + (System.currentTimeMillis() - start));

		/**
		 *
		 * Create a query object
		 *
		 */

		//Query query = QueryFactory.create(someClasses);
		//Query query = QueryFactory.create(howManyTriples);
		//Query query = QueryFactory.create(queryMyCF);
		//Query query = QueryFactory.create(queryGO);
		
		//question 5
		Query query = QueryFactory.create(q10); //   <-----------QUERY   QUERY   QUERY <--------------------------------------------------------------
		
		start = System.currentTimeMillis();

		QueryExecution qexec = QueryExecutionFactory.create(query, inf);

		System.out.println("Query pre-processing time : " + (System.currentTimeMillis() - start));

		/**
		 * 
		 * Execute Query and print result
		 * 
		 */
		start = System.currentTimeMillis();

		try {

			ResultSet rs = qexec.execSelect();

			ResultSetFormatter.out(System.out, rs, query);
			taille =  rs.getRowNumber();

		} finally {

			qexec.close();
		}

		System.out.println("Query + Display time : " + (System.currentTimeMillis() - start));
		System.out.println("taille: "+taille);

		// /**
		// *
		// * Export saturated ontology
		// *
		// */
		//
		// PrintWriter resultWriter;
		// try {
		// resultWriter = new PrintWriter("testmycfsat.rdf");
		//
		// inf.write(resultWriter);
		//
		// resultWriter.close();
		//
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// }

	}
}
