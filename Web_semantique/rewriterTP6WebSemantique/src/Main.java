/**
 * Created by Forgh
 */
import java.io.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("Started!");
        String line = "";
        String[] frags;
        BufferedReader reader  =null;
        PrintWriter writer = null;
        //Chemin+nom vers le fichier d'entrée, à changer par le votre !
        String filepath = "soc-Epinions1.txt";
        try {
            reader = new BufferedReader(new FileReader(filepath));

            //fichier de sortie (à changer par .. ce que vous voulez)
            writer = new PrintWriter("newfile.txt", "UTF-8");

            writer.println("@prefix foaf: <http://xmlns.com/foaf/0.1/> .");
            writer.println("@prefix wss2016: <http://www2.lirmm.fr/~ulliana/HMIN209Social.rdf#> .");

            while ((line = reader.readLine()) != null){
                frags = line.split("\t"); //split les lignes en fonction des tabulations!


                //Voici comment la ligne va être écrite. A changer en fonction de vos besoins!
                writer.println("wss2016:u"+frags[0]+" foaf:knows wss2016:u"+frags[1]+" .");

                //Flush l'écrivain, très important, laissez-le !
                writer.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                reader.close();
                writer.close();
            } catch (Exception exp) {

            }
        }
        System.out.println("Done!");
    }
}
