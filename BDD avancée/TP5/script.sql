-- PART "3"
DROP TABLE Publications FORCE;
DROP TABLE Tags FORCE;
DROP TABLE Date_d FORCE;
DROP TABLE Utilisateur FORCE;
DROP TABLE Temps FORCE;
DROP TABLE Photographie FORCE;



CREATE TABLE Date_d (
  dateKey number,
  date_d date,
  dateEntiere varchar2(10),
  jourDeLaSemaine varchar2(10),
  mois varchar2(10),
  annee number,
  CONSTRAINT pk_date PRIMARY KEY (dateKey)
);

DECLARE
  year  number;
  month  number;
  day  number;
  result varchar2(10);
Begin

FOR i IN 1..20
LOOP
  year:=FLOOR(DBMS_RANDOM.value(2000,2100));
  month:=FLOOR(DBMS_RANDOM.value(1,12));
  IF month=2 and (year/4)=0 and (year/100)!=0 then
    day:=FLOOR(DBMS_RANDOM.value(1,29));
  ELSIF month=2 or (year/100)=0 then
    day:=FLOOR(DBMS_RANDOM.value(1,28));
  ELSIF MOD(month,2)=1 then
    day:=FLOOR(DBMS_RANDOM.value(1,31));
  ELSIF MOD(month,2)=0 and month!=2 then
    day:=FLOOR(DBMS_RANDOM.value(1,30));
  END IF;  
  result:=day||'-'||month||'-'||year;

  INSERT INTO Date_d VALUES (i, TO_DATE(result,'dd-mm-yyyy'), result, to_char(to_date(result,'dd-mm-yyyy'),'DAY'), to_char(to_date(result,'dd-mm-yyyy'),'MONTH'), year);
END LOOP;

End;
/

CREATE TABLE Utilisateur (
  utilisateurKey number,
  nom varchar2(25),
  prenom varchar2(25),
  CONSTRAINT pk_utilisateur PRIMARY KEY (utilisateurKey)
);


INSERT INTO Utilisateur
SELECT ROUND(DBMS_RANDOM.value(1,10000)) AS utilisateurKey,
DBMS_RANDOM.string('u',5) AS nom,
DBMS_RANDOM.string('u',5) AS prenom
FROM dual
CONNECT BY level <= 20;
COMMIT;




CREATE TABLE Temps (
  minuteKey number,
  seconde number,
  minute number,
  heure number, 
  tempsEntier varchar2(10),
  CONSTRAINT pk_temps PRIMARY KEY (minuteKey)
);


DECLARE
  seconde  number;
  minute  number;
  heure  number;
  tempsEntier varchar2(10);
Begin

FOR i IN 1..20
LOOP
  seconde:= ROUND(DBMS_RANDOM.value(0,59));
  minute:= ROUND(DBMS_RANDOM.value(0,59));
  heure := ROUND(DBMS_RANDOM.value(0,23));
  tempsEntier:=heure||':'||minute||':'||seconde;

  INSERT INTO Temps VALUES (i, seconde, minute, heure, tempsEntier);
END LOOP;

End;
/

CREATE TABLE Appareil (
  nomAppareilPhoto varchar2(25),
  modele varchar2(25),
  marque varchar2(25),
  CONSTRAINT pk_appareil PRIMARY KEY (idAppareil) 
);

INSERT INTO Appareil VALUES ("Nikon D3200", "D3200", "Nikon");
INSERT INTO Appareil VALUES ("Nikon D3100", "D3100", "Nikon");
INSERT INTO Appareil VALUES ("K4600 Canon", "K4600", "Canon");
INSERT INTO Appareil VALUES ("K4700 Canon", "K4700", "Canon");
INSERT INTO Appareil VALUES ("K4800 Canon", "K4800", "Canon");


CREATE TABLE Concepteurs (
	id_concepteur number,
	nom varchar2(25),
	prenom varchar2(25),
	nomAppareilPhoto varchar2(25),
	CONSTRAINT fk_concepteur_appareil FOREIGN KEY (nomAppareilPhoto) references Appareil(nomAppareilPhoto),
	CONSTRAINT pk_concepteur PRIMARY KEY (id_concepteur, nom, prenom)
);

INSERT INTO Concepteurs VALUES ("Takuo", "Kuo", "Nikon D3200");
INSERT INTO Concepteurs VALUES ("Takuo", "Kuo", "Nikon D3100");
INSERT INTO Concepteurs VALUES ("Eiji", "Fumio", "Nikon D3200");
INSERT INTO Concepteurs VALUES ("Sekai", "Yui", "K4600 Canon");
INSERT INTO Concepteurs VALUES ("Yokai", "Nuo", "K4700 Canon");
INSERT INTO Concepteurs VALUES ("Ishi", "Tetsuo", "K4800 Canon");



CREATE TABLE Bridge_Appareil (
	ancetre varchar2(25),
	descendant varchar2(25),
	distance varchar2(25),
	estEnHaut number,
	estEnBas number,
	CONSTRAINT fk_bridge_appareil_anc FOREIGN KEY (ancetre) references Appareil(nomAppareilPhoto),
	CONSTRAINT fk_bridge_appareil_des FOREIGN KEY (descendant) references Appareil(nomAppareilPhoto),
);



INSERT INTO Bridge_Appareil VALUES ("Nikon D3100", "Nikon D3100", 0, 1, 0);
INSERT INTO Bridge_Appareil VALUES ("Nikon D3100", "Nikon D3200", 1, 0, 0);
INSERT INTO Bridge_Appareil VALUES ("Nikon D3200", "Nikon D3200", 0, 0, 1);

INSERT INTO Bridge_Appareil VALUES ("K4600 Canon", "K4600 Canon", 0, 1, 0);
INSERT INTO Bridge_Appareil VALUES ("K4600 Canon", "K4700 Canon", 1, 0, 0);
INSERT INTO Bridge_Appareil VALUES ("K4600 Canon", "K4800 Canon", 2, 0, 0);
INSERT INTO Bridge_Appareil VALUES ("K4700 Canon", "K4700 Canon", 0, 0, 0);
INSERT INTO Bridge_Appareil VALUES ("K4700 Canon", "K4800 Canon", 1, 0, 0);
INSERT INTO Bridge_Appareil VALUES ("K4800 Canon", "K4800 Canon", 0, 0, 1);


--Question 1 - 2
SELECT a.nomAppareilPhoto, COUNT(p.photographieKey)
FROM Appareil a, Photographie p, Bridge_Appareil b
WHERE a.nomAppareilPhoto = b.ancetre
AND p.nomAppareilPhoto = a.nomAppareilPhoto
AND b.ancetre != b.descendant
GROUP BY a.nomAppareilPhoto;

--Question 1 - 3
SELECT c.nom, c.prenom, COUNT(p.photographieKey)
FROM Appareil a, Photographie p, Bridge_Appareil b, Concepteurs c
WHERE a.nomAppareilPhoto = b.ancetre
AND p.nomAppareilPhoto = a.nomAppareilPhoto
AND a.nomAppareilPhoto = c.nomAppareilPhoto
GROUP BY c.nom, c.prenom;





CREATE TABLE Photographie (
  photographieKey number,
  coordonneeX float,
  coordonneeY float,
  ouvertureFocale number,
  tempsExposition number,
  flashActif varchar2(1),
  distanceFocale number,
  ISO varchar2(10),
  nomAppareilPhoto varchar2(25),
  CONSTRAINT pk_photographie PRIMARY KEY (photographieKey),
  CONSTRAINT fk_photographie_appareil FOREIGN KEY (nomAppareilPhoto) references Appareil(nomAppareilPhoto)
);



DECLARE 
	marque varchar2(25);
	nomAppareil varchar(25);
	flash varchar2(1);
BEGIN

FOR i in 1..20 
LOOP

	with booleans as (
		select 'T' as s from dual union all
		select 'F' as s from dual 
	)
	select s INTO flash 
	from (
		select s 
		from booleans 
		order by dbms_random.value()
	) s where rownum = 1;


	with marques as ( 
		select 'Nikon' as s from dual union all
		select 'Canon' as s from dual union all
		select 'Olympus' as s from dual
	) 
	select s INTO marque 
	from (
		select s 
		from marques 
		order by dbms_random.value()
	) s where rownum = 1;

	select s INTO nomAppareil 
	from (
		select s 
		from Appareil
		WHERE marque = marque 
		order by dbms_random.value()
	) s where rownum = 1;
	 
	INSERT INTO Photographie VALUES (
		i,
		DBMS_RANDOM.value(-85,85),
		DBMS_RANDOM.value(-85,85),
		ROUND(DBMS_RANDOM.value(0,100)),
		ROUND(DBMS_RANDOM.value(0,1000)),
		flash,
		ROUND(DBMS_RANDOM.value(0,100)),
		DBMS_RANDOM.string('u',10),
		nomAppareil
	);

END LOOP;

END;
/


CREATE TABLE Publications (
  dateKey number, 
  utilisateurKey number,
  minuteKey number,
  photographieKey number,
  publicationId number, 
  nbVisualisations number,
  CONSTRAINT pk_publications primary key (publicationId),
  CONSTRAINT fk_publications_dateKey foreign key (dateKey) references Date_d(dateKey),
  CONSTRAINT fk_publications_userKey foreign key (utilisateurKey) references Utilisateur(utilisateurKey),
  CONSTRAINT fk_publications_minuteKey foreign key (minuteKey) references Temps(minuteKey),
  CONSTRAINT fk_publications_photoKey foreign key (photographieKey) references Photographie(photographieKey)
);



DECLARE 
	dateKey number;
	utilisateurKey number;
	minuteKey number;
	photographieKey number;
	publicationId number;
	nbVisualisations number;
BEGIN

FOR i IN 1..100 LOOP

	select dateKey INTO dateKey 
	from (
		select dateKey 
		from Date_d 
		order by dbms_random.value()
	)  where rownum = 1;

	select utilisateurKey INTO utilisateurKey 
	from (
		select utilisateurKey 
		from Utilisateur 
		order by dbms_random.value()
	)  where rownum = 1;

	select minuteKey INTO minuteKey 
	from (
		select minuteKey 
		from Temps 
		order by dbms_random.value()
	)  where rownum = 1;

	select photographieKey INTO photographieKey 
	from (
		select photographieKey 
		from Photographie 
		order by dbms_random.value()
	)  where rownum = 1;


	INSERT INTO Publications VALUES (
		dateKey,
		utilisateurKey,
		minuteKey,
		photographieKey,
		i,
		ROUND(DBMS_RANDOM.value(0,1000000))
	);
END LOOP;
END;
/

CREATE TABLE Tags(
  utilisateurKey number,
  photographieKey number,
  tagId number,
  motCle varchar2(200),
  constraint pk_tags primary key (tagId),
  constraint fk_userTag foreign key (utilisateurKey) references Utilisateur(utilisateurKey),
  constraint fk_photoTag foreign key (photographieKey) references Photographie(photographieKey)
);


DECLARE 
	utilisateurKey number;
	photographieKey number;
	motCle varchar2(200);
BEGIN

FOR i IN 1..100 LOOP

	select utilisateurKey INTO utilisateurKey 
	from (
		select utilisateurKey 
		from Utilisateur 
		order by dbms_random.value()
	)  where rownum = 1;

	select photographieKey INTO photographieKey 
	from (
		select photographieKey 
		from Photographie 
		order by dbms_random.value()
	)  where rownum = 1;


	INSERT INTO Tags VALUES (
		utilisateurKey,
		photographieKey,
		i,
		DBMS_RANDOM.string('u',30)
	);
END LOOP;
END;
/


