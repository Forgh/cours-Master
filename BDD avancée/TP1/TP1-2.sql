//rajouter l'attribut commantaire_autorisé à la table publication
//s'abonner à une personne include suivre ses publications
//commentaries et photographies sont des cas particuliers d’un Contenu Numérique


CREATE TABLE PHOTOGRAPHIE
       (CODE NUMBER(5) NOT NULL,
        LIEU VARCHAR2(20) NOT NULL,
        DROITS_RESERVES NUMBER(1) NOT NULL,
        UT_COMM_AUTO NUMBER(1) NOT NULL,
        MODIFICATION_AUTORISEE NUMBER(1) NOT NULL,
        NUM_CONFIG NUMBER(5) NOT NULL,
        DATE_PUB DATE NOT NULL,
        NUM_ALB NUMBER(3),
        NUM_GAL NUMBER(3),
        ID_APPAREIL NUMBER(5) UNSIGNED NOT NULL,
        CONSTRAINT PK_PHOTO PRIMARY KEY (CODE),
        CONSTRAINT FK_PHOTO_CONFIG FOREIGN KEY (NUM_CONFIG) REFERENCES CONFIGURATION(NUM_CONFIG),
        CONSTRAINT FK_PHOTO_CONFIG FOREIGN KEY (ID_APPAREIL) REFERENCES APPAREIL(ID_APPAREIL)
        );
        
    /* Tuples de la table Photographie*/
INSERT INTO PHOTOGRAPHIE VALUES
        (00001,'montpellier',1,0,0,10000,'01-JAN-2015',01,01);
INSERT INTO PHOTOGRAPHIE VALUES
        (00002,'paris',1,0,0,10000,'01-JAN-2015',01,01);
INSERT INTO PHOTOGRAPHIE VALUES
        (00003,'nice',0,1,1,20000,'01-JAN-2015',01,01);
INSERT INTO PHOTOGRAPHIE VALUES
        (00004,'toulouse',1,1,1,30000,'01-JAN-2015',01,01);
INSERT INTO PHOTOGRAPHIE VALUES
        (00005,'lyon',0,1,0,40000,'01-JAN-2015',01,01);
        
CREATE TABLE CONFIGURATION
       (NUM_CONFIG NUMBER(5) UNSIGNED NOT NULL,
        OUVERTURE_FOCALE FLOAT(2) NOT NULL,
        TEMPS_EXPOSITION NUMBER(10) NOT NULL,
        FLASH NUMBER(1) NOT NULL,
        DISTANCE_FOCALE NUMBER(4) NOT NULL,
        CONSTRAINT PK_CONFIG PRIMARY KEY (NUM_CONFIG) );

/* Tuples de la table Configuration*/
INSERT INTO CONFIGURATION VALUES
        (10000,3.5,100,TRUE,35);
INSERT INTO CONFIGURATION VALUES
        (20000,3.7,200,FALSE,40);
INSERT INTO CONFIGURATION VALUES
        (30000,4.5,300,TRUE,45);
INSERT INTO CONFIGURATION VALUES
        (40000,4.7,400,FALSE,50);
INSERT INTO CONFIGURATION VALUES
        (50000,5.6,500,TRUE,55);

CREATE TABLE appareil
    (id_appareil NUMBER(5) NOT NULL,
     nom VARCHAR2(50) NOT NULL
     CONSTRAINT pk_appareil PRIMARY KEY (id_appareil) );

INSERT INTO appareil VALUES
(001,'appareil1');
INSERT INTO appareil VALUES
(002,'appareil2');
INSERT INTO appareil VALUES
(003,'appareil3');
INSERT INTO appareil VALUES
(004,'appareil4');
INSERT INTO appareil VALUES
(005,'appareil5');
        
ALTER TABLE PHOTOGRAPHIE ADD CONSTRAINT FK_CONFIG FOREIGN KEY (NUM_CONFIG) REFERENCES CONFIGURATION (NUM_CONFIG);

    
    /* Question 3 a  b*/
SELECT * FROM PHOTOGRAPHIE WHERE LIEU='montpellier';
SELECT ID_USER FROM USERS, PHOTOGRAPHIE WHERE DATE_PUB <= '2010-01-01';
SELECT ID_USER, COUNT(*) FROM  utilisateur, ... WHERE ; 

_____________________________________________________________________

CREATE TABLE utilisateur (
    id_user number(5) UNSIGNED NOT NULL,
    pseudonyme varchar(25) NOT NULL,
    password varchar(25) NULL,
    description varchar(2000),
    nb_followers number(5) UNSIGNED DEFAULT '0',
    CONSTRAINT pk_user PRIMARY KEY (id_user)
);

INSERT INTO utilisateur VALUES
(1,'pseudo1','pass1','ma description');
INSERT INTO utilisateur VALUES
(2,'pseudo2','pass2','ma description');
INSERT INTO utilisateur VALUES
(3,'pseudo3','pass3','ma description');
INSERT INTO utilisateur VALUES
(4,'pseudo4','pass4','ma description');
INSERT INTO utilisateur VALUES
(5,'pseudo5','pass5','ma description');

CREATE TABLE album(
    id_album number(8) UNSIGNED NOT NULL,
    nom_album varchar(25),
    id_proprietaire number(5) unsigned NOT NULL,
    CONSTRAINT pk_album PRIMARY KEY (id_album),
    CONSTRAINT fk_album_proprio FOREIGN KEY (id_proprietaire) REFERENCES utilisateur(id_user)
);
INSERT INTO album VALUES
(10,'album1','1');
INSERT INTO album VALUES
(20,'album2','2');
INSERT INTO album VALUES
(30,'album3','3');
INSERT INTO album VALUES
(40,'album4','4');
INSERT INTO album VALUES
(50,'album5','5');

CREATE TABLE album_photographie(
    id_album number(8) UNSIGNED NOT NULL,
    code number(5) UNSIGNED NOT NULL,
    CONSTRAINT pk_album_photographie PRIMARY KEY (id_album,code),
    CONSTRAINT fk_album_photographie_id_album FOREIGN KEY (id_album) REFERENCES album(id_album),
    CONSTRAINT fk_album_photographie_code photo FOREIGN KEY (code) REFERENCES photographie(code)
);

INSERT INTO album_photographie VALUES
(10,00001);
INSERT INTO album_photographie VALUES
(20,00002);
INSERT INTO album_photographie VALUES
(30,00003);
INSERT INTO album_photographie VALUES
(40,00004);
INSERT INTO album_photographie VALUES
(50,00005);

CREATE TABLE publication (
    id_user number(5) UNSIGNED NOT NULL,
    code number(5) UNSIGNED NOT NULL,
    date_publication date NOT NULL,
    CONSTRAINT pk_publication PRIMARY KEY (id_user,code),
    CONSTRAINT fk_publication_id_user FOREIGN KEY (id_user) REFERENCES utilisateur(id_user),
    CONSTRAINT fk_publication_code_photo FOREIGN KEY (code) REFERENCES photographie(code)
);

INSERT INTO publication VALUES
(1,00001,2015-01-01);
INSERT INTO publication VALUES
(2,00002,2015-01-02);
INSERT INTO publication VALUES
(3,00003,2015-01-03);
INSERT INTO publication VALUES
(4,00004,2015-01-04);
INSERT INTO publication VALUES
(5,00005,2015-01-01);

CREATE TABLE tag (
    nom varchar(25) NOT NULL,
    code number(5) UNSIGNED NOT NULL,
    CONSTRAINT pk_tag PRIMARY KEY (nom,code),
    CONSTRAINT fk_tag_code_photo FOREIGN KEY (code) REFERENCES photographie(code)
);

INSERT INTO tag VALUES
('tag1',00001);
INSERT INTO tag VALUES
('tag2',00002);
INSERT INTO tag VALUES
('tag3',00003);
INSERT INTO tag VALUES
('tag4',00004);
INSERT INTO tag VALUES
('tag5',00005);

CREATE TABLE galerie (
    id_galerie number(8) UNSIGNED NOT NULL,
    nom_galerie varchar(25),
    CONSTRAINT pk_galerie PRIMARY KEY (id_galerie)
);

INSERT INTO galerie VALUES
(0001,'galerie1');
INSERT INTO galerie VALUES
(0002,'galerie2');
INSERT INTO galerie VALUES
(0003,'galerie3');
INSERT INTO galerie VALUES
(0004,'galerie4');
INSERT INTO galerie VALUES
(0005,'galerie5');

CREATE TABLE galerie_user (
    id_user number(5) UNSIGNED NOT NULL,
    id_galerie number(8) UNSIGNED NOT NULL,
    CONSTRAINT pk_galerie_user PRIMARY KEY (id_user,id_galerie),
    CONSTRAINT fk_galerie_user_id_user FOREIGN KEY (id_user) REFERENCES utilisateur(id_user),
    CONSTRAINT fk_galerie_user_id_galerie FOREIGN KEY (id_galerie) REFERENCES galerie(id_galerie)
);

CREATE TABLE suivre (
    id_user_suivi number(5) UNSIGNED NOT NULL,
    id_user_suivant number(5) UNSIGNED NOT NULL,
    CONSTRAINT pk_suivre PRIMARY KEY (id_user_suivi,id_user_suivant),
    CONSTRAINT fk_suivre_id_user_suivi FOREIGN KEY (id_user_suivi) REFERENCES utilisateur(id_user),
    CONSTRAINT fk_suivre_id_user_suivant FOREIGN KEY (id_user_suivant) REFERENCES utilisateur(id_user),
);

CREATE TRIGGER inc_nb_follower AFTER INSERT ON suivre
    UPDATE utilisateur SET nb_followers = nb_followers + 1
    WHERE utilisateur.id_user = :new.id_user_suivi;

INSERT INTO galerie_user VALUES
(1,0001);
INSERT INTO galerie_user VALUES
(2,0002);
INSERT INTO galerie_user VALUES
(3,0003);
INSERT INTO galerie_user VALUES
(4,0004);
INSERT INTO galerie_user VALUES

CREATE TABLE galerie_photographie (
    code number(5) UNSIGNED NOT NULL,
    id_galerie number(8) UNSIGNED NOT NULL,
    CONSTRAINT pk_galerie_user PRIMARY KEY (code,id_galerie),
    CONSTRAINT fk_galerie_user_code_photo FOREIGN KEY (code) REFERENCES photographie(code),
    CONSTRAINT fk_galerie_user_id_galerie FOREIGN KEY (id_galerie) REFERENCES galerie(id_galerie)
);

CREATE TABLE like (
    id_user number(5) UNSIGNED NOT NULL,
    code_photo number(5) UNSIGNED NOT NULL,
    CONSTRAINT pk_like PRIMARY KEY (id_user,code_photo),
    CONSTRAINT fk_like_id_user FOREIGN KEY (id_user) REFERENCES utilisateur(id_user),
    CONSTRAINT fk_like_code_photo FOREIGN KEY (code_photo) REFERENCES photographie(code),
);

INSERT INTO galerie_photographie VALUES
(00001,0001);
INSERT INTO galerie_photographie VALUES
(00002,0002);
INSERT INTO galerie_photographie VALUES
(00003,0003);
INSERT INTO galerie_photographie VALUES
(00004,0004);
INSERT INTO galerie_photographie VALUES
(00005,0005);