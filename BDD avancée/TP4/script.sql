-- DROP TABLE salaires ;
CREATE TABLE salaires (
departement VARCHAR(10) NOT NULL,
employee VARCHAR(10) NOT NULL,
manager VARCHAR(10) NOT NULL,
job VARCHAR(10) NOT NULL,
salaire NUMBER(10,2) NOT NULL
);


INSERT INTO salaires
SELECT DBMS_RANDOM.string('u',1) AS departement,
DBMS_RANDOM.string('u',1) AS employee,
DBMS_RANDOM.string('u',1) AS manager,
DBMS_RANDOM.string('u',1) AS job,
ROUND(DBMS_RANDOM.value(low => 1, high => 10000), 2) AS salaire
FROM dual
CONNECT BY level <= 1000;
COMMIT;
SELECT * FROM salaires;



--- R2
---2. Quel est le salaire moyen des employées du manager ’A’ par département et type d’emploi (job) ?
SELECT employee, departement, AVG(salaire)
FROM salaires
WHERE manager = 'A'
GROUP BY (employee, departement);
-- 110 rows selected


SELECT employee, departement, AVG(salaire)
FROM salaires
WHERE manager = 'A'
GROUP BY ROLLUP (employee, departement);
-- 137 rows selected


SELECT employee, departement, AVG(salaire)
FROM salaires
WHERE manager = 'A'
GROUP BY CUBE (employee, departement);
-- 163 rows selected



