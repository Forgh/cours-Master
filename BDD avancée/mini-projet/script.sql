-- Creation des TAD
drop table Salle force;
drop table Coach force;
drop table Abonne force;
drop table Seance force;
drop table Reservation force;
drop type Fitness_t force;
drop type Cardio_t force;
drop type Musculation_t force;
drop type EquipementArray_t force;
drop type Seance_t force;
drop type Collection_Seance_t force;
drop type Coach_t force;
drop type Abonne_t force;
drop type Utilisateur_t force;
drop type Inscriptions_varray_t force;
drop type Inscription_t force;
drop type Salle_t force;
drop type Collection_Reservation_t force;
drop type Equipement_t force;
drop type Reservation_t force;
create or replace TYPE Equipement_t as Object (
  id number,
  nom varchar2(100)
) not final;
/
create or replace type EquipementArray_t as VARRAY(10) OF Equipement_t;
/
create or replace type Reservation_t AS Object(
  id number,
  heureDebut date,
  heureFin date
) final;
/
create or replace type Collection_Reservation_t as Table OF Reservation_t;
/
create or replace type Seance_t as object(
  id number,
  heureDebut date,
  heureFin date
) final;
/

create or replace type Collection_Seance_t as Table OF Seance_t;
/
CREATE OR REPLACE TYPE Inscription_t as Object (
  dateInscription DATE,
  prix NUMBER
);
/
CREATE or replace TYPE Inscriptions_varray_t As VARRAY(10) OF Inscription_t;
/

CREATE OR REPLACE TYPE Utilisateur_t AS OBJECT (
  idUser number,
  nom varchar(100),
  prenom varchar(100)
)not final;
/


CREATE OR REPLACE TYPE Abonne_t UNDER Utilisateur_t (
  reduction number,
  inscriptions Inscriptions_varray_t,
  liste_reservation Collection_Reservation_t,
  liste_seance_abonne Collection_Seance_t,
  MEMBER FUNCTION calculateDiscount RETURN number
) final;
/

CREATE OR REPLACE TYPE BODY Abonne_t AS
	MEMBER function calculateDiscount return number IS
	BEGIN
		RETURN inscriptions.count * 4;
	END calculateDiscount;
END;
/

CREATE OR REPLACE TYPE Coach_t UNDER Utilisateur_t (
  specialite varchar(100),
  liste_seance Collection_Seance_t,
  MEMBER PROCEDURE listProchaineSeances
) final;
/

CREATE OR REPLACE TYPE BODY Coach_t AS
MEMBER PROCEDURE listProchaineSeances IS
BEGIN
	FOR i IN 1 ..  liste_seance.count()
	LOOP 
		IF(liste_seance(i).heureDebut >= SYSDATE) THEN
		   dbms_output.put_line(TO_CHAR(liste_seance(i).heureDebut, 'DD-MM-YYYY HH24:MI')||' to '||TO_CHAR(liste_seance(i).heureFin, 'DD-MM-YYYY HH24:MI'));
      END IF;
   end loop;
END listProchaineSeances;
END;
/


ALTER TYPE Seance_t ADD ATTRIBUTE (animateur REF Coach_t) CASCADE;

CREATE OR REPLACE TYPE Salle_t as Object (
  numero number,
  collection_Reservation Collection_Reservation_t,
  listeEquipements EquipementArray_t,
  MEMBER procedure listEquipements,
  MEMBER procedure reservationOn(d date)
);
/

ALTER TYPE Reservation_t ADD ATTRIBUTE (numero_salle REF Salle_t)CASCADE;

CREATE OR REPLACE TYPE BODY Salle_t AS
MEMBER PROCEDURE listEquipements IS
BEGIN
	FOR i IN 1 ..  listeEquipements.count()
	LOOP
      dbms_output.put_line(listeEquipements(i).nom);
   end loop;
END listEquipements;	

MEMBER PROCEDURE reservationOn(d date) IS
BEGIN
	FOR i IN 1 ..  collection_Reservation.count()
	LOOP 
		IF(TO_DATE(collection_Reservation(i).heureDebut, 'DD-MM-YYYY') = TO_DATE(d, 'DD-MM-YYYY')) THEN
		   dbms_output.put_line(TO_CHAR(collection_Reservation(i).heureDebut, 'HH24:MI') || ' to ' ||TO_CHAR(collection_Reservation(i).heureFin, 'HH24:MI'));
      END IF;
   end loop;
END reservationOn;
END;
/


CREATE OR REPLACE TYPE Fitness_t UNDER Equipement_t ()final;
/

CREATE OR REPLACE TYPE Cardio_t UNDER Equipement_t (
  vitesseMax number
)final;
/
CREATE OR REPLACE TYPE Musculation_t UNDER Equipement_t (
  poidsMin number,
  poidsMax number
)final;
/
-- Creation des tables objet (racines de persistance)
CREATE TABLE Salle OF Salle_t (
  CONSTRAINT pk_salle PRIMARY KEY(numero)
)NESTED TABLE collection_Reservation STORE AS collection_Reservation_table;
CREATE TABLE Reservation OF Reservation_t (
  CONSTRAINT pk_reservation PRIMARY KEY (id)
);
CREATE TABLE Abonne OF Abonne_t (
  CONSTRAINT pk_abonne PRIMARY KEY (idUser)
)NESTED TABLE liste_seance_abonne STORE AS liste_seance_abonne_table
NESTED TABLE liste_reservation STORE AS liste_reservation_table;
CREATE TABLE Coach OF Coach_t (
  CONSTRAINT pk_coach PRIMARY KEY (idUser)
)NESTED TABLE liste_seance STORE AS liste_seance_coach_table;
CREATE TABLE Seance OF Seance_t (
  CONSTRAINT pk_seance PRIMARY KEY (id)
);
-- TRIGGERS
CREATE OR REPLACE TRIGGER is_room_available
  BEFORE INSERT OR UPDATE ON Reservation
  FOR EACH ROW
DECLARE
    not_available EXCEPTION;
    numero_salle number;
BEGIN
        
    SELECT COUNT(*) INTO numero_salle
    FROM Reservation r
    WHERE  r.heureDebut <= :new.heureDebut
    AND r.heureFin >= :new.heureFin
    AND r.numero_salle = :new.numero_salle;
    
    IF (numero_salle > 0) THEN
    raise not_available;
    END IF;
    exception
    when not_available then raise_application_error(-20001, 'ROOM IS NOT AVAILABLE AT THIS TIME'); 
END;
/
-- Insertions
 
INSERT INTO Salle values (1, Collection_Reservation_t(),EquipementArray_t(
  Cardio_t(1,'Equipement cardio 1',10),
  Cardio_t(2,'Equipement cardio ',20)));
 insert into the (select s.collection_Reservation
                  from Salle s
                  where s.numero = 1)
              values (1,TO_DATE('10-JAN-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('11-JAN-16 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(s) from Salle s where s.numero = 1));                         
  insert into the (select s.collection_Reservation
                  from Salle s
                  where s.numero = 1)
              values (2,TO_DATE('14-AUG-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('15-JAN-16 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(s) from Salle s where s.numero = 1));                          
                          
                          
                          
INSERT INTO Salle values (2, Collection_Reservation_t(),EquipementArray_t(
  Fitness_t(3,'Mon appareil de fitness'),
  Fitness_t(4,'Un deuxieme appareil'),
  Musculation_t(5,'La musculation pour les nuls',10,200)));  

  
insert into the (select s.collection_Reservation
                  from Salle s
                  where s.numero = 2)
              values (3,TO_DATE('13-DEC-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('13-Dec-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(s) from Salle s where s.numero = 2));
                     
INSERT INTO Salle values (3, Collection_Reservation_t(),EquipementArray_t(
  Cardio_t(6,'Un deuxieme appareil',100),
  Musculation_t(7,'La musculation pour les nuls',10,200)));  
  insert into the (select s.collection_Reservation
                  from Salle s
                  where s.numero = 3)
              values (4,TO_DATE('15-DEC-15 10:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('15-Dec-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(s) from Salle s where s.numero =3));
  insert into the (select s.collection_Reservation
                  from Salle s
                  where s.numero = 3)
              values (5,TO_DATE('11-DEC-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('11-Dec-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(s) from Salle s where s.numero =3));      
                     
INSERT INTO Salle values (4, Collection_Reservation_t(),EquipementArray_t(
  Fitness_t(8,'Mon appareil de fitness'),
  Cardio_t(9,'Un deuxieme appareil',20),
  Musculation_t(10,'La musculation pour les nuls',10,300)));  
  insert into the (select s.collection_Reservation
                  from Salle s
                  where s.numero = 4)
              values (5,TO_DATE('10-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('10-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(s) from Salle s where s.numero = 4));
                                    
                          
 INSERT INTO Reservation values (16,TO_DATE('23-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('23-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),(select ref(s) from Salle s where s.numero =8));                         
                          
 INSERT INTO Reservation values (15,TO_DATE('10-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('23-Nov-16 12:00 A.M.','DD-MON-YY HH:MI A.M.'),(select ref(s) from Salle s where s.numero =7));  
                      
 INSERT INTO Reservation values (8,TO_DATE('23-Dec-15 09:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('23-Dec-15 10:00 A.M.','DD-MON-YY HH:MI A.M.'),(select ref(s) from Salle s where s.numero =2));                       
                    --   Insetion Seance
                                          
INSERT INTO Seance
values(1,TO_DATE('23-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),TO_DATE('23-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),  (select ref(d) from Coach d where d.idUser = 2));
INSERT INTO Seance 
values (2,TO_DATE('24-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),TO_DATE('24-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'), (select ref(d) from Coach d where d.idUser = 3));
INSERT INTO Seance 
values (3,TO_DATE('25-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),TO_DATE('24-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'), (select ref(d) from Coach d where d.idUser = 4 ));
INSERT INTO Seance
 values (4,TO_DATE('26-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),TO_DATE('26-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'), (select ref(d) from Coach d where d.idUser = 6));
INSERT INTO Seance 
values (6,TO_DATE('28-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),TO_DATE('28-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'), (select ref(d) from Coach d where d.idUser = 3));
INSERT INTO Seance 
values (7,TO_DATE('29-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),TO_DATE('29-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'), (select ref(d) from Coach d where d.idUser = 4 ));
INSERT INTO Seance
 values (8,TO_DATE('30-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),TO_DATE('30-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'), (select ref(d) from Coach d where d.idUser = 6));
            -- Insertion Coach
            
INSERT INTO Coach values (2,'Paul','Dupont','danse',Collection_Seance_t());
insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 2)
                values(5,TO_DATE('10-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('10-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 2));
insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 2)
                values(6,TO_DATE('01-Nov-12 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('01-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 2));

                insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 2)
                values(7,TO_DATE('25-MAR-12 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('25-MAR-12 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 2));
                 insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 2)
                values(8,TO_DATE('04-MAR-10 08:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('04-MAR-10 09:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 2));

                
                --Insertion Coache 2
                INSERT INTO Coach values (10,'Benois','Mirmi','RPM',Collection_Seance_t());
insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 10)
                values(10,TO_DATE('20-JAN-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('12-JAN-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 10));
insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 2)
                values(6,TO_DATE('01-JAN-12 02:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('01-JAN-15 03:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 2));

                insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 10)
                values(7,TO_DATE('01-JAN-12 04:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('25-JAN-12 06:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 10));
                 insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 10)
                values(8,TO_DATE('04-MAR-10 08:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('04-MAR-10 09:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 10));

                 insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 10)
                values(9,TO_DATE('25-NOV-15 06:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('25-NOV-15 07:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 10));

                insert into the (select c.liste_seance
                from Coach c
                where c.idUser = 10)
                values(10,TO_DATE('26-DEC-15 09:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('26-DEC-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(d) from Coach d where d.idUser = 10));    
     
  --                     Isertion Abonne
  
INSERT  INTO  Abonne values(3,'Jeremy','Ben',10,
  Inscriptions_varray_t(Inscription_t(TO_DATE('10-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),50)),
 Collection_Reservation_t(),Collection_Seance_t());



Insert into the (select a.liste_seance_abonne
                from Abonne a
                where a.idUser=3)
                    values (2,TO_DATE('14-AUG-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('15-JAN-16 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(c) from Coach c where c.idUser = 2));


Insert into  the(select r.liste_reservation
                from Abonne r 
                where r.idUser = 3)
                     values (5,TO_DATE('10-Nov-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('10-Nov-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(s) from Salle s where s.numero = 3));

---Insertion 2
INSERT  INTO  Abonne values(4,'Alamak','Celaeno',5,
  Inscriptions_varray_t(Inscription_t(TO_DATE('10-Jan-14 10:00 A.M.','DD-MON-YY HH:MI A.M.'),9)),
 Collection_Reservation_t(),Collection_Seance_t());



Insert into the (select a.liste_seance_abonne
                from Abonne a
                where a.idUser=4)
                    values (3,TO_DATE('01-AUG-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('01-AUG-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(c) from Coach c where c.idUser = 2));  


Insert into the(select r.liste_reservation
                from Abonne r 
                where r.idUser = 4)
                     values (5,TO_DATE('10-SEP-15 11:00 A.M.','DD-MON-YY HH:MI A.M.'),
                      TO_DATE('10-SEP-15 12:00 A.M.','DD-MON-YY HH:MI A.M.'),
                     (select ref(s) from Salle s where s.numero = 2));



-- Requêtes

-- selectionner tous les coach disponibles
select distinct c.idUser, c.nom, c.prenom
from Coach c, Table (c.liste_seance) l
where l.heureDebut > current_date
or (l.heureDebut < current_date and l.heureFin < current_date);


-- liste des salles qui ont plus de 2 equipements
select s.numero
from Salle s, Table (s.listeEquipements) l
group by s.numero
having count(l.id) > 2;


-- selectionner les abonnes ayant effectue au moins 1 reservation
select a.idUser, a.nom, a.prenom
from Abonne a, Table (a.liste_reservation) r
group by a.idUser, a.nom, a.prenom
having count(r.id) >= 1;


-- afficher la liste des coaches et les dates des seances
-- d un abonne donne 
select distinct s.heureDebut,c.nom 
from Abonne a, 
   Coach c, 
     table(c.liste_seance) s, 
     table(a.liste_seance_abonne) ab
where s.animateur.idUser= c.idUser 
and s.id = ab.id and a.nom = 'Ikbal'
group by s.heureDebut,c.nom;


-- afficher les coaches qui ont plus 
-- que deux  abonnes
select c.nom 
from Abonne a ,
   Coach c, 
     table(c.liste_seance) s,
     table(a.liste_seance_abonne) ab
where s.animateur.idUser = c.idUser 
and s.id = ab.id 
group by c.nom 
having count(s.animateur.idUser) >= 2;


-- selectionner la salle la plus utilisee  
select s.numero 
from Salle s, 
     table(s.Collection_Reservation) c,
     Reservation r
where c.id=r.id 
group by s.numero
having count(c.id)=(Select MAX(count (*)) 
from  reservation group by id );


-- Modifier la premiere inscription d un abonne a la date d aujourd hui
DECLARE
  I_VARRAY Inscriptions_varray_t;
BEGIN
  SELECT inscriptions
  INTO I_VARRAY
  FROM Abonne
  WHERE nom='Dupont';
  I_VARRAY(1).dateInscription := (SYSDATE);
  I_VARRAY(1).prix := 9;
  UPDATE Abonne
  SET inscriptions = I_VARRAY
  WHERE nom='Dupont';
END;
/

-- afficher toutes les inscriptions d'un abonne 
-- selon la date d'inscription
SELECT a.nom, a.prenom, i.*
FROM Abonne a, TABLE(a.inscriptions) i
GROUP BY i.dateInscription;


-- afficher la reduction possible pour l'abonne d'id 1
SELECT a.calculateDiscount() 
FROM Abonne a 
WHERE a.idUser = 3;


-- Utilisation des methodes pour afficher les prochaines
-- seances du coach 10, les equipements de la salle 2
-- et ses reservation le 13 decembre 2015
DECLARE
    toto Coach_t;
  salle Salle_t;
BEGIN
  SELECT VALUE(c) INTO toto FROM Coach c WHERE c.idUser=10;
  SELECT VALUE(s) INTO salle FROM Salle s WHERE s.numero = 2;
  dbms_output.put_line('======Prochaine Seance coach 10======');
  toto.listProchaineSeances;
  dbms_output.put_line('======Equipement salle 2======');
  salle.listEquipements;
  dbms_output.put_line('======Reservation salle 2 13 DEC 2015======');
  salle.reservationOn(TO_DATE('13-DEC-15','DD-MON-YY'));
END;
/
/
