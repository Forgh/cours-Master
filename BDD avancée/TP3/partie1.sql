DROP TYPE Adresse_t;

DROP TYPE Annee_t;

DROP TABLE Annee;

DROP TABLE Adresse;

DROP TABLE Personne;

DROP TYPE Personne_t;

DROP TABLE UE;

DROP TABLE Cursus;

DROP TYPE UE;






create or replace type Adresse_t as Object (
  numero NUMBER, 
  rue VARCHAR(50), 
  ville VARCHAR(50)
);
/

create or replace type Annee_t as Object(
  valeur number
);
/


create table Personne (
  num number,
  nom varchar(25),
  prenom varchar(25),
  adresse Adresse_t,
  anneeNaissance Annee_t,
  CONSTRAINT chk_annee CHECK  (anneeNaissance.valeur >= 1900 AND anneeNaissance.valeur <= 2007), 
  CONSTRAINT pk_personne_num PRIMARY KEY (num),
  CONSTRAINT un_personne_nom UNIQUE (nom)
);

INSERT INTO Personne VALUES ('12345678', 'Jean', 'Michel', Adresse_t('607', 'route de Montpellier', 'Montpellier'), Annee_t(1990));
INSERT INTO Personne VALUES ('12345679', 'Dupont', 'Jacques', Adresse_t('87', 'avenue de la pointe', 'Narbonne'), Annee_t(1987));
INSERT INTO Personne VALUES ('12345680', 'De Matteo', 'Lisa', Adresse_t('78', 'chemin St Hubert', 'Beziers'), Annee_t(1960));
INSERT INTO Personne VALUES ('12345681', 'Campmas', 'Mickael', Adresse_t('89', 'place de la Mairie', 'Nimes'), Annee_t(1980));

--TEST constraint num
INSERT INTO Personne VALUES ('12345681', 'Lyla', 'Sisi', Adresse_t('89', 'place de la Mairie', 'Nimes'), Annee_t(1980));

--TEST constraint annee
INSERT INTO Personne VALUES ('12345682', 'Lolo', 'Mickael', Adresse_t('89', 'place de la Mairie', 'Nimes'), Annee_t(1789));

--TEST constraint nom unique
INSERT INTO Personne VALUES ('12345683', 'Campmas', 'Mickael', Adresse_t('89', 'place de la Mairie', 'Nimes'), Annee_t(1989));



--Question 8
SELECT * FROM Personne;
SELECT nom, adresse FROM Personne;
-- ???  
SELECT nom, P.anneeNaissance.valeur FROM Personne P WHERE P.adresse.ville = 'Nimes';




--- Partie 2

DROP TABLE Personne;

create or replace type Personne_t as Object (
  num number,
  nom varchar(25),
  prenom varchar(25),
  adresse Adresse_t,
  anneeNaissance Annee_t
);
/

CREATE TABLE Personne OF Personne_t
(  
  CONSTRAINT chk_annee CHECK  (anneeNaissance.valeur >= 1900 AND anneeNaissance.valeur <= 2007), 
  CONSTRAINT pk_personne_num PRIMARY KEY (num),
  CONSTRAINT un_personne_nom UNIQUE (nom)
);


INSERT INTO Personne VALUES ('12345678', 'Jean', 'Michel', Adresse_t('607', 'route de Montpellier', 'Montpellier'), Annee_t(1990));
INSERT INTO Personne VALUES ('12345679', 'Dupont', 'Jacques', Adresse_t('87', 'avenue de la pointe', 'Narbonne'), Annee_t(1987));
INSERT INTO Personne VALUES ('12345680', 'De Matteo', 'Lisa', Adresse_t('78', 'chemin St Hubert', 'Beziers'), Annee_t(1960));
INSERT INTO Personne VALUES ('12345681', 'Campmas', 'Mickael', Adresse_t('89', 'place de la Mairie', 'Nimes'), Annee_t(1980));

--TEST constraint num
INSERT INTO Personne VALUES ('12345681', 'Lyla', 'Sisi', Adresse_t('89', 'place de la Mairie', 'Nimes'), Annee_t(1980));

--TEST constraint annee
INSERT INTO Personne VALUES ('12345682', 'Lolo', 'Mickael', Adresse_t('89', 'place de la Mairie', 'Nimes'), Annee_t(1789));

--TEST constraint nom unique
INSERT INTO Personne VALUES ('12345683', 'Campmas', 'Mickael', Adresse_t('89', 'place de la Mairie', 'Nimes'), Annee_t(1989));


--Question 8
SELECT * FROM Personne;
SELECT nom, adresse FROM Personne P;
SELECT ref(P) FROM Personne P;
SELECT nom, P.anneeNaissance.valeur FROM Personne P WHERE P.adresse.ville = 'Nimes';






--PARTIE 3
--RO ---> Pas demandé
DROP TYPE Cursus;

DROP TABLE CursusV;

DROP TABLE CursusT;

DROP TYPE CursusT_t;

DROP TYPE CursusV_t;

DROP TABLE UE;

DROP TYPE UE_collection_table_t;

DROP TYPE UE_collection_varray_t;

DROP TYPE UE_t;

--OR

create or replace type UE_t as Object (
  codeApo varchar(10),
  libelle varchar(25),
  nbHC number,
  nbHTDTP number
);
/

CREATE TABLE UE OF UE_t (
  CONSTRAINT pk_UE PRIMARY KEY (codeApo)
);


create or replace type UE_collection_varray_t as VARRAY(6) OF UE_t;
/

create or replace type UE_collection_table_t as Table OF UE_t;
/



--Type Cursus avec Varray
create or replace type CursusV_t as Object (
  libelle varchar(25),
  niveau varchar(2),
  liste_ue UE_collection_varray_t
);
/

--Type Cursus avec Table
create or replace type CursusT_t as Object (
  libelle varchar(25),
  niveau varchar(2),
  liste_ue UE_collection_table_t
);
/




--Question 3

--varray
INSERT INTO CursusV VALUES ('AIGLE','M1', UE_collection_varray_t(
                      UE_t('HMIN106M','BDA','15','36'),
                      UE_t('HMIN116','Reseau','10','40'),
                      UE_t('HMIN112M','SIBD','12','38')
                    )
);

INSERT INTO CursusV VALUES ('AIGLE','M2', UE_collection_varray_t(
                      UE_t('HMIN305','Complexite','20','0'),
                      UE_t('HMIN314','Web','10','10'),
                      UE_t('HMIN303','Genie logiciel','20','0')
                    )
);



--nested table
INSERT INTO CursusT VALUES ('AIGLE','M1', UE_collection_table_t());
INSERT INTO THE (SELECT c.liste_ue FROM CursusT c WHERE c.libelle = 'AIGLE' AND c.niveau = 'M1') 
  VALUES ('HMIN106M','BDA','15','36');
INSERT INTO THE (SELECT c.liste_ue FROM CursusT c WHERE c.libelle = 'AIGLE' AND c.niveau = 'M1') 
  VALUES ('HMIN116','Reseau','10','40');
INSERT INTO THE (SELECT c.liste_ue FROM CursusT c WHERE c.libelle = 'AIGLE' AND c.niveau = 'M1') 
  VALUES ('HMIN112M','SIBD','12','38');

INSERT INTO CursusT VALUES ('AIGLE','M2', UE_collection_table_t());
INSERT INTO THE (SELECT c.liste_ue FROM CursusT c WHERE c.libelle = 'AIGLE' AND c.niveau = 'M2') 
  VALUES ('HMIN305','Complexite','20','0');
INSERT INTO THE (SELECT c.liste_ue FROM CursusT c WHERE c.libelle = 'AIGLE' AND c.niveau = 'M2') 
  VALUES ('HMIN314','Web','10','10');
INSERT INTO THE (SELECT c.liste_ue FROM CursusT c WHERE c.libelle = 'AIGLE' AND c.niveau = 'M2') 
  VALUES ('HMIN303','Genie logiciel','20','0');  



--Question 4

--R1
SELECT *
FROM CursusV;

SELECT *
FROM CursusT;


--R2
Select u.codeApo
From CursusV c, Table (c.liste_ue) u
WHERE c.libelle = 'AIGLE' AND c.niveau= 'M1';

Select u.codeApo
From CursusT c, Table (c.liste_ue) u
WHERE c.libelle = 'AIGLE' AND c.niveau= 'M1';


--R3
Select c.libelle, u.codeApo
From CursusV c, Table (c.liste_ue) u
WHERE u.nbHC > 15;

Select c.libelle, u.codeApo
From CursusT c, Table (c.liste_ue) u
WHERE u.nbHC > 15;



-- Partie 4
DROP TABLE Etudiant;

DROP TYPE Etudiant_t;

DROP TABLE Personne;

DROP TYPE Personne_t;

CREATE OR REPLACE TYPE Personne_t as Object (
  num number,
  nom varchar(25),
  prenom varchar(25),
  adresse Adresse_t,
  anneeNaissance Annee_t
)NOT FINAL;
/

CREATE TABLE Personne OF Personne_t(
CONSTRAINT pk_personne PRIMARY KEY(num)
);

CREATE OR REPLACE TYPE Etudiant_t UNDER Personne_t(
  numCarte number,
  cursus REF CursusT_t,
  anneeInscri Annee_t
  )FINAL;
  /

CREATE TABLE Etudiant OF Etudiant_t(
  CONSTRAINT pk_etudiant PRIMARY KEY(num)
);



INSERT INTO Etudiant VALUES (
  '12345678', 
  'Jean', 
  'Michel', 
  Adresse_t('607', 'route de Montpellier', 'Montpellier'), 
  Annee_t(1990),
  21345675,
  (SELECT REF(C) FROM CursusT C WHERE C.niveau = 'M2' AND C.libelle ='AIGLE'),
  Annee_t(2012)
);

INSERT INTO Etudiant VALUES (
  '12345679', 
  'Dupont', 
  'Jacques', 
  Adresse_t('87', 'avenue de la pointe', 'Narbonne'), 
  Annee_t(1987),
  34664767,
  (SELECT REF(C) FROM CursusT C WHERE C.niveau = 'M2' AND C.libelle ='AIGLE'),
  Annee_t(2012)
);

INSERT INTO Etudiant VALUES (
  '12345680', 
  'De Matteo', 
  'Lisa', 
  Adresse_t('78', 'chemin St Hubert', 'Beziers'), 
  Annee_t(1960),
  3654764,
  (SELECT REF(C) FROM CursusT C WHERE C.niveau = 'M1' AND C.libelle ='AIGLE'),
  Annee_t(2011)
);

INSERT INTO Etudiant VALUES (
  '12345681', 
  'Campmas', 
  'Mickael', 
  Adresse_t('89', 'place de la Mairie', 'Nimes'),
  Annee_t(1980),
  4675859,
  (SELECT REF(C) FROM CursusT C WHERE C.niveau = 'M1' AND C.libelle ='AIGLE'),
  Annee_t(2011)
);




--R1
SELECT e.nom, e.anneeInscri, DEREF(e.cursus)
FROM Etudiant e;

--R2
SELECT u.codeApo, u.libelle
FROM Etudiant e, TABLE(e.cursus.liste_ue) u
WHERE e.nom = 'Campmas' AND e.prenom = 'Mickael';