-- declaration
create or replace package archiOracle
 as 




 -- definition corps du paquetage
create or replace package body archiOracle 
as 

-- function radians
function general_consultation ()
as 
begin
dbms_output.put_line('1.1');
dbms_output.put_line('1.2');
dbms_output.put_line('1.3');
dbms_output.put_line('1.4');




return;
end;


/*Retourne les parametres de la requette*/
select to_char(logon_time, 'DD/MM/YYYY HH24:MI:SS') , username, program, sql_text from v$session , v$sqlarea where v$session.sql_address = v$sqlarea.address order by username, program;

/*Retourne .....*/
select r.sql_id, disk_reads, elapsed_time, username from v$sql r, v$session s where s.sql_id = r.sql_id and type='USER';

/*Retourne les dernieres requette tapées et mise en cache*/
select parsing_schema_name, substr(sql_text,1,20) from v$sqlarea where parsing_schema_name ='IMOUGENOT';

/* Retourne info et consomation des dernieres requette tapées et mise en cache*/
select sql_FullText,(cpu_time/100000) "Cpu Time (s)", (elapsed_time/1000000) "Elapsed time (s)", fetches,buffer_gets,disk_reads,executions FROM v$sqlarea WHERE Parsing_Schema_Name ='IMOUGENOT' AND rownum <50 order by 3 desc;