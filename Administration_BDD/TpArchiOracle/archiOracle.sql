/*1. consulter la vue portant sur l’instance (v$instance) et répondre à des interrogations telles que : quel est le nom de l’hôte sur lequel tourne l’instance et depuis quand l’instance est démarrée,*/
select host_name, STARTUP_TIME from v$instance;

/*
2. consulter la vue portant sur la base de données (v$database) et répondre à des interrogations
telles que : quel est le nom de la base et exploite t’elle le mode archive,*/
select name, archivelog_change# from v$database;

/*
3. consulter la vue (v$option) portant sur les fonctionnalités du serveur de données et répondre à des interrogations telles que : de quelles options disposons-nous ?*/
select parameter, value from v$option;

/*
4. consulter la vue (v$version) portant sur la version du SGBD Oracle sous-jacent*/
select * from v$version;


-- 1.2
/*
1. consulter les vues portant sur la mémoire SGA (v$sga, v$sgainfo ou show SGA) et répondreà des interrogations telles que : quelles sont les tailles allouées à la mémoire partagée (sharedpool), au cache de données (data buffer cache) buffer de données, au tampon de journalisation(buffer redo) ? Exploiter aussi une commande de type show parameter shared pool size*/
select * from v$sgainfo where name='Redo Buffers' or name='Shared Pool Size' or name='Buffer Cache Size';


/*
2. Pour aller plus dans le détail, vous pouvez consulter la vue v$sgastat ;*/
 select * from v$sgastat;

/*
3. interroger les vues v$sql, v$sqlarea, v$sqltext qui sont associées à la ”library cache” (sharedpool) et identifiez les informations contenues dans la ”library cache” et donc son rôle.*/
Ok

/*
4. une requête vous est donnée pour identifier les processus d’arrière-plan interagissant avec lesstructures mémoire et les fichiers de données
Listing 2 – processus*/

select p.pid, bg.name, bg.description, p.program from v$bgprocess bg, v$process p
where bg.paddr = p.addr order by p.pid; 

/*
5. avoir la connaissance de divers paramètres portant sur l’instance : select name, value from v$parameter. Par exemple, trouver la taille du bloc de données qui va être l’unité d’échange montée en mémoire vive.
*/
select value from v$parameter where name='db_block_size';


-- 1.3

/* 1. consulter les tablespaces d´eﬁnis (dba tablespaces) */
select tablespace_name from  dba_tablespaces;
SYSTEM
SYSAUX
UNDOTBS1
TEMP
USERS

/* 2. consulter l’emplacement des ﬁchiers de donn´ees select name from v$dataﬁle; */
select name from v$dataﬁle;

/u01/app/oracle/oradata/master/system01.dbf
/u01/app/oracle/oradata/master/sysaux01.dbf
/u01/app/oracle/oradata/master/undotbs01.dbf
/u01/app/oracle/oradata/master/users01.dbf


/* 3. consulter l’emplacement des ﬁchiers journaux select name from v$logﬁle; */
select member from v$logfile;

/u01/app/oracle/oradata/master/redo03.log
/u01/app/oracle/oradata/master/redo02.log
/u01/app/oracle/oradata/master/redo01.log



/* 4. consulter l’emplacement des ﬁchiers de controˆle select name from v$controlﬁle; */
select name from v$controlfile;

/u01/app/oracle/oradata/master/control01.ctl
/u01/app/oracle/flash_recovery_area/master/control02.ctl


/* 5. faire le lien entre espace de table et ﬁchier de donn´ees : select tablespace name, ﬁle name from dba data ﬁles; */
select tablespace_name, file_name from dba_data_files;

USERS
/u01/app/oracle/oradata/master/users01.dbf
UNDOTBS1
/u01/app/oracle/oradata/master/undotbs01.dbf
SYSAUX
/u01/app/oracle/oradata/master/sysaux01.dbf
SYSTEM
/u01/app/oracle/oradata/master/system01.dbf


-- 1.4
/*Vous construirez différentes fonctions permettant de disposer d’informations sur les activités d’un usager en particulier et sur les requêtes les plus coûteuses. Vous pouvez aussi tester tout l’intérêt des variables liées.*/

/*--------------------*/
/*Exemple de requettes*/
/*--------------------*/

/*Retourne les parametres de la requette*/
select to_char(logon_time, 'DD/MM/YYYY HH24:MI:SS') , username, program, sql_text from v$session , v$sqlarea where v$session.sql_address = v$sqlarea.address order by username, program;

/*Retourne les caractéristiques des requêtes en cache pour l'utilisateur de cette session*/
select r.sql_id, disk_reads, elapsed_time, username from v$sql r, v$session s where s.sql_id = r.sql_id and type='USER';

/*Retourne les dernieres requette tapées et mise en cache*/
select parsing_schema_name, substr(sql_text,1,20) from v$sqlarea where parsing_schema_name ='ALAMANT';

/* Retourne info et consomation des dernieres requette tapées et mise en cache*/
select sql_FullText,(cpu_time/100000) "Cpu Time (s)", (elapsed_time/1000000) "Elapsed time (s)", fetches,buffer_gets,disk_reads,executions FROM v$sqlarea WHERE Parsing_Schema_Name ='ALAMANT' AND rownum <50 order by 3 desc;

/*Test  plus lourdes requetes en temps d'execution pour un utilisateur donnée*/
select sql_FullText,(cpu_time/100000) "Cpu Time (s)", (elapsed_time/1000000) "Elapsed time (s)", fetches,buffer_gets,disk_reads,executions FROM v$sqlarea WHERE Parsing_Schema_Name ='ALAMANT' AND rownum <5 order by 3 desc;

-- 1.5.1
/*Vérifiez que que toutes vos tables sont situées dans le même tablespace et qu’il en va de même pour les autres schémas utilisateurs.*/
/*En mode feignasse*/
select TABLESPACE_NAME from user_tables;

/*En mode prise de tete*/
CREATE OR REPLACE PROCEDURE areAllTablesInSameTablespace AS
tablespace_name varchar(128);
count_t integer;
BEGIN
  SELECT u.TABLESPACE_NAME INTO tablespace_name FROM USER_tables u WHERE rownum = 1;
  SELECT COUNT(DISTINCT u.TABLESPACE_NAME) into count_t FROM USER_tables u WHERE u.TABLESPACE_NAME != tablespace_name;
  
  IF count_t=0 THEN
   dbms_output.put_line('Les tables sont toute dans le meme tablespace_name');

ELSE
   dbms_output.put_line('Les tables ne sont pas toutes dans le meme tablespace_name');
END IF;

END;
/
show errors;

-- 1.5.2
/* Combien de blocs de données sont utilisés pour contenir les données de chacune de vos tables ? Créer la table test(num char(3), commentaire char(97)), et appliquer sur num une contrainte de domaine : nombres entiers compris entre 0 et 999*/
CREATE TABLE TEST (
  num integer NOT NULL,
  commentaire char(97),
  Constraint chk_test Check (num>=0 and num<1000)
);

CREATE OR REPLACE PROCEDURE question1523
AS
loop_counter integer;
blocks integer;

BEGIN

EXECUTE IMMEDIATE('analyze table TEST compute statistics');

select blocks INTO blocks from user_tables where table_name='TEST';
dbms_output.put_line('Block' || blocks);

FOR loop_counter IN  1..50
LOOP
   INSERT INTO TEST VALUES (loop_counter, 'test');
END LOOP;

EXECUTE IMMEDIATE('analyze table TEST compute statistics');

select blocks INTO blocks from user_tables where table_name='TEST';
dbms_output.put_line('Block' || blocks);

FOR loop_counter IN  1..100
LOOP
   INSERT INTO TEST VALUES (loop_counter, 'test');
END LOOP;

EXECUTE IMMEDIATE('analyze table TEST compute statistics');

select blocks INTO BLOCKS from user_tables where table_name='TEST';
dbms_output.put_line('Block' || blocks);
END;
/
show errors;     