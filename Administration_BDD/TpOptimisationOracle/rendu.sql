-- Exercice n°1. 

select codeinsee from commune ;

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 681562032

-----------------------------------------------------------------------------
Id | Operation            | Name       | Rows  | Bytes | Cost (%CPU)| Time     | 
--------------------------------------------------------------------------------
 0 | SELECT STATEMENT     |            | 36318 |   177K|    24   (0)| 00:00:0 1|
 1 | INDEX FAST FULL SCAN | COMMUNE_PK | 36318 |   177K|    24   (0)| 00:00:0 1|


-- 2.

select nom_Com from commune ;

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 1326938554

-----------------------------------------------------------------------------
| Id  | Operation         | Name    | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |         | 36318 |  425K |  581   (1) | 00:00:07 |
|   1 |  TABLE ACCESS FULL| COMMUNE | 36318 |  425K |  581   (1) | 00:00:07 |
-----------------------------------------------------------------------------


-- 3. 

select nom_Com, codeinsee from commune ;

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 1326938554

-----------------------------------------------------------------------------
| Id  | Operation         | Name    | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |         | 36318 |  602K |  581   (1) | 00:00:07 |
|   1 | TABLE ACCESS FULL | COMMUNE | 36318 |  602K |  581   (1) | 00:00:07 |
-----------------------------------------------------------------------------


-- 4. 

select nom_Com from commune where codeinsee='34192' ;

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 181619738

------------------------------------------------------------------------------------------
| Id  | Operation                   | Name       | Rows  | Bytes | Cost (%CPU)|   Time   |
------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT            |            |     1 |    17 |     2   (0)| 00:00:01 | 
|   1 | TABLE ACCESS BY INDEX ROWID | COMMUNE    |     1 |    17 |     2   (0)| 00:00:01 |
|*  2 | INDEX UNIQUE SCAN           | COMMUNE_PK |     1 |       |     1   (0)| 00:00:01 |
-------------------------------------------------------------------------------------------

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Predicate Information (identified by operation id):
---------------------------------------------------
2 - access("CODEINSEE"='34192')


-- 5. 

select nom_Com from commune where codeinsee like '34%' ;

PLAN_TABLE_OUTPUT
--------------------------------
Plan hash value: 797486710

--------------------------------------------------------------------------------------------
| Id  | Operation                    | Name       | Rows  | Bytes | Cost (%CPU) | Time     | 
--------------------------------------------------------------------------------------------
| 0   | SELECT STATEMENT             |            |    17 |   289 |     5   (0) | 00:00:01 | 
| 1   |  TABLE ACCESS BY INDEX ROWID | COMMUNE    |    17 |   289 |     5   (0) | 00:00:01 |
|*  2 |   INDEX RANGE SCAN           | COMMUNE_PK |    17 |       |     2   (0) | 00:00:01 |
--------------------------------------------------------------------------------

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Predicate Information (identified by operation id):
---------------------------------------------------
2 - access("CODEINSEE" LIKE '34%')
    filter("CODEINSEE" LIKE '34%')

-- 6. 
explain plan for select nom_Com from commune where codeinsee like '%392' ;


PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 1326938554

-----------------------------------------------------------------------------
| Id  | Operation         | Name    | Rows  | Bytes | Cost (%CPU)| Time     | 
-----------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |         |  1816 | 30872 |  581   (1) | 00:00:07 |
|*  1 |  TABLE ACCESS FULL| COMMUNE |  1816 | 30872 |  581   (1) | 00:00:07 |
-----------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------
PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
1 - filter("CODEINSEE" LIKE '%392')

-- 7. 
explain plan for select nom_Com from commune where codeinsee >= 34 ;


PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 1326938554

-----------------------------------------------------------------------------
| Id  | Operation         | Name    | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------
|   0 | SELECT STATEMENT  |         |  1816 | 30872 |  581   (1) | 00:00:07  |
|*  1 |  TABLE ACCESS FULL| COMMUNE |  1816 | 30872 |  581   (1) | 00:00:07  |
-----------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------
PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
1 - filter(TO_NUMBER("CODEINSEE")>=34)


-- 8. 

explain plan for select nom Com from commune where code insee in (’09330’,’09331’,’09332’,’09334’) ;

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 2135421030

-----------------------------------------------------------------------------------------
| Id  | Operation                     | Name       | Rows | Bytes | Cost (%CPU)|  Time  |
----------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT              |            |  4   |    68 |  6   (0)|  00:00:01 |
|   1 |  INLIST ITERATOR              |            |      |       |         |           |
|   2 |  TABLE ACCESS BY INDEX ROWID  | COMMUNE    |  4   |    68 |  6   (0)|  00:00:01 |
|*  3 |  INDEX UNIQUE SCAN            | COMMUNE_PK |  4   |       |  5   (0)|  00:00:01 |

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Predicate Information (identified by operation id):
---------------------------------------------------

3 - access("CODEINSEE"='09330' 
           OR "CODEINSEE"='09331' 
           OR "CODEINSEE"='09332'
           OR "CODEINSEE"='09334')
           
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- Avec desactivation de l'index sur codeInsee 
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SQL> explain plan for select /*+ NO_INDEX(Commune) */ nom_Com from commune where codeinsee in ('09330','09331','09332','09334') ;

PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 1326938554

-----------------------------------------------------------------------------
| Id  | Operation          | Name    | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------
|   0 | SELECT STATEMENT   |         |    4 |   68 |  581   (1)| 00:00:07 |
|*  1 |  TABLE ACCESS FULL | COMMUNE |    4 |   68 |  581   (1)| 00:00:07 |
-----------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------
PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
   1 - filter("CODEINSEE"='09330' 
              OR "CODEINSEE"='09331' 
              OR "CODEINSEE"='09332' 
              OR "CODEINSEE"='09334')
           
-- Exercice n°2. 


-- Exercice n°3. 



-- 1 
-- Jointure
SELECT c.nom_com, c.latitude, c.longitude FROM Commune c, Departement d WHERE c.reg = d.reg AND (d.nom_dep = 'GARD' OR d.nom_dep = 'HERAULT'); 

-- Version in
SELECT c.nom_com, c.latitude, c.longitude FROM Commune c where c.reg IN (SELECT d.reg from  Departement d WHERE d.nom_dep = 'GARD' or d.nom_dep = 'HERAULT'); 

-- Version exists
SELECT c.nom_com, c.latitude, c.longitude FROM Commune c WHERE EXISTS (SELECT * FROM Departement d WHERE c.reg = d.reg AND (d.nom_dep = 'GARD' OR d.nom_dep = 'HERAULT'));
    

-- Test avec set timming
-- Jointure
Elapsed: 00:00:00.33

-- Version IN
Elapsed: 00:00:00.15

-- Version EXIST
Elapsed: 00:00:00.15


-- Test avec explain plan
-- Jointure
PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 174368243

----------------------------------------------------------------------------------
| Id  | Operation          | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
----------------------------------------------------------------------------------
|   0 | SELECT STATEMENT   |             |  3302 |   125K|   584   (1)| 00:00:08 |
|*  1 |  HASH JOIN         |             |  3302 |   125K|   584   (1)| 00:00:08 |
|*  2 |   TABLE ACCESS FULL| DEPARTEMENT |     2 |    28 |     3   (0)| 00:00:01 |
|   3 |   TABLE ACCESS FULL| COMMUNE     | 36318 |   886K|   581   (1)| 00:00:07 |
----------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------
   1 - access("C"."REG"="D"."REG")
   2 - filter("D"."NOM_DEP"='GARD' OR "D"."NOM_DEP"='HERAULT')


-- Version IN
PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 1911161922

--------------------------------------------------------------------------------------
| Id  | Operation             | Name        | Rows  | Bytes | Cost (%CPU)| Time      |
--------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT      |             |  3302 |   125K|   584   (1)| 00:00: 08 |
|*  1 |  HASH JOIN RIGHT SEMI |             |  3302 |   125K|   584   (1)| 00:00: 08 |
|*  2 |   TABLE ACCESS FULL   | DEPARTEMENT |   2   |  28   |   3   (0)  | 00:00: 01 |
|   3 |   TABLE ACCESS FULL   | COMMUNE     | 36318 |   886K|   581   (1)| 00:00: 07 |

--------------------------------------------------------------------------------------
Predicate Information (identified by operation id):
---------------------------------------------------
   1 - access("C"."REG"="D"."REG")
   2 - filter("D"."NOM_DEP"='GARD' OR "D"."NOM_DEP"='HERAULT')


-- Version EXIST
PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 1911161922

-------------------------------------------------------------------------------------
| Id  | Operation            | Name        | Rows  | Bytes | Cost (%CPU)| Time      |
-------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT     |             |  3302 |   125K|   584   (1)| 00:00: 08 |
|*  1 |  HASH JOIN RIGHT SEMI|             |  3302 |   125K|   584   (1)| 00:00: 08 |
|*  2 |   TABLE ACCESS FULL  | DEPARTEMENT |   2   |  28   |   3     (0)| 00:00: 01 |
|   3 |   TABLE ACCESS FULL  | COMMUNE     | 36318 |   886K|   581   (1)| 00:00: 07 |
-------------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------
   1 - access("C"."REG"="D"."REG")
   2 - filter("D"."NOM_DEP"='GARD' OR "D"."NOM_DEP"='HERAULT')

-- Quelle est l’écriture qui vous semble la moins coûteuse ?

/*
D'apresles test appliqué aux requêtes via la commande "set timming" on note que les versions IN et EXIST sont les plus rapides, puis la version avec jointure est bien plus couteuse en terme de temps (environ 2x plus couteuse) Neanmoins ce résultat depend de la charge réseau
Les requette avec EXIST et IN propose une SEMI jointure contrairement a la version simple qui fait une jointure compléte (cf HASH JOIN RIGHT SEMI vs HASH JOIN RIGHT)
*/

-- Quels sont les opérateurs physiques exploités respectivement pour exprimer la jointure ? Exploiter les directives (hint) pour forcer le choix d’un opérateur (par exemple use nl).

/*
Version Jointure: On note la présence de l'opérateur :  HASH 
Version IN : On note la présence de de l'opérateur : HASH SEMI (semi jointure)
Version EXIST : On note la présence de des opérateurs : HASH SEMI (semi jointure)

On peut donc dire que les version IN et EXIST sont les moins couteuse car elles ne font intervernir qu'une jointure partielle contrairement à la version simple qui fait intervenir une jointure complete.
*/

-- Construisez sur papier un arbre algébrique puis les plans physiques correspondant à chaque plan d’exécution choisi.

-- 2
Utiliser la commande suivante pour recuperer la bonne version de la tabe Commune :
create table Commune2 as select * from imougenot.Commune;  

-- 3
-- Version jointure
SELECT c.nom_com, d.nom_dep, r.nom_reg from Commune c, Departement d, Region r where c.reg = d.reg and d.reg = r.reg and (r.nom_reg = 'LANGUEDOC-ROUSSILLON' or r.nom_reg = 'PROVENCE-ALPES-COTE D''AZUR' or r.nom_reg ='MIDI-PYRENEES');

-- Verison UNION
SELECT c.nom_com, d.nom_dep, r.nom_reg from Commune c, Departement d, Region r where c.reg = d.reg and d.reg = r.reg AND r.nom_reg = 'LANGUEDOC-ROUSSILLON' UNION SELECT c.nom_com, d.nom_dep, r.nom_reg from Commune c, Departement d, Region r where c.reg = d.reg and d.reg = r.reg AND r.nom_reg = 'PROVENCE-ALPES-COTE D''AZUR' UNION SELECT c.nom_com, d.nom_dep, r.nom_reg from Commune c, Departement d, Region r where c.reg = d.reg and d.reg = r.reg AND r.nom_reg = 'MIDI-PYRENEES';

-- Test avec explain plan
-- Version jointure
PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 3024601900

-------------------------------------------------------------------------------------
| Id  | Operation           | Name        | Rows  | Bytes | Cost (%CPU) | Time      |
-------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT    |             | 15095 |   663K|   588   (1) | 00:00:0 8 |
|*  1 |  HASH JOIN          |             | 15095 |   663K|   588   (1) | 00:00:0 8 |
|*  2 |   HASH JOIN         |             |    11 |   341 |  7      (15)| 00:00:0 1 |
|*  3 |    TABLE ACCESS FULL| REGION      |  3    |    51 |  3      (0) | 00:00:0 1 |
|   4 |    TABLE ACCESS FULL| DEPARTEMENT |   101 |  1414 |  3      (0) | 00:00:0 1 |
|   5 |   TABLE ACCESS FULL | COMMUNE     | 36318 |   496K|   581   (1) | 00:00:0 7 |
-------------------------------------------------------------------------------------

-----------------------------------------------------
Predicate Information (identified by operation id):
---------------------------------------------------
   1 - access("C"."REG"="D"."REG")
   2 - access("D"."REG"="R"."REG")
   3 - filter("R"."NOM_REG"='LANGUEDOC-ROUSSILLON' 
              OR "R"."NOM_REG"='MIDI-PYRENEES' 
              OR "R"."NOM_REG"='PROVENCE-ALPES-COTE-D''AZUR')

-- Version Union
PLAN_TABLE_OUTPUT
--------------------------------------------------------------------------------
Plan hash value: 255703522

-------------------------------------------------------------------------------------------------------
| Id  | Operation                | Name               | Rows  | Bytes |TempSpc| Cost (%CPU)| Time     |
-------------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT         |                    | 18239 |   891K|       |  1356  (57)| 00:00:17 |
|   1 |  SORT UNIQUE             |                    | 18239 |   891K|   792K|  1356  (57)| 00:00:17 |
|   2 |   UNION-ALL              |                    |       |       |       |            |          |
|*  3 |    HASH JOIN             |                    |  5032 |   221K|       |   587   (1)| 00:00:08 |
|   4 |     TABLE ACCESS FULL    | DEPARTEMENT        |   101 |  1414 |       |     3   (0)| 00:00:01 |
|   5 |     MERGE JOIN CARTESIAN |                    | 36318 |  1099K|       |   584   (1)| 00:00:08 |
|*  6 |      TABLE ACCESS FULL   | REGION             |     1 |    17 |       |     3   (0)| 00:00:01 |
|   7 |      BUFFER SORT         |                    | 36318 |   496K|       |   581   (1)| 00:00:07 |
|   8 |       TABLE ACCESS FULL  | COMMUNE            | 36318 |   496K|       |   581   (1)| 00:00:07 |
|*  9 |    HASH JOIN             |                    | 13207 |   670K|       |   594   (1)| 00:00:08 |
|  10 |     VIEW                 | VW_JF_SET$B0FF87A0 |     8 |   304 |       |    13   (8)| 00:00:01 |
|  11 |      SORT UNIQUE         |                    |     8 |   248 |       |    13  (54)| 00:00:01 |
|  12 |       UNION-ALL          |                    |       |       |       |            |          |
|* 13 |        HASH JOIN         |                    |     4 |   124 |       |     7  (15)| 00:00:01 |
|* 14 |   TABLE ACCESS FULL      | REGION             |     1 |    17 |       |     3   (0)| 00:00:01 |
|  15 |   TABLE ACCESS FULL      | DEPARTEMENT        |   101 |  1414 |       |     3   (0)| 00:00:01 |
|* 16 |        HASH JOIN         |                    |     4 |   124 |       |     7  (15)| 00:00:01 |
|* 17 |   TABLE ACCESS FULL      | REGION             |     1 |    17 |       |     3   (0)| 00:00:01 |
|  18 |   TABLE ACCESS FULL      | DEPARTEMENT        |   101 |  1414 |       |     3   (0)| 00:00:01 |
|  19 |     TABLE ACCESS FULL    | COMMUNE            | 36318 |   496K|       |    581  (1)| 00:00:07 |
---------------------------------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------
   3 - access("C"."REG"="D"."REG" AND "D"."REG"="R"."REG")
   6 - filter("R"."NOM_REG"='MIDI-PYRENEES')
   9 - access("C"."REG"="ITEM_1")
  13 - access("D"."REG"="R"."REG")
  14 - filter("R"."NOM_REG"='LANGUEDOC-ROUSSILLON')
  16 - access("D"."REG"="R"."REG")
  17 - filter("R"."NOM_REG"='PROVENCE-ALPES-COTE D''AZUR')


-- Conslusion

/*
On note une différence flagrante entre la jointure basique et les union.
Les Union produisent 19 opérations contre seulement 5 pour la jointure traditionnelle.
Ces opération produise une éplosion de l'utilisation CPU en mode Union.

Jointure Elapsed: 00:00:08.74
UNION Elapsed: 00:00:08.80
Au niveau temps d'éxecution on se retrouve avec un temps equivalent

On peut donc conclure que dans le cas de jointure multiples la methode traditionnelle est plus efficase que la méthode utilisant les UNION
dans le cas precis de notre jeu de données.
*/
