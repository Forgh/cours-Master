-- Exercice 2.2.1
-- Non fonctionnelle
CREATE OR REPLACE FUNCTION ToutesTables (sch IN VARCHAR2) RETURN CLOB
  IS  infos CLOB;
      info_c CLOB;
      CURSOR cur
      IS
        SELECT TABLE_NAME FROM user_tables;
        
        BEGIN 
          OPEN cur;
          if cur%notfound then
            return 'NOT FOUND';
          end if;
          
          LOOP
            FETCH cur INTO info_c;
            dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SQLTERMINATOR',true);
            SELECT dbms_metadata.get_ddl('TABLE', info_c, sch) into infos FROM user_tables;
          END LOOP;
    CLOSE cur;
    return infos;
  END;
/
show errors;


-- Exercice 2.2.2
-- Non faite
CREATE OR REPLACE FUNCTION ToutesTablesInfos (sch IN VARCHAR2) RETURN CLOB
  IS  infos-plus CLOB;
  BEGIN
  
      return infos-plus;
  END;
/
show errors;


-- Exercice 3.1.1
CREATE OR REPLACE FUNCTION TableXML (tb_name IN VARCHAR2) RETURN CLOB
  IS 
  description CLOB;
  BEGIN 
    SELECT DBMS_METADATA.GET_XML ('TABLE',tb_name) INTO description FROM dual;
    return description;
  END TableXML;
  /
  show errors;
    
-- Exercice 3.1.2
CREATE OR REPLACE FUNCTION TableDataXML (tb_name IN VARCHAR2, where_clause in VARCHAR2) RETURN CLOB
  IS 
  tree CLOB;
  BEGIN 
    SELECT DBMS_XMLGEN.GETXML(dbms_xmlgen.newcontext ('select * from '|| tb_name || ' WHERE '|| where_clause) ) INTO tree from dual;
    return tree;
  END TableDataXML;
  /
  show errors;


