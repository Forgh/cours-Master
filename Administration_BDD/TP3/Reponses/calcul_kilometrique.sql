-- Fonction de calcul kilometrique
create or replace function radians (valeurEnDegre in float) return float
as 
begin
  return (valeurEnDegre/57.295779513082);
end;
/
show errors;

CREATE OR REPLACE FUNCTION kilo_calc (lat_a IN FLOAT, lat_b IN FLOAT, long_a IN FLOAT, long_b IN FLOAT) RETURN FLOAT
IS
BEGIN
    RETURN 6366*acos(cos(radians(lat_a))*cos(radians(lat_b))*cos(radians(long_b)-radians(long_a))+sin(radians(lat_a))*sin(radians(lat_b)));
exception
    when others then dbms_output.put_line('Others error');
END kilo_calc;
/
show errors;


CREATE OR REPLACE FUNCTION calcul_distance_entre (communeA IN varchar, communeB IN varchar) RETURN FLOAT 
IS
-- ret FLOAT;
-- sql_stmtA  VARCHAR2(200);
-- sql_stmtB  VARCHAR2(200);
longA float;
latA float;
longB float;
latB float;
                                                
BEGIN
SELECT LONGITUDE, LATITUDE INTO longA, latA FROM Commune WHERE nom_com = communeA;
SELECT LONGITUDE, LATITUDE INTO longB, latB FROM Commune WHERE nom_com = communeB;
dbms_output.put_line(longA||' '||latA||' '||longB||' '||latB);
IF longA = null 
  then raise_application_error(-20100,'Colonne longitude de '|| communeA || ' nulle ou commune inexistante');
END IF;

IF latA = null 
  then raise_application_error(-20100,'Colonne latitude de '|| communeA || ' nulle ou commune inexistante');
END IF;
  
IF longB = null 
  then raise_application_error(-20100,'Colonne longitude de '|| communeB || ' nulle ou commune inexistante');
END IF;
  
IF latB = null 
  then raise_application_error(-20100,'Colonne latitude de '|| communeB || ' nulle ou commune inexistante');
END IF;

RETURN kilo_calc(latA,longA,latB,longB);
END calcul_distance_entre ;
/
show errors;