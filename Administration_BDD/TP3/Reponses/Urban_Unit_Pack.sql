-- declaration
create or replace package UrbanUnit
 as 
 function radians (valeurEnDegre in float) return float; 
 function kilo_calc (lat_a IN FLOAT, lat_b IN FLOAT, long_a IN FLOAT, long_b IN FLOAT) RETURN FLOAT; 
 function calcul_distance_entre (communeA IN varchar, communeB IN varchar) RETURN FLOAT; 
end UrbanUnit ; 
 / 


-- definition corps du paquetage
create or replace package body UrbanUnit 
as 

-- function radians
function radians (valeurEnDegre in float)
return float as 
begin
  return (valeurEnDegre/57.295779513082);
end;

-- fonction kilo_calc
function kilo_calc (lat_a IN FLOAT, lat_b IN FLOAT, long_a IN FLOAT, long_b IN FLOAT)
RETURN FLOAT is
BEGIN
    RETURN 6366*acos(cos(radians(lat_a))*cos(radians(lat_b))*cos(radians(long_b)-radians(long_a))+sin(radians(lat_a))*sin(radians(lat_b)));
exception
    when others then dbms_output.put_line('Others error');
END;

-- fonction calcul_distance_entre
function calcul_distance_entre (communeA IN varchar, communeB IN varchar) 
RETURN FLOAT is 
longA float;
latA float;
longB float;
latB float;                                      
BEGIN
SELECT LONGITUDE, LATITUDE INTO longA, latA FROM Commune WHERE nom_com = communeA;
SELECT LONGITUDE, LATITUDE INTO longB, latB FROM Commune WHERE nom_com = communeB;

RETURN kilo_calc(latA,latB,longA,longB);
exception
    when others then dbms_output.put_line('calc_dist error');
END;

end UrbanUnit; 
/
show errors;


-- tests possibles
-- select nbPopForDep('HERAULT') as NBPOP from DUAL;
select UrbanUnit.calcul_distance_entre('MONTPELLIER','BEZIERS') as MTP_BEZIERS FROM DUAL;
-- select UrbanUnit.calcul_distance_entre('MONTPELLIER','LUNEL') as Dist_MTPLUNEL FROM Commune;

desc UrbanUnit
drop package body UrbanUnit;
drop package UrbanUnit;