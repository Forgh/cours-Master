

create or replace PROCEDURE insert_population (annee in integer) IS
   TYPE tabColTyp IS REF CURSOR;  -- define weak REF CURSOR type
   tv_cv   tabColTyp;  -- declare cursor variable
   col_val float;
   col_name varchar(10);
   codeInsee varchar(6);	
   i integer;
   
BEGIN
   col_name := 'pop_'||annee;	
   i := 0;	
   OPEN tv_cv FOR  -- open cursor variable
      'SELECT  codeInsee, '|| col_name ||' FROM commune ';
LOOP
   FETCH tv_cv INTO codeInsee, col_val ;  -- fetch next row
   EXIT WHEN tv_cv%NOTFOUND;  -- exit loop when last row is fetched
   --DBMS_OUTPUT.PUT_LINE('Record: ' || codeInsee ||'  '||annee||' '|| col_val);
   insert into population values (codeInsee, annee, col_val);	
   i := i + 1;
END LOOP;
	DBMS_OUTPUT.PUT_LINE('compteur: ' || i);
      CLOSE tv_cv;
END;
/
show errors;
-- exemple utilisation
-- exec  insert_population(1976)


--declare 
--compteur number;
--begin
--for compteur in 1975..2010
--loop
--insert_population (compteur);
--end loop;
--end;
--/

create or replace PROCEDURE repet_insert_population (anneeMin in integer, anneeMax in integer)
is 
i integer;
begin
for i in anneeMin..anneeMax
loop
insert_population (i);
-- commit pour vider les rollback segments ?
commit;
end loop;
end;
/
show errors;
-- exemple utilisation
-- exec repet_insert_population (1975, 1976)

