create or replace PROCEDURE drop_column_commune (anneeMin in integer, anneeMax in integer)
is 
i integer;
sql_stmt  VARCHAR2(200);
begin
for i in anneeMin..anneeMax
loop
sql_stmt := 'ALTER TABLE Commune DROP COLUMN pop_' || TO_CHAR(i);
DBMS_OUTPUT.PUT_LINE(sql_stmt);
EXECUTE IMMEDIATE sql_stmt;

-- commit pour vider les rollback segments ?
commit;
end loop;
end;
/
show errors;