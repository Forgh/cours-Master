set serveroutput on;


--Quelques exemples....
create or replace trigger triggerDelete
	before delete on Region
	for each row
	begin
		if (to_char(Sysdate, 'DY')!='FRI')
			then raise_application_error(-20100,'interdiction');
		end if;
	end;
	/
	show errors;


CREATE OR REPLACE FUNCTION conversion (sommet in float) 
	RETURN FLOAT AS sommeE float;
	BEGIN
		sommeE := sommet/65;
		return sommeE;
	end;
	/
	show errors;
DECLARE compteur number;
BEGIN 
	FOR compteur in 1..10 LOOP
		dbms_output.put_line('message');
	END LOOP;
END;
/
	show errors;


DECLARE 
			conv float;
BEGIN 
	conv := conversion(100);
	dbms_output.put_line('resultat ' || conv);
END; 
/
	show errors;


--2.1
create or replace trigger checkPop
	before insert or update on Commune
	for each row
	begin
		if :new.pop_2010 < 0
			then raise_application_error(-20100,'Population negative');
		end if;
	end;
	/
	show errors;

--2.2
CREATE OR REPLACE PROCEDURE JoursEtHeuresOuvrables IS

	BEGIN
		if (to_char(sysdate,'DY')='SAT') or (to_char(sysdate,'DY')='SUN') THEN
raise_application_error(-20010, 'Modification interdite le '||to_char(sysdate,'DAY') ) ;
END IF;
	end;
	/
	show errors;


create or replace trigger Ouvrable
before delete or insert or update on Region
begin
JoursEtHeuresOuvrables();
end ;
/
	show errors;

--2.3
CREATE TABLE history (
	dateOperation DATE,
	nomUsager varchar2(15),
	typeOperation varchar2(25),
	CONSTRAINT pk_history PRIMARY KEY (dateOperation,nomUsager));

create or replace trigger historyRegion
	before insert or update or delete on Region
	for each row
	begin
	 IF INSERTING THEN
      INSERT INTO history VALUES (sysdate,user,'INSERT INTO Region');
	end if;
    IF UPDATING THEN
      INSERT INTO history VALUES (sysdate,user,'UPDATING ON Region');
	end if;
    IF DELETING THEN
      INSERT INTO history VALUES (sysdate,user,'DELETE FROM Region');
	end if;
	end;
	/
	show errors;


--2.4
create or replace trigger cascade 
	before delete or update on Region
	for each row
	begin
		if deleting then
			DELETE FROM Departement WHERE reg = :old.reg;
		end if;
		if updating then 
			UPDATE Departement SET reg = :new.reg WHERE reg = :old.reg;
		end if;
	END;
	/
	show errors;


--2.1.1
declare 
nPop2010 commune.pop_2010%type;

begin
Select SUM(c.pop_2010) into nPop2010 FROM Commune c, Departement d WHERE d.nom_dep = 'HERAULT' AND c.dep = d.dep;
dbms_output.put_line('sum pop :' || nPop2010);
end ;
/
show errors;

--2.1.2
declare cursor explicit_pop2010 is
Select SUM(c.pop_2010), d.nom_dep FROM Commune c, Departement d WHERE c.dep = d.dep GROUP BY d.nom_dep;
nPop2010 commune.pop_2010%type;
nDep departement.nom_dep%type;

begin
open explicit_pop2010;
loop
	fetch explicit_pop2010 into nPop2010, nDep;
	exit when explicit_pop2010%notfound;
	dbms_output.put_line('Department '|| nDep ||', sum pop :' || nPop2010);
end loop;
close explicit_pop2010;
end ;
/
show errors;


--2.2.1



--2.3.1
CREATE OR REPLACE FUNCTION nbPopForDep (dep in varchar2) 
	RETURN FLOAT AS sumPop float;

	BEGIN
		SELECT SUM(c.pop_2010), d.nom_dep INTO sumPop FROM Commune c, Departement d WHERE d.dep = c.dep AND d.nom_dep = dep GROUP BY d.nom_dep;
		return sumPop;
	end;
	/
	show errors;


--2.3.2
CREATE OR REPLACE PROCEDURE GetConnectedUsers IS

	countDba integer;
	countSys integer;
	percentage float;
	BEGIN
		select count(username) INTO countDba from dba_users where username <> '%SYS%';

		select count(username) INTO countSys from v$session where username is not null;

		percentage := (countSys / countDba)*100;


		dbms_output.put_line(countDba || ' DBA users, ' || countSys ||' currently connected, so ' || percentage || ' are connected to the DB');

	end;
	/
	show errors;


BEGIN 
	GetConnectedUsers();
END; 
/
	show errors;

