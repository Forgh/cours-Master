create or replace PROCEDURE manip_dbms_id (codeIn varchar) IS 
   cursor C IS SELECT DBMS_ROWID.ROWID_BLOCK_NUMBER(b.rowid) AS  numBlock,
          DBMS_ROWID.ROWID_OBJECT(b.rowId) AS numTable,
          DBMS_ROWID.ROWID_ROW_NUMBER(b.ROWID) AS numTuple,
          b.nom_com AS nomCom
          from commune a, commune b 
          where a.codeinsee= '34172' 
          and DBMS_ROWID.ROWID_BLOCK_NUMBER(b.rowid) = DBMS_ROWID.ROWID_BLOCK_NUMBER(a.rowid);  -- declare cursor variable
   numBlock number;
    numTable number;
    numTuple number;
    

BEGIN
 DBMS_OUTPUT.PUT_LINE('Coucou');
  FOR t in C  LOOP -- open cursor variable

  DBMS_OUTPUT.PUT_LINE('Block : ' || t.numBlock || ', numTable : ' || t.numTable || ',numTuple : ' || t.numTuple || ', nomCom : '|| t.nomCom );
  
  END LOOP;
END;
/
show errors;
