ALTER TABLE Commune ADD code_insee varchar(6);

UPDATE Commune  SET code_insee = dep || com;

ALTER TABLE Commune
ADD PRIMARY KEY (code_insee);

ALTER TABLE Commune DROP COLUMN reg;