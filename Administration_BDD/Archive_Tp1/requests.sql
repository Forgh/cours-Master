SELECT DISTINCT c.nom_com, c.pop_2010 
FROM Commune c, Departement d
WHERE d.nom_dep = 'HERAULT'
AND c.dep= d.dep;

--r2
SELECT c.nom_com 
FROM Commune c, Region r, Departement d  
WHERE c.dep = d.dep 
AND d.reg = r.reg 
AND r.chef_lieu = c.code_insee
INTERSECT(
	SELECT c.nom_com 
	FROM Commune c, Region r, Departement d  
	WHERE c.dep = d.dep 
	AND d.reg = r.reg 
	AND d.chef_lieu = c.code_insee
);



--r3

SELECT c.nom_com 
FROM Commune c, Region r, Departement d  
WHERE c.dep = d.dep 
AND d.reg = r.reg 
AND d.chef_lieu = c.code_insee 
MINUS(
	SELECT c.nom_com FROM Commune c, Region r, Departement d  
	WHERE c.dep = d.dep 
	AND d.reg = r.reg 
	AND r.chef_lieu = c.code_insee
);


--r4
SELECT c.nom_com 
FROM Commune c, Region r, Departement d  
WHERE c.dep = d.dep 
AND d.reg = r.reg 
AND r.chef_lieu = c.code_insee 
MINUS(
	SELECT c.nom_com FROM Commune c, Region r, Departement d  
	WHERE c.dep = d.dep
	AND d.reg = r.reg 
	AND d.chef_lieu = c.code_insee
);


--r5

SELECT c.nom_com 
from Commune c, Commune f, Departement d 
where c.dep = d.dep 
and f.dep=d.dep 
and f.codeinsee='34101' 
and c.dep=f.dep;

--r6
SELECT r.nom_reg, COUNT(DISTINCT d.dep) AS nb_dep 
FROM Region r, Departement d  
WHERE d.reg = r.reg
GROUP BY r.nom_reg;


--r7
SELECT r.nom_reg, COUNT(DISTINCT d.dep) AS nb_dep, COUNT(DISTINCT c.com) AS nb_com
FROM Region r, Departement d, Commune c
WHERE d.reg = r.reg
AND c.dep = d.dep
GROUP BY r.nom_reg;


--r8
SELECT c.nom_com, d.nom_dep, r.nom_reg
FROM Commune c, Region r, Departement d  
WHERE c.dep = d.dep 
AND d.reg = r.reg
AND c.pop_2010 = (
	SELECT MIN(pop_2010)
	FROM Commune
);


--r9
SELECT c.nom_com, c.code_insee
FROM Commune c
WHERE c.code_insee  LIKE '97%';

--r10
SELECT c.nom_com, c.pop_1975, c.pop_2010
FROM Commune c
WHERE c.pop_2010 > c.pop_1975;


--r11
SELECT c.nom_com, c.pop_1975, c.pop_2010
FROM Commune c, Region r, Departement d
WHERE r.nom_reg = 'LANGUEDOC-ROUSSILLON'
AND c.pop_2010 < c.pop_1975
AND d.reg = r.reg
AND c.dep = d.dep;


--r12
CREATE VIEW view_r11 AS
SELECT c.nom_com, d.nom_dep, r.nom_reg, c.pop_2010 - c.pop_2000 AS recul
FROM Commune c, Region r, Departement d
WHERE c.pop_2010 < c.pop_2000
AND d.reg = r.reg
AND c.dep = d.dep;

SELECT * 
FROM view_r11 ;

--r13
SELECT d.nom_dep 
FROM Departement d
WHERE d.nom_dep NOT IN (
	SELECT nom_dep
	FROM view_r11
);


--r14
SELECT c.nom_com
FROM Commune c, Region r, Departement d
WHERE r.nom_reg = 'LANGUEDOC-ROUSSILLON'
AND c.pop_2010 < c.pop_1975
AND d.reg = r.reg
AND c.dep = d.dep
AND c.pop_1975 - c.pop_2010 = (
	SELECT MAX(c.pop_1975 - c.pop_2010)
	FROM Commune c, Region r, Departement d
	WHERE d.reg = r.reg
	AND c.dep = d.dep
	AND r.nom_reg = 'LANGUEDOC-ROUSSILLON'
);