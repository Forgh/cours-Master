﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Xml;
using Share;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class Service : System.Web.Services.WebService
{
    IBibliotheque cat = null;

    public Service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
        TcpChannel tcp = new TcpChannel();
        ChannelServices.RegisterChannel(tcp, true);


        cat = (IBibliotheque)Activator.GetObject(typeof(IBibliotheque), "tcp://localhost:8087/Catalogue");
        //Tests

        //Before adding books   
        int cl = cat.GetSizeLibrary();

        //cat.AddLivre(9782830201215, "La Divina Comedia", "Dante Alighieri", "Folio", 2);
        //cat.AddLivre(9783492286282, "Mort", "Terry Pratchett", "Harper", 2);
        //cat.AddLivre(9783492286288, "La Science du Disque-Monde Vol. 1", "Terry Pratchett", "Harper", 2);

        //After adding books


        ChannelServices.UnregisterChannel(tcp);
        //Testing methods: comment and abonne
        //cat.CreateAbonne("aardvark");

        //cat.GetAbonne(0).AddCommentaire("Test", cat.RechercheLivre(9783492286282));
        //Console.WriteLine(((ILivre)cat.RechercheLivre(9783492286282)).ReadCommentaire());

        //Console.ReadLine();
    }

    [WebMethod]
    public string GetDate()
    {
        DateTime d = DateTime.Now;
        return d.ToString();
    }


    [WebMethod]
    public string SearchByTitle(String title)
    {
        return cat.RechercheLivre(title);
    }

    [WebMethod]
    public string SearchByISBN(string isbn)
    {
        long formattedISBN = formatISBN(isbn);
        if (CheckISBN(formattedISBN))
            return cat.RechercheLivre(formattedISBN);
        else return "ERROR: invalid isbn";
    }

    [WebMethod]
    public int GetSizeOfLibrary()
    {
        return cat.GetSizeLibrary();
    }

    [WebMethod]
    public string SearchByAuthor(string author)
    {
        return cat.RechercheParAuteur(author);
    }

    [WebMethod]
    public string AddBook(string isbn, string title, string author, string editor, int nbExemplaires)
    {
        long formattedISBN = formatISBN(isbn);
        if (CheckISBN(formattedISBN))
            return cat.AddLivre(formattedISBN, title, author, editor, nbExemplaires);
        else return "ERROR: invalid ISBN";
    }

    [WebMethod]
    public int AddAbonne(string pw)
    {
        return cat.CreateAbonne(pw);
    }

    [WebMethod]
    public string WriteComment(int id, string pw, string commment, string isbn)
    {
        long formattedISBN = formatISBN(isbn);
        if (CheckISBN(formattedISBN))
            return cat.WriteComment(id, pw, commment, formattedISBN);
        else return "ERROR: invalid ISBN";
    }

    private bool CheckISBN(long isbn)
    {
        com.daehosting.webservices.ISBNService service = new com.daehosting.webservices.ISBNService();
        bool isbn10 = service.IsValidISBN10(isbn.ToString());
        bool isbn13 = service.IsValidISBN13(isbn.ToString());
        return (isbn13 || isbn10);
    }

    private long formatISBN(string isbn)
    {
        string expr = Regex.Replace(isbn, @"[^0-9]", "");
        return Convert.ToInt64(expr);
    }



}