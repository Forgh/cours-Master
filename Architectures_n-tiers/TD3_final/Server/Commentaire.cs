﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Share;

namespace Server
{
    [Serializable]
    public class Commentaire : ICommentaire
    {
        public string content;
        public Abonne author;

        public Commentaire(string content, Abonne author)
        {
            this.content = content;
            this.author = author;
        }

        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        public Abonne Author
        {
            get { return author; }
            set { author = value; }
        }

        public override string ToString()
        {
            return this.Author + ": " + this.Content+" \n";
        }
    }
}
