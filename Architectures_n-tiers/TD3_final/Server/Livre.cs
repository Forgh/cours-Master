﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Share;

namespace Server
{
 
    public class Livre : ILivre
    {
        private long ISBN;
        private String title;
        private String author;
        private String editor;
        private int nbExemplaires;
        private List<Commentaire> comments;

        public Livre(long isbn, string title, string author, string editor, int nbExemplaires)
        {
            ISBN = isbn;
            this.title = title;
            this.author = author;
            this.editor = editor;
            this.nbExemplaires = nbExemplaires;
            this.comments = new List<Commentaire>();
        }

        public long Isbn
        {
            get { return ISBN; }
        }

        public string Title
        {
            get { return title; }
        }

        public string Author
        {
            get { return author; }
        }

        public string Editor
        {
            get { return editor; }
        }

        public int NbExemplaires
        {
            get { return nbExemplaires; }
        }

          public void AddCommentaire(ICommentaire com)
          {
              this.comments.Add((Commentaire)com);
              Console.WriteLine("Comment written");
          }

          public string ReadCommentaire()
          {
              string ret = "";
              for (int i = 0; i < this.comments.Count; i++)
                  ret += this.comments[i].ToString();
              return ret;
          }

        public override string ToString()
        {
            return "\n Title: " + this.Title +
                   "\n ISBN: " + this.ISBN +
                   "\n Author: " + this.Author +
                   "\n Editor: " + this.Editor +
                   "\n Nb Exemplaires: " + this.NbExemplaires;
        }

    }
}
