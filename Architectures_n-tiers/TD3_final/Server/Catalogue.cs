﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Share;

namespace Server
{
    public class Catalogue : MarshalByRefObject, IBibliotheque
    {
        private Dictionary<long,Livre> ContentLivres { get; set; }
        private int SizeLibrary { get; set; }
        private Dictionary<int,Abonne> Abonnes { get; set; }

        public Catalogue()
        {
            this.ContentLivres = new Dictionary<long, Livre>();
            this.SizeLibrary = 0;
            this.Abonnes = new Dictionary<int, Abonne>();
        }

        public string AddLivre(long isbn, string title, string author, string editor, int nbExemplaires)
        {
            String resp = "Error, isbn already existing";
            if (!this.ContentLivres.ContainsKey(isbn))
            {
                this.ContentLivres.Add(isbn, new Livre(isbn, title, author, editor, nbExemplaires));
                this.SizeLibrary += nbExemplaires;
                resp = "Book successfully added";
            }

            Console.WriteLine("Book added.");
            return resp;
        }

        public string RechercheLivre(long isbn)
        {
            Console.WriteLine("Search requested");
            String ret = "Not Found.";
   
            if (this.ContentLivres.ContainsKey(isbn))
            {
                ret = this.ContentLivres[isbn].ToString();
            }
            return ret;
        }

        public string RechercheLivre(String title)
        {
            Console.WriteLine("Search requested");
            int i = 0;
            String ret = "";
            while (i < this.ContentLivres.Count)
            {
                if (this.ContentLivres.ElementAt(i).Value.Title == title)
                    ret += this.ContentLivres.ElementAt(i).Value.ToString();

                i++;
            }
            return ret;
        }

        public string RechercheParAuteur(string auteur)
        {
            Console.WriteLine("Search requested");
            int i = 0;
            String ret = "Not Found";
            while (i < this.ContentLivres.Count)
            {
                if (this.ContentLivres.ElementAt(i).Value.Author == auteur)
                    ret += this.ContentLivres.ElementAt(i).Value.ToString();

                i++;
            }
            return ret;
        }

        public int CreateAbonne(string pw)
        {
            this.Abonnes.Add(this.Abonnes.Count,new Abonne(this.Abonnes.Count, pw));
            return this.Abonnes.Count - 1;
        }

        public IAbonne GetAbonne(int id)
        {
            return this.Abonnes[id];
        }

        public string WriteComment(int id, string pw, string com, long isbn)
        {
            String resp = "Error, book or subscriber not found";
            if (this.Abonnes[id].Password == pw && this.ContentLivres.ContainsKey(isbn))
            {
                Commentaire commentaire = new Commentaire(com,this.Abonnes[id]);

                this.ContentLivres[isbn].AddCommentaire(commentaire);
                resp = "Comment added succesfully";
            }
            return resp;
        }

        public int GetSizeLibrary()
        {
            return this.SizeLibrary;
        }

        public override string ToString()
        {
            string ret = "";
            foreach (KeyValuePair<long,Livre> entry in this.ContentLivres)
                ret += entry.Value.ToString();

            return ret;
        }


    }

}
