﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using Share;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ServerStartup.Main(): Server started");

    
            
            BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
            provider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
            IDictionary props = new Hashtable();
            props["port"] = 8087;
            TcpChannel tcp = new TcpChannel(props,null,provider);
            ChannelServices.RegisterChannel(tcp, true);

            RemotingConfiguration.RegisterWellKnownServiceType(typeof (Catalogue),
                "Catalogue", WellKnownObjectMode.Singleton);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof (Livre),
                "Livre", WellKnownObjectMode.SingleCall);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof (Abonne),
                "Abonne", WellKnownObjectMode.SingleCall);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof (Commentaire),
                "Commentaire", WellKnownObjectMode.SingleCall);
          

            Console.WriteLine("Ready... ");
            Console.Read();
        }
    }
}
