﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Share;

namespace Server
{
    [Serializable]
    public class Abonne : IAbonne
    {
        private int id;
        private String password;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public Abonne(int id, string password)
        {
            this.id = id;
            this.password = password;
        }


        public void AddCommentaire(string c, ILivre l)
        {
            Commentaire com = new Commentaire(c,this);
            l.AddCommentaire(com);

        }
    }
}
