﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace Share
{

    public interface ICommentaire
    {
        
    }
    

    public interface ILivre
    {
        void AddCommentaire(ICommentaire ca);
        string ReadCommentaire();
        string ToString();
    }

    


    public interface IBibliotheque
    {
        int GetSizeLibrary();
        void AddLivre(long isbn, string title, string author, string editor, int nbExemplaires);
        string RechercheLivre(long isbn);
        string RechercheLivre(string title);
        void CreateAbonne(string pw);
        IAbonne GetAbonne(int id);
    }

    public interface IAbonne
    {
        void AddCommentaire(string c, ILivre l);
    }

    

       

    


}
