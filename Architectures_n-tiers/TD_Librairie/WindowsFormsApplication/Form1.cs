﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Windows.Forms;
using Share;

namespace WindowsFormsApplication
{
    public partial class Form1 : Form
    {
        private IBibliotheque cat = null;
        public Form1()
        {
            InitializeComponent();
            TcpChannel tcp = new TcpChannel();
            ChannelServices.RegisterChannel(tcp, true);


            cat = (IBibliotheque)Activator.GetObject(typeof(IBibliotheque), "tcp://localhost:8087/Catalogue");
            cat.AddLivre(9782830201215, "La Divina Comedia", "Dante Alighieri", "Folio", 2);
            cat.AddLivre(9783492286282, "Mort", "Terry Pratchett", "Harper", 2);
            cat.AddLivre(9783492286282, "La Science du Disque-Monde Vol. 1", "Terry Pratchett", "Harper", 2);

            //After adding books
            //cl = cat.ContentLivres.Count;




            //Testing methods: comment and abonne
            cat.CreateAbonne("aardvark");

            cat.GetAbonne(0).AddCommentaire("Test", cat.RechercheLivre(9783492286282));

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.richTextBox1.Text = "";
            this.richTextBox1.Text = cat.RechercheLivre(textBox1.Text);
        }
    }
}
