﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading;
using Share;

namespace Client
{
    class Program
    {
        public bool init = false;
        public static Thread thread1 = null;
        public static Thread thread2 = null;

        static void Main(string[] args)
        {

            TcpChannel tcp = new TcpChannel();
            ChannelServices.RegisterChannel(tcp, true);
            Program p = new Program();


            IBibliotheque cat = (IBibliotheque) Activator.GetObject(typeof (IBibliotheque), "tcp://localhost:8087/Catalogue");
            //Livre liv = (Livre) Activator.GetObject(typeof (Livre), "tcp://localhost:8087/Livre");
            //Abonne abo = (Abonne) Activator.GetObject(typeof (Abonne), "tcp://localhost:8087/Abonne");
            //Commentaire com = (Commentaire) Activator.GetObject(typeof (Commentaire), "tcp://localhost:8087/Commentaire");
                
            //Tests
            
            //Before adding books   
            int cl = cat.GetSizeLibrary();
            Console.WriteLine("Client.Main(): Reference to rem. obj acquired");

            Console.WriteLine("Client.Main(): Original server side value (ContentLivres.Count): {0}", cl);
            Console.WriteLine("Client.Main(): Adding a few books");

            cat.AddLivre(9782830201215, "La Divina Comedia", "Dante Alighieri", "Folio", 2);
            cat.AddLivre(9783492286282, "Mort", "Terry Pratchett", "Harper", 2);
            cat.AddLivre(9783492286282, "La Science du Disque-Monde Vol. 1", "Terry Pratchett", "Harper", 2);

            //After adding books
            //cl = cat.ContentLivres.Count;
            Console.WriteLine("==============================");

            Console.WriteLine("Client.Main(): New server side value (ContentLivres.Count) {0}", cat.GetSizeLibrary());



            //Testing methods: comment and abonne
            cat.CreateAbonne("aardvark");

            cat.GetAbonne(0).AddCommentaire("Test", cat.RechercheLivre(9783492286282));
            Console.WriteLine(((ILivre)cat.RechercheLivre(9783492286282)).ReadCommentaire());

            
            //Question 8
            thread1 = new Thread(new ThreadStart(p.RunMe)); //C'est ca ?
            thread2 = new Thread(new ThreadStart(p.RunMe));
            thread1.Start();
            thread2.Start();
            Console.ReadLine();


        }

        public void RunMe()
        {
            if (Thread.CurrentThread == thread1)
            {
                Console.WriteLine("THREAD 1 =========");
                SearchTests();
                Console.WriteLine("THREAD 1 END =========");


            }
            else if (Thread.CurrentThread == thread2)
            {
                Console.WriteLine("THREAD 2 =========");
                SearchTests();
                Console.WriteLine("THREAD 2 END =========");

            }
        }


        public void SearchTests()
        {
            IBibliotheque cat = (IBibliotheque)Activator.GetObject(typeof(IBibliotheque), "tcp://localhost:8087/Catalogue");
   
            //Testing methods: search
            Console.WriteLine("==============================");
            Console.WriteLine("Client.Main(): Search by ISBN: 9783492286282 {0}", cat.RechercheLivre(9783492286282));
            Console.WriteLine("==============================");

            Console.WriteLine("Client.Main(): Search by Title: Mort {0}", cat.RechercheLivre("Mort"));
        }
    }
}
