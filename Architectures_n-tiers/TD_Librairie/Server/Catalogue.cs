﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Share;

namespace Server
{
    public class Catalogue : MarshalByRefObject, IBibliotheque
    {
        private List<Livre> ContentLivres { get; set; }
        private int SizeLibrary { get; set; }
        private List<Abonne> Abonnes { get; set; }

        public Catalogue()
        {
            this.ContentLivres = new List<Livre>();
            this.SizeLibrary = 0;
            this.Abonnes = new List<Abonne>();
        }

        public void AddLivre(long isbn, string title, string author, string editor, int nbExemplaires)
        {
            this.ContentLivres.Add(new Livre(isbn, title, author, editor, nbExemplaires));
            this.SizeLibrary += nbExemplaires;

            Console.WriteLine("Book added.");
        }

        public ILivre RechercheLivre(long isbn)
        {
            Console.WriteLine("Search requested");
            int i = 0;
            bool found = false;
            while (i < this.ContentLivres.Count && !found)
            {
                if (this.ContentLivres[i].Isbn == isbn)
                    found = true;
                i++;
            }
            if (found)
                return this.ContentLivres[i];
            else return null;
        }

        public string RechercheLivre(String title)
        {
            Console.WriteLine("Search requested");
            int i = 0;
            String ret = "";
            while (i < this.ContentLivres.Count)
            {
                if (this.ContentLivres[i].Title == title)
                    ret += this.ContentLivres[i].ToString();

                i++;
            }
            return ret;
        }

        public void CreateAbonne(string pw)
        {
            this.Abonnes.Add(new Abonne(this.Abonnes.Count, pw));
        }

        public IAbonne GetAbonne(int id)
        {
            return this.Abonnes[id];
        }


        public int GetSizeLibrary()
        {
            return this.SizeLibrary;
        }

        public override string ToString()
        {
            string ret = "";
            foreach (Livre l in this.ContentLivres)
                ret += l.ToString();

            return ret;
        }

    }

}
