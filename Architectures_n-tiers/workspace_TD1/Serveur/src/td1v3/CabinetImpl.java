package td1v3;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

public class CabinetImpl extends UnicastRemoteObject implements Cabinet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<String,Animal> patients;
	
	
	public CabinetImpl() throws RemoteException {
		this.patients = new HashMap<String,Animal>();
	}

	public HashMap<String, Animal> getPatients() {
		return patients;
	}
	
	public String addPatient (AnimalImpl a) {
		this.patients.put(a.getName(),a);
		switch (this.patients.size())
		{
			case 100:
				return "/!\\ 100 limit" ;
			case 500:
				return "/!\\ 100 limit" ;
			case 1000:
				return "/!\\ 100 limit" ;
			break;
			default: return "Added safely.";

		}

	}


	@Override
	public Animal searchByName(String t) throws RemoteException {
		if(this.patients.get(t) == null) return null;
		else return this.patients.get(t);
	}

	@Override
	public String describeAnimal(Specy s) throws RemoteException {
		return s.getSpecifications();
	}
}