package td1v3;

public class MedicalRecord {
	private String content;
	
	public MedicalRecord() {
		this.content = "";
	}
	
	public MedicalRecord(String t){
		this.content = t;
	}

	public String getContent() {
		return content;
	}

	public void addToRecord(String t){
		String newline = System.getProperty("line.separator");
		this.content += t;
		this.content += newline;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
}
