package td1v3;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AnimalImpl extends UnicastRemoteObject  implements Animal {
	/**
	 * 
	 */
	private static final long serialVersionUID = -586798841210719419L;
	String name;
	String nameMaster;
	Specy specy;
	String race;
	MedicalRecord record;
	
	public MedicalRecord getRecord() {
		return record;
	}

	public void setRecord(MedicalRecord record) {
		this.record = record;
	}

	
	public AnimalImpl() throws RemoteException {
		this.name = "Unknown";
		this.nameMaster = "John Doe";
		//this.specy = new SpecyImpl();
		this.race = "Unknown";
		this.record = new MedicalRecord();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameMaster() {
		return nameMaster;
	}

	public void setNameMaster(String nameMaster) {
		this.nameMaster = nameMaster;
	}
	
	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public AnimalImpl(String n, String nm, String r, Specy s) throws RemoteException{
		this.name = n;
		this.nameMaster = nm;
		this.specy = s;
		this.race = r;
		this.record = new MedicalRecord();
	}

	@Override
	public String getNames() throws RemoteException {
		String newline = System.getProperty("line.separator");
		return "Animal: "+this.name+newline
				+"Master: "+this.nameMaster+newline;
	}

	@Override
	public void printNames() throws RemoteException {
		System.out.println("Animal: "+this.name);
		System.out.println("Master: "+this.nameMaster);
	}

	@Override
	public void printRecord() throws RemoteException {
		System.out.println(this.record.getContent());	
	}

	@Override
	public String getMedicalRecord() throws RemoteException {
		return this.record.getContent();
	}

	@Override
	public void writeToRecord(String t) throws RemoteException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		System.out.println(); //2014/08/06 16:00:22
		this.record.addToRecord(dateFormat.format(cal.getTime())+": "+t);
	}

	@Override
	public String getSpecy() throws RemoteException {
		return this.specy.getSpecifications();
	}

	@Override
	public void printSpecy() throws RemoteException {
		this.specy.printSpecifications();
	}

	@Override
	public void setSpecy(Specy s) throws RemoteException {
		this.specy = s;
	}
	
}
