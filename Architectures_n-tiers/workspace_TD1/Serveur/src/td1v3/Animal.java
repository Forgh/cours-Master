package td1v3;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Animal extends Remote{
	String getNames() throws RemoteException;
	void printNames() throws RemoteException;
	String getMedicalRecord() throws RemoteException;
	void printRecord() throws RemoteException;
	void writeToRecord(String t) throws RemoteException;
	String getSpecy() throws RemoteException;
	void printSpecy() throws RemoteException;
	void setSpecy(Specy s) throws RemoteException;
}


