package td1v3;

import java.io.Serializable;

public class Specy implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String name;
	int lifeExpectancy;
	
	public Specy(){
		this.name = "Animal";
		this.lifeExpectancy = 0;
	}
	
	public Specy(String n, int l){
		this.name = n;
		this.lifeExpectancy = l;
	}

	public String getName() {
		return name;
	}

	public int getLifeExpectancy() {
		return lifeExpectancy;
	}

	public String getSpecifications()  {
		String newline = System.getProperty("line.separator");

		return this.name + ", lives "+this.lifeExpectancy+newline;
	}

	public void printSpecifications() {
		System.out.println(this.name+",  lives"+this.lifeExpectancy);		
	}
}
