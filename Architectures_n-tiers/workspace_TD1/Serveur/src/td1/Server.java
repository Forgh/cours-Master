package td1;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
	public static void main(String args[]) {

		
		try {
			System.setProperty("java.security.policy", "td1.policy");
			
			SpecyImpl sp = new SpecyImpl("Canine", 12);
			AnimalImpl dogmeat = new AnimalImpl("Dogmeat", "Lone Wanderer", "German Shepherd", sp);
			CabinetImpl cabinet = new CabinetImpl();
			cabinet.addPatient(dogmeat);
			
			//Registry registry = LocateRegistry.createRegistry(1099);
			Registry registry = LocateRegistry.getRegistry();
			
			if (registry==null){
				System.err.println("RmiRegistry not found");
			}else{
				System.out.println("Binding...");
				registry.bind("cabinet", cabinet);
				System.err.println("Server ready");
			}
		} catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}
	}

}
