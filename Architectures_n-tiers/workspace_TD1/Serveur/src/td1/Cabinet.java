package td1;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Cabinet extends Remote {
	public Animal searchByName(String t) throws RemoteException ;
}
