package td1;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

public class CabinetImpl extends UnicastRemoteObject implements Cabinet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<String,Animal> patients;
	
	
	public CabinetImpl() throws RemoteException {
		this.patients = new HashMap<String,Animal>();
	}

	public HashMap<String, Animal> getPatients() {
		return patients;
	}
	
	public void addPatient (AnimalImpl a) {
		this.patients.put(a.getName(),a);
	}


	@Override
	public Animal searchByName(String t) throws RemoteException {
		if(this.patients.get(t) == null) return null;
		else return this.patients.get(t);
	}
}