package td1;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Specy extends Remote {
	String getSpecifications() throws RemoteException;
	void printSpecifications() throws RemoteException;
}
