package td1;

import java.rmi.RemoteException;

public class SpecyImpl implements Specy {
	String name;
	int lifeExpectancy;
	
	public SpecyImpl(){
		this.name = "Animal";
		this.lifeExpectancy = 0;
	}
	
	public SpecyImpl(String n, int l){
		this.name = n;
		this.lifeExpectancy = l;
	}

	public String getName() {
		return name;
	}

	public int getLifeExpectancy() {
		return lifeExpectancy;
	}

	@Override
	public String getSpecifications() throws RemoteException {
		String newline = System.getProperty("line.separator");

		return this.name + ", lives "+this.lifeExpectancy+newline;
	}

	@Override
	public void printSpecifications() throws RemoteException {
		System.out.println(this.name+",  lives"+this.lifeExpectancy);		
	}
}
