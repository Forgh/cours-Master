package td1v3;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
	private Client() {}

	public static void main(String[] args) {

		String host = (args.length < 1) ? null : args[0];
		try {
			Registry registry = LocateRegistry.getRegistry(host);
			
			Cabinet cab = (Cabinet) registry.lookup("cabinet");
			
			Animal stub = cab.searchByName("Dogmeat");
			stub.writeToRecord("First visit");
			stub.writeToRecord("Nothing more");
			
			String response = stub.getNames();
			response += stub.getMedicalRecord();
			response += stub.getSpecy();
			System.out.println("response: " + response);

			//stub.printRecord();
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
	}
}
