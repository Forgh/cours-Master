package td1v3;

public class ProtectedSpecy extends Specy {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private int i=12;
	@Override
	public String getSpecifications() {
		String newline = System.getProperty("line.separator");
		
		return "/!\\ Attention, protected specy" + newline 
				+ "Name:"+ this.getName() +", lives "+ this.getLifeExpectancy() + newline;
	}

	@Override
	public void printSpecifications()  {
		System.out.println("/!\\ Attention, protected specy"); 
		System.out.println("Name:"+this.name + ", lives "+this.lifeExpectancy);
	}

	public ProtectedSpecy(String name, int lifeExpectancy) {
		super(name,lifeExpectancy);
	}

}
