package td1v3;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientVeterinaire {
	private ClientVeterinaire() {}

	public static void main(String[] args) {

		String host = (args.length < 1) ? null : args[0];
		try {
			Registry registry = LocateRegistry.getRegistry(host);
			Cabinet cab = (Cabinet) registry.lookup("cabinet");
			Animal stub = cab.searchByName("Dogmeat");

			//Added functionnalities
			Specy sp = new Specy("Mammal", 12);
			AnimalImpl firefox = new AnimalImpl("Firefox", "Mozilla", "Red Panda", sp);
			System.out.println(cab.addPatient(firefox));

			stub.writeToRecord("First visit");
			stub.writeToRecord("Nothing more");
			stub.setSpecy(new ProtectedSpecy("Red Panda",5));
			
			String response = stub.getNames();
			response += stub.getMedicalRecord();
			response += stub.getSpecy();
			System.out.println("response: " + response);

			//stub.printRecord();
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
	}
}
