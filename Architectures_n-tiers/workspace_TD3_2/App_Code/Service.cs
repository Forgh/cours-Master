﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Threading;
using System.Web;
using System.Web.Services;
using Share;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class Service : System.Web.Services.WebService
{
    IBibliotheque cat = null;

    public Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
        TcpChannel tcp = new TcpChannel();
        ChannelServices.RegisterChannel(tcp, true);


        cat = (IBibliotheque)Activator.GetObject(typeof(IBibliotheque), "tcp://localhost:8087/Catalogue");
        //Tests

        //Before adding books   
        int cl = cat.GetSizeLibrary();
  
        cat.AddLivre(9782830201215, "La Divina Comedia", "Dante Alighieri", "Folio", 2);
        cat.AddLivre(9783492286282, "Mort", "Terry Pratchett", "Harper", 2);
        cat.AddLivre(9783492286288, "La Science du Disque-Monde Vol. 1", "Terry Pratchett", "Harper", 2);

        //After adding books
      

        ChannelServices.UnregisterChannel(tcp);
        //Testing methods: comment and abonne
        //cat.CreateAbonne("aardvark");

        //cat.GetAbonne(0).AddCommentaire("Test", cat.RechercheLivre(9783492286282));
        //Console.WriteLine(((ILivre)cat.RechercheLivre(9783492286282)).ReadCommentaire());

        //Console.ReadLine();
    }

    [WebMethod]
    public string GetDate() {
        DateTime d = DateTime.Now;
        return d.ToString();
    }


    [WebMethod]
    public string SearchByTitle(String title)
    {
        return cat.RechercheLivre(title);
    }

    [WebMethod]
    public string SearchByISBN(long isbn)
    {
        return (String)cat.RechercheLivre(isbn);
    }

    [WebMethod]
    public int GetSize()
    {
        return cat.GetSizeLibrary();
    }
}