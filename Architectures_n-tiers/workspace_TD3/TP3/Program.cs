﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using TP3.ConvertTemperature;
using TP3.GlobalWeather;

namespace TP3
{
    class Program
    {
        static void Main(string[] args)
        {
            GlobalWeather.GlobalWeather g = new GlobalWeather.GlobalWeather();

            String weather = g.GetWeather("Reims", "France");
            XmlDocument xml = new XmlDocument();
            StringReader xmlSr;
            if (weather.Equals("Data Not Found"))
            {
                xmlSr = new StringReader(g.GetCitiesByCountry("France"));
                xml.Load(xmlSr);
                foreach (XmlNode node in xml.GetElementsByTagName("City"))
                {
                    Console.WriteLine(node.InnerXml);
                }
            }
            else //The requested city exists, let's roll with it
            {
                
                //display it, load it as a XML doc
                Console.WriteLine(weather);
                xmlSr = new StringReader(weather);
                xml.Load(xmlSr);

                //Converting temperature
                String ret = xml.GetElementsByTagName("Temperature")[0].InnerXml;

                string[] tokens = ret.Split(' ');
                double temp = double.Parse(tokens[1]);

                ConvertTemperature.ConvertTemperature cv = new ConvertTemperature.ConvertTemperature();
                Console.WriteLine(cv.ConvertTemp(temp, TemperatureUnit.degreeFahrenheit, TemperatureUnit.degreeCelsius));
            }

            Console.Read();
        }
    }
}
