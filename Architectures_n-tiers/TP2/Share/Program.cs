﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Share
{
    public class Commentaire : MarshalByRefObject
    {
        private string content;
        private Abonne author;

        public Commentaire(string content, Abonne author)
        {
            this.content = content;
            this.author = author;
        }

        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        public Abonne Author
        {
            get { return author; }
            set { author = value; }
        }
    }

    public class Livre : MarshalByRefObject
    {
        private long ISBN;
        private String title;
        private String author;
        private String editor;
        private int nbExemplaires;
        private List<Commentaire> comments;

        public Livre(long isbn, string title, string author, string editor, int nbExemplaires)
        {
            ISBN = isbn;
            this.title = title;
            this.author = author;
            this.editor = editor;
            this.nbExemplaires = nbExemplaires;
            this.comments = new List<Commentaire>();
        }

        public long Isbn
        {
            get { return ISBN; }
        }

        public string Title
        {
            get { return title; }
        }

        public string Author
        {
            get { return author; }
        }

        public string Editor
        {
            get { return editor; }
        }

        public int NbExemplaires
        {
            get { return nbExemplaires; }
        }

        public void AjouterCommentaire(Abonne a, string com)
        {
            Commentaire c = new Commentaire(com, a);
            this.comments.Add(c);
        }

        public override string ToString()
        {
            return "\n Title: " + this.Title +
                   "\n ISBN: "+this.ISBN+
                   "\n Author: " + this.Author +
                   "\n Editor: " + this.Editor +
                   "\n Nb Exemplaires: " + this.NbExemplaires;
        }

    }

    public interface IBibliotheque
    {
        int GetSizeLibrary();
        void AddLivre(long isbn, string title, string author, string editor, int nbExemplaires);
        Livre RechercheLivre(long isbn);
        string RechercheLivre(string title);
    }

    public class Catalogue : MarshalByRefObject, IBibliotheque
    {
        public List<Livre> ContentLivres { get; set; }
        public int SizeLibrary { get; set; }

        public Catalogue()
        {
            this.ContentLivres = new List<Livre>();
            this.SizeLibrary = 0;
        }

        public void AddLivre(long isbn, string title, string author, string editor, int nbExemplaires)
        {
            this.ContentLivres.Add(new Livre(isbn,title,author,editor,nbExemplaires));
            this.SizeLibrary += nbExemplaires;

            Console.WriteLine("Book added.");
        }

        public Livre RechercheLivre(long isbn)
        {
            Console.WriteLine("Search requested");
            int i = 0;
            bool found = false;
            while (i < this.ContentLivres.Count && !found)
            {
                if (this.ContentLivres[i].Isbn == isbn)
                    found = true;
                i++;
            }
            if (found)
                return this.ContentLivres[i];
            else return null;
        }

        public string RechercheLivre(String title)
        {
            Console.WriteLine("Search requested");
            int i = 0;
            String ret = "";
            while (i < this.ContentLivres.Count)
            {
                if (this.ContentLivres[i].Title == title)
                    ret += this.ContentLivres[i].ToString();
                
                i++;
            }
            return ret;
        }

        public int GetSizeLibrary()
        {
            return this.SizeLibrary;
        }

        public override string ToString()
        {
            string ret = "";
            foreach (Livre l in this.ContentLivres)
                ret += l.ToString();
            
            return ret;
        }
    }

    public class Abonne : MarshalByRefObject
    {
        private int id;
        private String password;

        public Abonne(int id, string password)
        {
            this.id = id;
            this.password = password;
        }

        public int Id
        {
            get { return id; }
        }

        public string Password
        {
            get { return password; }
        }
    }


}
