﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using Share;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ServerStartup.Main(): Server started");

    
            TcpChannel tcp = new TcpChannel(8087);
            ChannelServices.RegisterChannel(tcp, true);
            
         
            RemotingConfiguration.RegisterWellKnownServiceType(typeof (Catalogue),
                "Catalogue", WellKnownObjectMode.Singleton);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof (Livre),
                "Livre", WellKnownObjectMode.SingleCall);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof (Abonne),
                "Abonne", WellKnownObjectMode.SingleCall);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof (Commentaire),
                "Commentaire", WellKnownObjectMode.SingleCall);
            /*RemotingConfiguration.RegisterWellKnownServiceType(
                typeof (Bibliotheque),
                "Catalogue",
                WellKnownObjectMode.SingleCall);*/

            Console.WriteLine("Ready... ");
            Console.Read();
        }
    }
}
