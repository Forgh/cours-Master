--XML type

<batiment><etage><description>lorem ipsum</description><bureau><code>155</code><personne>Dupont</personne></bureau></etage><etage><description>lorem ispum dolor sit amet</description><salle><nbsalle>156</nbsalle></salle></etage></batiment>

<presse><journal><articles><article titre="Un titre" auteur="012"><corps>Lorem ipsum dolor sit amet</corps></article></articles></journal><journalistes><journaliste idJ="012"><nom>Dupont</nom><prenom>Jean</prenom></journaliste></journalistes></presse>


--VERSION INDENTEE
<presse>
	<journal>
		<articles>
			<article titre="Un titre" auteur="012">
				<corps>Lorem ipsum dolor sit amet</corps>
			</article>
		</articles>
	</journal>

	<journalistes>
		<journaliste idJ="012">
			<nom>Dupont</nom>
			<prenom>Jean</prenom>
		</journaliste>
	</journalistes>
</presse>


--Question 4
CREATE TABLE TP1_CLOB (nom_document varchar(20), fichier_xml XMLTYPE)
XMLTYPE fichier_xml STORE AS CLOB;


CREATE TABLE TP1_binaryxml (nom_document varchar(20), fichier_xml XMLTYPE)
XMLTYPE fichier_xml STORE AS BINARY XML;



--Question 5



INSERT INTO TP1_CLOB (nom_document, fichier_xml) VALUES ('batiments', sys.xmltype.createxml('<batiment><etage><description>lorem ipsum</description><bureau><code>155</code><personne>Dupont</personne></bureau></etage><etage><description>lorem ispum dolor sit amet</description><salle><nbsalle>156</nbsalle></salle></etage></batiment>') );


INSERT INTO TP1_CLOB (nom_document, fichier_xml) VALUES ('presses', sys.xmltype.createxml('<presse><journal><articles><article titre="Un titre" auteur="012"><corps>Lorem ipsum dolor sit amet</corps></article></articles></journal><journalistes><journaliste idJ="012"><nom>Dupont</nom><prenom>Jean</prenom></journaliste></journalistes></presse>') );

INSERT INTO TP1_binaryxml (nom_document, fichier_xml) VALUES ('batiments', sys.xmltype.createxml('<batiment><etage><description>lorem ipsum</description><bureau><code>155</code><personne>Dupont</personne></bureau></etage><etage><description>lorem ispum dolor sit amet</description><salle><nbsalle>156</nbsalle></salle></etage></batiment>') );


INSERT INTO TP1_binaryxml (nom_document, fichier_xml) VALUES ('presses', sys.xmltype.createxml('<presse><journal><articles><article titre="Un titre" auteur="012"><corps>Lorem ipsum dolor sit amet</corps></article></articles></journal><journalistes><journaliste idJ="012"><nom>Dupont</nom><prenom>Jean</prenom></journaliste></journalistes></presse>') );




--Question 6
--Batiments

--CLOB
SELECT EXTRACT(fichier_xml, '//batiment/etage/bureau//text()')
FROM TP1_CLOB;

SELECT EXTRACT(fichier_xml, '//batiment/etage/bureau[code//text()="155"]/personne//text()')
FROM TP1_CLOB;

SELECT EXTRACT(fichier_xml, '//batiment/etage/bureau[count(personne)>1]/code//text()')
FROM TP1_CLOB;

--XML binary
SELECT EXTRACT(fichier_xml, '//batiment/etage/bureau//text()')
FROM TP1_binaryxml;

SELECT EXTRACT(fichier_xml, '//batiment/etage/bureau[code//text()="155"]/personne//text()')
FROM TP1_binaryxml;

SELECT EXTRACT(fichier_xml, '//batiment/etage/bureau[count(personne)>1]/code//text()')
FROM TP1_binaryxml;


--Presse
--XML Binary
SELECT EXTRACT(fichier_xml, '//presse/journal/articles/article/corps')
FROM TP1_binaryxml ;

SELECT EXTRACT(fichier_xml, '//presse/journal/articles/article/corps')
FROM TP1_CLOB ;

SELECT EXTRACT(fichier_xml, '//presse/journalistes/journaliste[nom//text()="Dupont"]')
FROM TP1_CLOB;

SELECT EXTRACT(fichier_xml, '//presse/journalistes/journaliste[nom//text()="Dupont"]')
FROM TP1_binaryxml;




--Q7

--EDGE
CREATE TABLE EDGES(source varchar(5), target varchar(5), ordinal number, tag varchar(25), type varchar(5));

CREATE TABLE TEXTVALUES (node varchar(5), value varchar(10));

CREATE TABLE NUMVALUES (node varchar(5), value number);

INSERT INTO EDGES VALUES (null, 'n1', 1, 'presse', 'elt');
INSERT INTO EDGES VALUES ('n1', 'n2', 1, 'journal', 'elt');
INSERT INTO EDGES VALUES ('n2', 'n3', 1, 'articles', 'elt');
INSERT INTO EDGES VALUES ('n3', 'n4', 1, 'article', 'elt');
INSERT INTO EDGES VALUES ('n4', 'n5', 1, 'corps', 'elt');
INSERT INTO EDGES VALUES ('n5', 'n6', 1, null, 'txt');

INSERT INTO TEXTVALUES('n6','Lorem ipsum dolor sit amet');

INSERT INTO EDGES VALUES ('n4', 'n7', 2, 'titre', 'elt');
INSERT INTO EDGES VALUES ('n7', 'n8', 1, null, 'txt');

INSERT INTO TEXTVALUES('n8','Un titre');

INSERT INTO EDGES VALUES ('n4', 'n9', 3, 'auteur', 'elt');
INSERT INTO EDGES VALUES ('n9', 'n10', 1, null, 'num');

INSERT INTO NUMVALUES('n10',012);

INSERT INTO EDGES VALUES ('n1', 'n11', 2, 'journalistes', 'elt');
INSERT INTO EDGES VALUES ('n11', 'n12', 1, 'journaliste', 'elt');
INSERT INTO EDGES VALUES ('n12', 'n13', 1, 'nom', 'elt');
INSERT INTO EDGES VALUES ('n13', 'n14', 1, null, 'txt');

INSERT INTO TEXTVALUES('n14','Dupont');

INSERT INTO EDGES VALUES ('n12', 'n15', 2, 'prenom', 'elt');
INSERT INTO EDGES VALUES ('n15', 'n16', 1, null, 'txt');

INSERT INTO TEXTVALUES('n16','Jean');

INSERT INTO EDGES VALUES ('n12', 'n17', 3, 'idJ', 'elt');
INSERT INTO EDGES VALUES ('n17', 'n18', 1, null, 'num');

INSERT INTO NUMVALUES('n10',012);



--VERTICAL-EDGE

CREATE TABLE people(source varchar(5), target varchar(5), ordinal number,
 tag varchar(25), type varchar(5));

CREATE TABLE person (source varchar(5), target varchar(5), ordinal number, 
tag varchar(25), type varchar(5));

CREATE TABLE name (source varchar(5), target varchar(5), ordinal number, 
tag varchar(25), type varchar(5));

CREATE TABLE age (source varchar(5), target varchar(5), ordinal number, 
tag varchar(25), type varchar(5));