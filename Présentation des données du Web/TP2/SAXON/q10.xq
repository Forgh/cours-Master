declare function local:retwittedBy($i){
	let $x:= doc('twitter.xml') 
	let $t:= $x//Tweet[@id = $i]/corps/texte_libre/text()
	
	for $rt in $x//Tweet[not(@id = $i)]
	let $corps = $rt/corps/texte_libre/text()
	where contains($corps, "RT") and contains($corps, $t)

	return 
		<result>
			{ $rt/@auteur }
		</results>
};

local:retwittedBy(5)