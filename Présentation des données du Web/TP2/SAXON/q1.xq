let $x:= doc('twitter.xml') 


for $a in $x//utilisateur
for $t in $x//Tweet
where $t/@auteur = $a/@id
return 
<result>
  <tweet> { $t/corps/texteLibre/text() } </tweet>
  <auteur> { $a/nom/text() } </auteur>
</result>
