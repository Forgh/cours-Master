let $x:= doc('twitter.xml') 


for $a in $x//utilisateur
for $t in $x//Tweet
where $t/@auteur = $a/@id
return 
<result>
  <tweet> { $t/corps/texteLibre/text() } </tweet>
  <auteur> { $a/nom/text() } </auteur>
</result>

for $t in $x//Tweet
where $t/corps/hashtags/hashtag/text() = "#I&lt;3XML"
return $t


for $t in $x//Tweet
where $t/datePost/text() = min($t/datePost/text()) or $t/datePost/text() = max($t/datePost/text())
return $t


for $t in $x//Tweet
where $t/corps/hashtags
return 
<tweet>{ $t/corps/texteLibre } { string-join($t/hashtags, ' ')} </tweet>

for $t in $x//Tweet
where $t/corps/refusers
return 
<tweet>{ $t/corps/texteLibre } { string-join($t/refusers, ' ')} </tweet>

