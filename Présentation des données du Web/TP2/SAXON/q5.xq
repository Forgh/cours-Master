let $x:= doc('twitter.xml') 

for $a in $x//utilisateur/nom
order by $a ascending 
return 

<result>
<user>{ $a/text() }</user>
</result>