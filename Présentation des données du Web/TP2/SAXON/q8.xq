let $x:= doc('twitter.xml') 

for $t in $x//Tweet
where $t/corps/hashtags
return 
<tweet>{ $t/corps/texteLibre/text() } { string-join($t/corps/hashtags, ' ')} </tweet>
