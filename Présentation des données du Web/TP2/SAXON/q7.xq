let $x:= doc('twitter.xml') 

let $fin := count($x//Tweet)

for $t at $pos in $x//Tweet
where $pos = 1 or $pos = $fin
order by xs:date($t/datePost/text())
return $t