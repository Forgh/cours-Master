let $x:= doc('twitter.xml') 

for $a in $x//utilisateur
for $t in $x//Tweet
let $s := $t/nb_retweets
where ($t/@auteur = $a/@id )and ($s > 2)
return 
<result>
<utilisateur>{ $a/nom/text() }</utilisateur>
</result>