let $x:= doc('twitter.xml') 

for $t in $x//Tweet
where $t/corps/refusers
return 
<tweet>{ $t/corps/texteLibre/text() } { string-join($t/corps/refusers, ' ')} </tweet>

