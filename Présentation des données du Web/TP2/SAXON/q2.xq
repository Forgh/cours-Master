let $x:= doc('twitter.xml') 

for $a in $x//utilisateur

return 
<result>
{for $t in $x//Tweet
where $t/@auteur = $a/@id
return
<tweet>{ $t/datePost/text() }</tweet>}
</result>