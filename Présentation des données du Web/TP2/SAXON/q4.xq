let $x:= doc('twitter.xml') 

for $a in $x//utilisateur
for $b at $pos in $x//Tweet
let $z := $b/nb_retweets
let $y := $b/datePost
where $pos = 1 or $pos = 2
order by $y descending
return 
if ($z = 0) then 

<result>
<ononRetwitted/>
</result>

else 
 
<result>
<tweet>{ $b/corps/texte_libre/text() }</tweet>
</result>
