<%--
  Created by IntelliJ IDEA.
  User: ghost_000
  Date: 12/10/2016
  Time: 11:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.util.*" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:useBean class="Converter.ConverterBean" id="beanConv"/>
<%
    double amount = Double.parseDouble(request.getParameter("amount"));
    String currency = request.getParameter("currency");
    amount = beanConv.euroToOtherCurrency(amount,currency);
    out.println("<h4>Le montant converti est : </h4>"+amount);
%>
</body>
</html>
