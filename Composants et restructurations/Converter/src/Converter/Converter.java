package Converter;

import org.jdom2.JDOMException;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * @author Forgh
 */
public interface Converter {
    public double euroToOtherCurrency(double amount, String currencyCode);
}
