package Converter;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.util.IteratorIterable;

import javax.ejb.Stateless;
import javax.swing.text.html.HTMLDocument;
import javax.xml.crypto.URIDereferencer;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * @author Forgh
 */
@Stateless(name = "ConverterEJB")
public class ConverterBean implements Converter{
    public ConverterBean() {
    }

    @Override
    public double euroToOtherCurrency(double amount, String currencyCode)  {
        SAXBuilder sxb = new SAXBuilder();
        URL url = null;
        try {
            url = new URL("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Document document = null;
        try {
            document = sxb.build(url);
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
        Element racine = document != null ? document.getRootElement() : null;
        Namespace ns = Namespace.getNamespace("http://www.ecb.int/vocabulary/2002-08-01/eurofxref");
        List<Element> elems = racine.getChild("Cube", ns).getChildren();
        for (Element e : elems
             ) {
            if(e.getAttributeValue("currency").equals(currencyCode)) {
                amount = amount * Double.parseDouble(e.getAttributeValue("rate"));
            }

        }

        System.out.println(amount);

        return amount;
    }
}
