/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appli1;

import javax.swing.JLabel;

/**
 *
 * @author mcampmas
 */
public class Test1 extends JLabel{
    private String text;
    
    public Test1(){
        this.text = "open";
    }

    public String getText() {
        return text;
    }
    
    public void switchText(){
        if(this.text.equals("open"))
            this.text = "closed";
        else this.text = "open";
        this.setText(text);
    }
}
