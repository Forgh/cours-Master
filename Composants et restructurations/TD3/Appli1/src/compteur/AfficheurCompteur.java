/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compteur;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.TextField;
import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author mcampmas
 */
public class AfficheurCompteur extends Panel {
    private TextField textfield;
    private Color color = Color.red;
    
    public AfficheurCompteur() {
        super();
        setLayout(new FlowLayout(FlowLayout.CENTER, 30, 10));
        setSize(60,40);
        setBackground(color);
        textfield = new TextField("0");
        add(textfield); 
    }



    public void affiche(String s){
        textfield.setText(s); this.repaint();
    }

    public void affiche(Integer i){
        this.affiche(String.valueOf(i));
    }

}
    
