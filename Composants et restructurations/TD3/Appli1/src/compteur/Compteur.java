/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compteur;

import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author mcampmas
 */
public class Compteur implements Serializable {
    
    public static final String PROP_CPT_PROPERTY = "compteur";
    
    private Integer cpt;
    
    private PropertyChangeSupport propertySupport;
    
    public Compteur() {
        propertySupport = new PropertyChangeSupport(this);
        cpt = new Integer(0);
    }
    
    public int getSampleProperty() {
        return cpt;
    }
    
    public void setCpt(Integer value) {
        Integer oldValue = cpt;
        cpt = value;
        propertySupport.firePropertyChange(PROP_CPT_PROPERTY, oldValue, cpt);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
    public void increase(){
        this.setCpt(this.getCpt()+1);
    }
    
    
   public void decrease(){
        this.setCpt(this.getCpt()-1);
   }
   
    /**
     *
     * @return cpt
     */
    public Integer getCpt() {
        return cpt;
    }
    
}
