package stockage;

/**
 * @author Forgh
 */
public class JavaCleanVisitor {
    public void visit(ElementStockage f){
        delFileSuf(".class",f);
    }

    public void delFileSuf(String suf, ElementStockage d){

        if(d instanceof Directory){
            ArrayList<ElementStockage> elements = new ArrayList<> ((Directory)d).getElements());
            for (int i=0; i<elements.size(); i++){
                if (elements.get(i) instanceof File && elements.get(i).getName().endsWith(".class")){
                    ((File) elements.get(i)).getParent().remove(elements.get(i));
                }else if(elements.get(i) instanceof Directory){
                    delFileSuf(suf, elements.get(i));
                }
            }
        }

        else if (d instanceof File && d.getName().endsWith(".class")){
            d.getParent().remove(d);
        }
    }
}
