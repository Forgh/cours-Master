package stockage;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Forgh
 */
public class RazVisitor extends Visitor {
    public void visitFile(File f){
        System.out.println("RAZVisiting File "+ f.name);


        f.setContents("");
    }

    public void visitDirectory(Directory d){
        System.out.println("RAZVisiting Directory "+ d.name);

        ArrayList<ElementStockage> elementStockageList = new ArrayList<>(d.getElements());

        for(ElementStockage e : elementStockageList){
            e.accept(this);
        }
    }

    public void visitArchive(Archive a) {
        System.out.println("RAZVisiting Archive "+ a.name);
    }

    public void visitSymlink(Symlink s) {
        System.out.println("RAZVisiting Symlink "+ s.name);
    }

    public void visitLink(Link l) {
        System.out.println("RAZVisiting link "+l.name);
    }
}
