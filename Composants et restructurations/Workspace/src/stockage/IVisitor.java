package stockage;

/**
 * @author Forgh
 */
public interface IVisitor {
    void visitFile(File f);
    void visitArchive(Archive a);
    void visitDirectory(Directory d);
    void visitSymlink(Symlink s);
    void visitLink(Link l);
}
