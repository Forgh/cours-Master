package stockage;

/**
 * @author Forgh
 */
public class Visitor implements IVisitor {
    @Override
    public void visitFile(File f) {
        System.out.println("Visiting File "+ f.name);
        f.cat();
    }

    @Override
    public void visitArchive(Archive a) {
        System.out.println("Visiting Archive "+ a.name);
        a.extract();
    }

    @Override
    public void visitDirectory(Directory d) {
        System.out.println("Visiting Directory "+ d.name);
        d.ls();
    }

    @Override
    public void visitSymlink(Symlink s) {
        System.out.println("Visiting Symlink "+ s.name);
        System.out.println("Size : "+s.size());
        s.cat();
    }

    @Override
    public void visitLink(Link l) {
        System.out.println("Visiting link "+l.name);
        System.out.println("Size : "+l.size());
        l.cat();
    }
}
