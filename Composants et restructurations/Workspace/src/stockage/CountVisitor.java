package stockage;

import java.util.ArrayList;

/**
 * @author Forgh
 */
public class CountVisitor extends Visitor {
    private int count;

    public CountVisitor(){
        count = 0;
    }
    public void visitFile(File f){
        System.out.println("CountVisiting File "+ f.name);

        if(f.size() > 10)
            count++;
    }

    public void visitDirectory(Directory d){
        System.out.println("CountVisiting Directory "+ d.name);

        ArrayList<ElementStockage> elementStockageList = new ArrayList<>(d.getElements());

        for(ElementStockage e : elementStockageList)
            e.accept(this);
    }

    public void visitArchive(Archive a) {
    }

    public void visitSymlink(Symlink s) {
    }

    public void visitLink(Link l) {
    }

    public void raz(){
        count = 0;
    }

    public int getCount() {
        return count;
    }
}
