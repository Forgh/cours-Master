package stockage;

import java.util.ArrayList;

/**
 * @author Forgh
 */
public class FindVisitor extends Visitor{
    private String searched;
    private ArrayList<ElementStockage> found;

    public FindVisitor(String s){
        searched = s;
        found = new ArrayList<>();
    }

    public void visitFile(File f){
        System.out.println("FindVisiting File "+ f.name);
        if(f.getName().equals(searched))
            found.add(f);

    }

    public void visitDirectory(Directory d){
        System.out.println("FindVisiting Directory "+ d.name);

        if(d.getName().equals(searched))
            found.add(d);
    }

    public void visitArchive(Archive a) {
    }

    public void visitSymlink(Symlink s) {
    }

    public void visitLink(Link l) {
    }
}
