package com.example.mcampmas.tp2;

/**
 * Created by ghost_000 on 29/02/2016.
 */
public class Contact {
    private String nom;
    private String prenom;
    private String phone;

    public Contact(String nom, String prenom, String phone) {
        this.nom = nom;
        this.prenom = prenom;
        this.phone = phone;
    }

    public String getNom() {

        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String _toString(){
        return this.nom+" "+this.prenom;
    }

    public String toFile(){
        String newline = System.getProperty("line.separator");
        return this.getNom()+newline+this.getPrenom()+newline+this.getPhone();
    }
}
