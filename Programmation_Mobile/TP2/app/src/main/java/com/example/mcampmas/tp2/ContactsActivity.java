package com.example.mcampmas.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ContactsActivity extends AppCompatActivity {

    private String name, surname, phone, name2, surname2, phone2;
    private ArrayList<Contact> contacts;
    private ArrayList<String> contactStrings;
    private static SQLiteDatabase database = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        name = getIntent().getExtras().get("name").toString();
        surname = getIntent().getExtras().get("surname").toString();
        phone = getIntent().getExtras().get("phone").toString();
        System.out.println(phone);
        name2 = getIntent().getExtras().get("name2").toString();
        surname2 = getIntent().getExtras().get("surname2").toString();
        phone2 = getIntent().getExtras().get("phone2").toString();
        System.out.println(phone2);

        Contact newContact = new Contact(surname, name, phone);
        Contact newContact2 = new Contact(surname2, name2, phone2);

        contacts = new ArrayList<Contact>();
        contacts.add(newContact);
        contacts.add(newContact2);


        saveToFile(newContact);

        try {
            database = this.openOrCreateDatabase("TP2", MODE_PRIVATE, null);

            database.execSQL("CREATE TABLE IF NOT EXISTS Contact (name VARCHAR, surname VARCHAR, phone_number VARCHAR)");

            for (Contact c : contacts) {
                database.execSQL("INSERT INTO Contact Values ('"+c.getPrenom()+"','"+c.getNom()+"','"+c.getPhone()+"')");
            }
            Cursor cursor = database.rawQuery("SELECT name, surname FROM Contact", null);


            if (cursor != null) {
                // move cursor to first row
                if (cursor.moveToFirst()) {
                    do {
                        // Get version from Cursor
                        String name = cursor.getString(cursor.getColumnIndex("name"));
                        String surname = cursor.getString(cursor.getColumnIndex("surname"));
                        // add the strings to an array
                        contactStrings.add(name + " " + surname);
                        // move to next row
                    } while (cursor.moveToNext());
                }
            }

        }catch(SQLiteException se){
            Toast.makeText(getApplicationContext(), "Error creating the database", Toast.LENGTH_LONG).show();
        }finally {
            if (database != null){
                if (database != null) {
                    database.execSQL("DELETE FROM Contact");
                    database.close();
                }
            }

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contactStrings);

        final ListView list_view = (ListView)findViewById(R.id.listViewContact);
        list_view.setAdapter(adapter);
        list_view.setClickable(true);


        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                String item = (String)(list_view.getItemAtPosition(position));
                clickOnItem(position);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Toast.makeText(this, "Choisi: "+item.toString(), Toast.LENGTH_LONG).show();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void clickOnItem(int position){
        String contact = contactStrings.get(position);
       // Toast.makeText(this, contact, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, ContactFicheActivity.class);

        intent.putExtra("file","contact_"+contact+".txt");
        startActivity(intent);
    }

    public void saveToFile(Contact c){
        try {
            FileOutputStream file = openFileOutput("contact_"+c._toString()+".txt", MODE_PRIVATE);
            file.write(c.toFile().getBytes());
        }
        catch(IOException e){
            e.printStackTrace();
        }

    }
}
