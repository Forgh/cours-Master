package com.example.mcampmas.tp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void showContacts(View view)
    {
        Intent viewer = new Intent(this, ContactsActivity.class);

        viewer.putExtra("name",((EditText)findViewById(R.id.editName)).getText().toString());
        viewer.putExtra("surname",((EditText)findViewById(R.id.editSurname)).getText().toString());
        viewer.putExtra("phone",((EditText)findViewById(R.id.editPhone)).getText().toString());
        viewer.putExtra("name2",((EditText)findViewById(R.id.editName2)).getText().toString());
        viewer.putExtra("surname2",((EditText)findViewById(R.id.editSurname2)).getText().toString());
        viewer.putExtra("phone2",((EditText)findViewById(R.id.editPhone2)).getText().toString());
        startActivity(viewer);
    }
}
