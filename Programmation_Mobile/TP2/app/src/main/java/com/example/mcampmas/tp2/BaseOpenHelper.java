package com.example.mcampmas.tp2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ghost_000 on 15/02/2016.
 */
class MaBaseOpenHelper extends SQLiteOpenHelper {
    private static final int BASE_VERSION = 1;
    private static final String BASE_NOM = "planetes.db";
    private static final String TABLE_PLANETES = "table_planetes";
    public static final String COLONNE_ID = "id";
    public static final int COLONNE_ID_ID = 0;
    public static final String COLONNE_NOM = "nom";
    public static final int COLONNE_NOM_ID = 1;
    public static final String COLONNE_RAYON = "rayon";
    public static final int COLONNE_RAYON_ID = 2;
    /**
     * La requête de création de la structure de la base de données.
     */
    private static final String REQUETE_CREATION_BD = "create table "
            + TABLE_PLANETES + " (" + COLONNE_ID
            + " integer primary key autoincrement, " + COLONNE_NOM
            + " text not null, " + COLONNE_RAYON + " text not null);";
    /**
     * L'instance de la base qui sera manipulée au travers de cette classe
     */
    private SQLiteDatabase maBaseDonnees;
    public MaBaseOpenHelper(Context context, String nom, SQLiteDatabase.CursorFactory
            cursorfactory, int version) {
        super(context, nom, cursorfactory, version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_BD);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//Dans notre cas, nous supprimons la base et les données pour en
// créer une nouvelle ensuite.
        db.execSQL("drop table" + TABLE_PLANETES + ";");
// Création de la nouvelle structure.
        onCreate(db);
    }
}