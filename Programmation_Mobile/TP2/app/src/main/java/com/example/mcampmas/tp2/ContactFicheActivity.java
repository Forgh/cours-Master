package com.example.mcampmas.tp2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.IOException;

public class ContactFicheActivity extends AppCompatActivity {

    private String contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_fiche);

        try {
            FileInputStream file = openFileInput(getIntent().getExtras().get("file").toString());
            StringBuilder builder = new StringBuilder();
            int c;
            while((c = file.read()) != -1){
                builder.append((char)c);
            }
            contact = builder.toString();
        }
        catch (IOException e) {
            e.printStackTrace();
            contact = "File Error";
        }

        TextView view = (TextView)findViewById(R.id.contactInfo);
        view.setText(contact);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_fiche, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
