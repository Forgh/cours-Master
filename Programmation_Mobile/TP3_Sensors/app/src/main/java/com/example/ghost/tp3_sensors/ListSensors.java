package com.example.ghost.tp3_sensors;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListSensors extends AppCompatActivity {
    private ArrayList<String> sensorsList;
    private static SensorManager sensorService;
    private Sensor sensor, proximitySensor;
    private String name;
    private RelativeLayout layout;
    private long lastUpdate = 0;
    private float x,y,z,last_x,last_y,last_z;
    private static final int SHAKE_THRESHOLD = 800;
    private Context t;
    private boolean flashReady = true ;
    private static CameraManager cameraManager = null;

    private SensorEventListener accelerometerListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onSensorChanged(SensorEvent event) {

            int red = Math.abs(Math.round((event.values[0]/10)*255));
            int green = Math.abs(Math.round((event.values[1] / 10) * 255));
            int blue = Math.abs(Math.round((event.values[2]/10)*255));
            layout.setBackgroundColor(Color.rgb(red, green, blue));
            //System.out.println(event.values[0] + "," + event.values[1] + "," + event.values[2]);



            long curTime = System.currentTimeMillis();
            // only allow one update every 100ms.
            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                x = event.values[0];
                y = event.values[0];
                z = event.values[0];

                float speed = Math.abs(x+y+z - last_x - last_y - last_z) / diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                    try {
                        String[] cameras = cameraManager.getCameraIdList();
                        cameraManager.setTorchMode(cameras[0], flashReady);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                    flashReady = !flashReady;

                }
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    };

    private SensorEventListener proximityListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.values[0] < 1)
                Toast.makeText(t, "JEEZ, GO AWAY", Toast.LENGTH_SHORT);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sensor);

        t = this.getApplicationContext();

        sensorService = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        List<Sensor> sensors = sensorService.getSensorList(Sensor.TYPE_ALL);
        ArrayList<String> sensorsList = new ArrayList<String>();

        layout = (RelativeLayout)findViewById(R.id.listSensorsLayout);

        for (Sensor sensor : sensors){
            name = sensor.getName();
            sensor = sensorService.getDefaultSensor(sensor.getType());
            if (sensor == null)
                name += " { UNAVAILABLE }";
            sensorsList.add(name);
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sensorsList);

        final ListView list_view = (ListView)findViewById(R.id.listView);
        list_view.setAdapter(adapter);

        list_view.setClickable(true);

        sensor = sensorService.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (sensor != null) {
            sensorService.registerListener(accelerometerListener, sensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }

        proximitySensor = sensorService.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (proximitySensor != null) {
            sensorService.registerListener(proximityListener, sensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }


}
