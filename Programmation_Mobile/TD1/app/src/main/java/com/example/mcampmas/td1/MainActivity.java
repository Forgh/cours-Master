package com.example.mcampmas.td1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Button submitButton = (Button) findViewById(R.id.submit);
        //submitButton.setOnClickListener(submitHandler);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onConfirm(){
        Intent activity2 = new Intent(this, PrintFormActivity.class);
        activity2.putExtra("name", ((EditText) findViewById(R.id.name)).getText().toString());
        activity2.putExtra("surname", ((EditText) findViewById(R.id.surname)).getText().toString());
        activity2.putExtra("phonenumber", ((EditText) findViewById(R.id.phonenumber)).getText().toString());
        activity2.putExtra("birthdate", ((EditText) findViewById(R.id.birthdate)).getText().toString());
        activity2.putExtra("expertise", ((EditText) findViewById(R.id.expertise)).getText().toString());

        startActivity(activity2);
    }

    public void onSubmit(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Set other dialog properties
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);

        // Add the buttons
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                    onConfirm();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();


        dialog.show();
    }


}
