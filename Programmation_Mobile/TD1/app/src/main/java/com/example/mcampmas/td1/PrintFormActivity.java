package com.example.mcampmas.td1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class PrintFormActivity extends Activity {
    String phoneNb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_form);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String surname = intent.getStringExtra("surname");
        String birthdate = intent.getStringExtra("birthdate");
        String expertise = intent.getStringExtra("expertise");
        String phonenumber = intent.getStringExtra("phonenumber");
        this.phoneNb = phonenumber;
        String message = "Hi "+name+" "+surname+", " +    "you\'re born on the "+birthdate+", your phone number is "+phonenumber  +" and you're working in "+expertise;

        //System.out.println(message);
        ((TextView)findViewById(R.id.printText)).setText(message);


        //
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_print_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickButtonCall(View v) {
        Uri uri = Uri.parse("tel:" +phoneNb);
        Intent call = new Intent(Intent.ACTION_DIAL, uri);
        //call.setData(Uri.parse("tel:" + phoneNb));
        startActivity(call);
    }
}
