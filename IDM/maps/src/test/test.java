package test;

import maps.MapsPackage;
import maps.Pedestrian;
import maps.PublicSpace;
import maps.Road;
import maps.Square;
import maps.Street;
import maps.map;

import org.eclipse.emf.*;
import org.eclipse.emf.common.util.*;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;


public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Je charge l'instance map.xmi du méta-modèle maps.ecore
		Resource resource = chargerModele("model/map.xmi", MapsPackage.eINSTANCE);
		if (resource == null) System.err.println(" Erreur de chargement du modèle");
		
		
		//Instruction récupérant le modèle sous forme d'arbre à partir de la classe racine "map"
		map maMap = (map) resource.getContents().get(0);
						
		
		//obtenir le nom de la map
		System.out.println(maMap.getName());
		
		printStreets(maMap);
		printPedestrianGreaterThan1000(maMap);
		printNeighboringStreets(maMap);
		printBorderingStreetsFromSquare(maMap);
		
	}
	
	
	public static Resource chargerModele(String uri, EPackage pack) {
		   Resource resource = null;
		   try {
		      URI uriUri = URI.createURI(uri);
		      Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		      resource = (new ResourceSetImpl()).createResource(uriUri);
		      XMLResource.XMLMap xmlMap = new XMLMapImpl();
		      xmlMap.setNoNamespacePackage(pack);
		      java.util.Map options = new java.util.HashMap();
		      options.put(XMLResource.OPTION_XML_MAP, xmlMap);
		      resource.load(options);
		   }
		   catch(Exception e) {
		      System.err.println("ERREUR chargement du modèle : "+e);
		      e.printStackTrace();
		   }
		   return resource;
		}
	
	public static void printStreets(map m){
		
		System.out.println("Toutes les rues:");

		EList<Road> e = m.getRoads();
		for(Road r : e){
			if(r instanceof Street)
				System.out.println(r.getName());
		}
				
		
		
	}
	
	public static void printPedestrianGreaterThan1000(map m){
		System.out.println("Routes piétonnes plus grandes que 1000m: ");

		
		EList<Road> e = m.getRoads();
		for(Road r : e){
			if(r instanceof Pedestrian && r.getLength() > 1000)
				System.out.println(r.getName());
		}
				
		
		
	}
	
	public static void printNeighboringStreets(map m){
		System.out.println("Routes adjacentes de la route :");
		
		EList<Road> e = m.getRoads();
		for(Road r : e){
			if(r instanceof Street){
				System.out.println("   - "+r.getName());
				EList<Road> roads = r.getMeet();
				for(Road j : roads){
					System.out.println("      "+ j.getName());
				}
			}
		}
				
		
		
	}
	
	public static void printBorderingStreetsFromSquare(map m){
		System.out.println("Routes bordant la place :");
		
		EList<PublicSpace> e = m.getSpaces();
		for(PublicSpace p : e){
			if(p instanceof Square){
				System.out.println("   - "+ p.getName());
				EList<Road> roads = p.getBorderedBy();
				for(Road j : roads){
					System.out.println("      "+ j.getName());
				}
			}
		}
				
		
		
	}
	
	

}
