import java.io.*;
import java.util.*;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */

class Solution {


  
  static public String affiche(ArrayList<Bidon> b ){
    String s =  "";
    for(int i = 0 ; i < b.size(); i++){
      s += b.get(i).getVolume() + "  |  ";
      
    }
    return s;
    
  }
  public static void main(String[] args) {
   
     Summoner a = new Summoner();
  ArrayList<Bidon> bidons = new ArrayList<Bidon>();
        // Création des bidons
       for (int i = 0 ; i < 10; i++){
        bidons.add(new Bidon(10*i));
       }
    System.out.println(affiche(bidons));
    a.storeAndExecute(new Transvaser(bidons.get(7), bidons.get(8)));
    System.out.println(affiche(bidons));
    a.storeAndExecute(new Transvaser(bidons.get(8), bidons.get(9)));
    System.out.println(affiche(bidons));
    
    System.out.println("RETOUR ARRIERE");
    a.undo();
    System.out.println(affiche(bidons));
    a.undo();
    System.out.println(affiche(bidons));
   // a.undo();
    /*Scanner sc = new Scanner(System.in);
    String l;
    while(!l.equals("exit")){
      l = sc.nextLine();
      switch (l){
        case "undo":
          a.undo();
          break;
          case ""
      }
    }*/
    
    
   // a.storeAndExecute(new Remplir(bidons.get(2)));
    
    
    
    
    
  }
}

class Summoner{
  
  
      
  
     private List<ICommande> history = new ArrayList<ICommande>();

   public Summoner() {
  
   }

   public void storeAndExecute(ICommande cmd) {
      this.history.add(cmd); // optional 
      cmd.execute();        
   }
  
  public void undo(){
    if(this.history.size() != 0){
        this.history.get(this.history.size()-1).cancel();
        this.history.remove(this.history.size()-1);
    }
    else{
       System.err.println("Attention undo non possible");
    }
   
  
  }
}

 interface ICommande {
  void execute();
  void cancel();
}



 class Vider implements ICommande{
   private Bidon b;
   private int oldVolume;
   
   public Vider(Bidon b){
     this.b = b;   
     this.oldVolume = b.getVolume();
  }
  @Override
  public void execute() {
    this.b.vider();
    
  }
    public void cancel(){
      this.b.setVolume(this.oldVolume);
    
  }


}


class Remplir implements ICommande{
  
     private Bidon b;
     private int oldVolume;
   
   public Remplir(Bidon b){
     this.b = b;    
     this.oldVolume = b.getVolume();
  }

  @Override
  public void execute() {
    this.b.remplir();
    
  }
    public void cancel(){
    this.b.setVolume(this.oldVolume);
  }
}

class Transvaser implements ICommande{
  
  
  private Bidon a,b ;
  private int oldVola, oldVolb;
  
  public Transvaser(Bidon a, Bidon b){
     this.a = a;
     this.b = b;    
  }

  @Override
  public void execute() {
    this.oldVola = a.getVolume();
    this.oldVolb = b.getVolume();
    a.transvaser(b);
    
  }
  
  public void cancel(){
    this.a.setVolume(this.oldVola);
    this.b.setVolume(this.oldVolb);
  }

}



 class Bidon {
static  int capacity_max = 100;
  
  
  private int volume;// Combien j'ai d'eau
  
  public Bidon(){
    
  
    this.volume = 0; // Combien je peux encore avoir d'eau
    
  }
   
   public Bidon(int vol){
    
  
    this.volume = vol; // Combien je peux encore avoir d'eau
    
  }
   
   public int getVolume(){
     return this.volume;
   }

    public boolean isVide(){
    
    return this.volume == 0;
  }
   public boolean isFull(){
    
    return this.volume == capacity_max;
  }
  
   
  public void remplir(){
    
    this.volume = capacity_max;
  }
  
  public void vider(){
  
    this.volume = 0;
  }

  public void setVolume(int vol){
    this.volume = vol;
  }
   
  public void transvaser(Bidon b){
    if(!b.isFull()){ // Si le bidon b a encore de la place
      if(! this.isVide()){ // Si ce bidon n'est pas vide
        // Si le second tonneaux peux me contenir
        if((capacity_max - b.getVolume()) >= this.volume){
          // Je reçois de l'eau , ma capacité diminue 
          // de l'eau que contient l'autre tonneau
          b.setVolume(b.getVolume()+this.volume);
          this.volume = 0;
        }
        else{
          
            this.setVolume(this.getVolume()-(capacity_max-b.getVolume()));
            b.remplir();
          
        }        
        
      }
    }
  }

}