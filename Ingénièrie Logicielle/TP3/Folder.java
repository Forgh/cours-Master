package TP3;

import java.util.ArrayList;


public class Folder extends StockageSysteme {
    ArrayList<Stockage> stock;
    public Folder(){
        this.basicSize = 4;
    }
   
  public int size(){
      int s = 0;
      for(Stockage st : stock){
          s += st.size();
      }
      return s+this.basicSize;
  }
   
  protected int nbElement(){
      return this.stock.size();
  }
  
  protected String ls(){
    String s ="";
    for(Stockage st : stock){
      s += st.display();
    }
    return s;
  }
  
  protected String display(){
    return "";
  }
  
  protected String absoluteAddress(){
    return  this.parent.absoluteAddress()+"/"+this.name ;
  }
  
  protected boolean collectionFind(String name){
	  for(Stockage st : stock){
		  if( st.name == name ){ return true;}
		  
	  }
	  return false;
  }
  
  protected void add(Stockage s){
	  this.stock.add(s);
  }
  
}