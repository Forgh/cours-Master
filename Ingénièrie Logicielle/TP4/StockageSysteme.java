package TP3;

public abstract class StockageSysteme extends Stockage {
	  protected abstract int nbElement();  
	  protected abstract String display();
	  protected abstract String absoluteAddress();
	}
