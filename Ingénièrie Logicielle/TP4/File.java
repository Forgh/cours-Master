package TP3;

public class File extends StockageSysteme{
	   String content;
	   
	   public File(){
	     this.basicSize = 0;
	     this.content = "";
	   }
	   
	    public File(String str){
	      this.basicSize = 0;
	      this.content = str;
	   }
	   
	  public int size(){
	      return this.basicSize;
	  }
	  
	  protected String cat(){
		  
		  return this.content;
	  }
	   
	   
	  protected int nbElement(){
	    int counter = 0;
	    
	    for( int i=0; i<this.content.length(); i++ ) {
	      if( this.content.charAt(i) == ' ' ) {
	        counter++;
	      } 
	    }
	    
	    //on retourne le nombre de mot : il y a toujours un mot de plus que d'espaces
	    return counter+1;
	  }
	   
	  protected String display(){
	    return "";
	  }
	   
	  protected String absoluteAddress(){
	    return  this.parent.absoluteAddress()+"/"+this.name ;
	  }
	  
	}

	 class Link extends Stockage {
	  public int size(){
	    return this.basicSize;
	  }
	   
	     protected String display(){
	    return "";
	  }
	   
	  protected String absoluteAddress(){
	    return  this.parent.absoluteAddress()+"/"+this.name ;
	  }
	  
	}