package TP3;

import java.io.*;
import java.util.*;


/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */




public class Solution {
  public static void main(String[] args) {
    ArrayList<String> strings = new ArrayList<String>();
    strings.add("C'est bon pas de bug !");

    for (String string : strings) {
      System.out.println(string);
    }
  }
}
