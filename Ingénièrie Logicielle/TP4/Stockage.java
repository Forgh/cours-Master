package TP3;

public abstract class Stockage {
	 protected int basicSize;
	    protected String name;
	    protected Stockage parent;
	   
	    protected abstract int size();
	    protected abstract String display();
	    protected abstract String absoluteAddress();
}
