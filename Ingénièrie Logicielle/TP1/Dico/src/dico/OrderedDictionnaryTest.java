package dico;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class OrderedDictionnaryTest {

	OrderedDictionnary dico;
	
	@Before
	public void setUp() throws Exception {
		dico = new OrderedDictionnary();
	}

	
	
	@Test
	public void testAddOneElementToEmptyDico(){
		dico.put("pomme", "un fruit");
		dico.put("citron", "un fruit");
		
		
		assertEquals(dico.size(),2);
		assertTrue(dico.indexOf("pomme") > dico.indexOf("citron"));
		assertTrue(dico.containsKey("citron"));
	}
}
