package dico;


public abstract class AbstractDictionnary implements IDictionnary {
	protected Object[] key;
	protected Object[] value;

	protected abstract int indexOf(Object key);
	protected abstract int newIndexOf(Object key);
	
	public Object get(Object key) throws Exception{
		int i =this.indexOf(key);
		if(i != -1) return this.value[i];
		else throw new Exception("La clé" + key.toString() + " n\'est pas dans ce dictionnaire");
	}
	public boolean isEmpty(){
		int count = 0;
		for(int i =0; i < this.key.length; i++){
			if(this.key[i] != null)
				count++;
		}
		return (count == 0);
	}

	public void put(Object key, Object value) {
		int ind = this.indexOf(key) ;
		if(ind != -1) {
			this.value[ind] = value;
		}
		else {
			int newInd = this.newIndexOf(key); 
			this.value[newInd] = value;
		}
	}
	
}