package dico;
public interface IDictionnary {
	Object get(Object key) throws Exception;
	void put(Object key, Object value);
	boolean isEmpty();
	boolean containsKey(Object key);
	int size();
}