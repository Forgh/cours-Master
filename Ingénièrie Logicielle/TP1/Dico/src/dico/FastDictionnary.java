	

package dico;
 
public class FastDictionnary extends AbstractDictionnary {
       
       
        public FastDictionnary(int size){       
                key = new String[size];
                value = new String[size];
        }
       
        private boolean mustGrow(){
                return (size()> ((key.length*0.75)));
               
        }
       
       
        private void grow(int more){
               
                String[] tempskey = new String[this.key.length+more];
                String[] tempsvalue = new String[this.key.length+more];      
               
                String[] copiekey =  (String[]) key.clone();
                String[] copievalue = (String[]) value.clone();
               
 
 
       
                value = tempsvalue;
                key = tempskey;
               
               
               
                for(int i = 0 ; i < copiekey.length ; i++){
                        if (copiekey[i] != null){
                               
                                put(copiekey[i], copievalue[i]);
 
                        }
                       
                }
 
               
        }
       
       
 
        protected int indexOf(String Key) {
       
 
                int index = Key.hashCode(); // On prend la valeur du has ...
           
            if(index < 0){ // Si la valeur est négative on change
                    index = index *-1;
            }
           
   
           
            if(index < key.length){ // Si la valeur ne dépasse pas la longueur total du tableau
                    /*
                     * Tant que la clef n'est pas trouvé on la cherche
                     * Si on dépasse la taille du tableau : Problème : Retourne -1
                     */
                   
                   
                   
                    while(index < key.length && key[index] != Key ){
                            index++;
                    }
                   
                    // Erreur
                    if(index > key.length){
                           
                            return -1;
                    }
                    // On retourne l'index
                    else return index;
                   
            }
            else {
                   
                    // L'indice est trop grand , on prend le modulo , on refait la même opperation que si dessus
                    index = index%key.length;
                   
                    while(index < key.length && key[index] != Key ){
                            index++;
                    }
                   
                   
                    //System.out.println("Je passe ici" + index );
                    // Erreur
                    if(index >= key.length){
                           
                            return -1;
                    }
                    // On retourne l'index
                        else return index;
                       
                }
               
               
        }
 
        protected int newIndexOf(String Key) {
       
                if(mustGrow()){
                        grow(key.length/2);
                }
                int index = Key.hashCode(); // On prend la valeur du has ...
                if(index < 0 ) index = index *-1;
                if(index > key.length){
                        index = index%key.length;
                }
               
                while(key[index] != null){
                        index++;
                       
                }
               
                key[index] = Key;
                       
                       
               
               
                return index;
        }
 
        protected boolean moreThanOneElement() {
               
                return size()>1;
        }
       
       
        public void affichage(){
                for(int i = 0 ; i < key.length ; i++ ){
                        System.out.println(i +" "+key[i] + " : " + value[i]);
                }
               
               
               
        }

		public boolean containsKey(Object key) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		protected int indexOf(Object key) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		protected int newIndexOf(Object key) {
			// TODO Auto-generated method stub
			return 0;
		}

		public int size() {
			// TODO Auto-generated method stub
			return 0;
		}

 
}

