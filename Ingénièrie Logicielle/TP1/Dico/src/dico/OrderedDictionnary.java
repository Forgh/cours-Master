	

    package dico;
    public class OrderedDictionnary extends AbstractDictionnary {
     
        public OrderedDictionnary() {
            //super(0);
            key = new Object[0];
            value = new Object[0];
        }
 
 
 
        public int size() {
            int s = 0;
            for (int i = 0; i < this.key.length; i++) {
                if (this.key[i] != null)
                    s++;
            }
            return s;
        }
 
        public int indexOf(Object key) {
            for (int i = 0; i < this.key.length; i++) {
                if (key.equals(this.key[i])){
                    return i;
                }
                           
            }
           
            return -1;
        }
        
        protected boolean compareWord(String a, String b ){ // Return true si le mot vient après le mot B
               
                if (a.charAt(0) > b.charAt(0)){
                       
                        return true;
                }
                if(a.charAt(0) == b.charAt(0)) return compareWord(a.substring(0),b.substring(0));
               
                return false;  
               
        }
        protected int newIndexOf(Object key) {
            boolean inserate = false; // Ai-je déjà inséré la clef?
            String k = key.toString();
           
            int size = this.size();
            if (size == this.key.length) { // SI le tableau est plein
                Object[] newKeys = new Object[this.key.length + 1];
                Object[] newValues = new Object[this.key.length + 1];
                int index =0; // Indexe de l'endroit ou j'insère la clef
                for (int i = 0; i < this.key.length; i++) {
                    if(!inserate){
                        if(compareWord(k,this.key[i].toString())){
                            newKeys[i] = this.key[i];
                            newValues[i] = this.value[i];
                        }
                        else{
                            newKeys[i] = k;
                            index = i;
                            inserate = true;
                            newKeys[i+1] = this.key[i];
                            newValues[i+1] = this.value[i];
                        }
                           
                    }
                    else{
                        newKeys[i+1] = this.key[i];
                        newValues[i+1] = this.value[i];
                    }
                }
 
                this.key = newKeys;
                this.value = newValues;
                this.key[index] = k;
       
                return index;}
                // Sinon je retourne la taille
           
            else {
                this.key[size] = key;
                return size;
            }
               
        }
 
 
 
        public boolean containsKey(Object key) {
            return indexOf(key) !=-1 ;
        }
 
    }

