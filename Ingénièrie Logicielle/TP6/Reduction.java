package TP6;

public class Reduction extends DecorateurCompte{

    public Reduction(CompteAbstrait c){
     this.c = c;
   }
   public double prixLocation(Produit p){
	   
      return this.c.prixLocation(p) * 0.9; 
   } 
 
}
