package TP6;

public class Produit {
	  protected String name;
	  protected double value;
	  
	 Produit(String name, double value){
	    this.name = name;
	    this.value = value;
	  }
	  
	  public double prixLocation(){
	    return this.value;
	  }
}
