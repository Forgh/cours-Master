package TP6;

public class Seuil extends DecorateurCompte {
    private int nbLocation;
    public Seuil(CompteAbstrait c){
      this.c = c;
      this.nbLocation = 0;
    }
  
      public double prixLocation(Produit p){
        if(this.nbLocation == 3){
          this.nbLocation =0;
           return 0; 
        }
        else{
           this.nbLocation++;
           return this.c.prixLocation(p); 
        }
      
    } 
}
