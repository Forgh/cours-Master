function Shape(state, x, y, w, h, fill){
  this.state = state;
  this.x = x || 0;
  this.y = y || 0;
  this.w = w || 1;
  this.h = h || 1;
  this.fill = fill || '#AAAAAA';
}


Shape.prototype.draw = function (mx, my){

}


Shape.prototype.contains = function (...) {
  "use strict";
  // All we have to do is make sure the Mouse X,Y fall in the area between
  // the shape's X and (X + Height) and its Y and (Y + Height)
  return  (this.x <= mx) && (this.x + this.w >= mx) &&
          (this.y <= my) && (this.y + this.h >= my);
}


function CanvasState (...) {
	 // **** Then events! ****
	  
	  // This is an example of a closure!
	  // Right here "this" means the CanvasState. But we are making events on the Canvas itself,
	  // and when the events are fired on the canvas the variable "this" is going to mean the canvas!
	  // Since we still want to use this particular CanvasState in the events we have to save a reference to it.
	  // This is our reference!
	  myState = this;
	  
	  //fixes a problem where double clicking causes text to get selected on the canvas
	  canvas.addEventListener('selectstart', function(e) { e.preventDefault(); return false; }, false);
	  // Up, down, and move are for dragging
	  canvas.addEventListener('mousedown', function(e) {
	    var mouse, mx, my, shapes, l, i, mySel;
	    if (myState.expectResize !== -1) {
	      myState.resizeDragging = true;
	      return;
	    }
	    mouse = myState.getMouse(e);
	    mx = mouse.x;
	    my = mouse.y;
	    shapes = myState.shapes;
	    l = shapes.length;
	    for (i = l-1; i >= 0; i -= 1) {
	      if (shapes[i].contains(mx, my)) {
	        mySel = shapes[i];
	        // Keep track of where in the object we clicked
	        // so we can move it smoothly (see mousemove)
	        myState.dragoffx = mx - mySel.x;
	        myState.dragoffy = my - mySel.y;
	        myState.dragging = true;
	        myState.selection = mySel;
	        myState.valid = false;
	        return;
	      }
	    }
	    // havent returned means we have failed to select anything.
	    // If there was an object selected, we deselect it
	    if (myState.selection) {
	      myState.selection = null;
	      myState.valid = false; // Need to clear the old selection border
	    }
	  }, true);
	  canvas.addEventListener('mousemove', function(e) {
	    var mouse = myState.getMouse(e),
	        mx = mouse.x,
	        my = mouse.y,
	        oldx, oldy, i, cur;
	    if (myState.dragging){
	      mouse = myState.getMouse(e);
	      // We don't want to drag the object by its top-left corner, we want to drag it
	      // from where we clicked. Thats why we saved the offset and use it here
	      myState.selection.x = mouse.x - myState.dragoffx;
	      myState.selection.y = mouse.y - myState.dragoffy;   
	      myState.valid = false; // Something's dragging so we must redraw
	    } else if (myState.resizeDragging) {
	      // time ro resize!
	      oldx = myState.selection.x;
	      oldy = myState.selection.y;
	      
	      // 0  1  2
	      // 3     4
	      // 5  6  7
	      switch (myState.expectResize) {
	        case 0:
	          myState.selection.x = mx;
	          myState.selection.y = my;
	          myState.selection.w += oldx - mx;
	          myState.selection.h += oldy - my;
	          break;
	        case 1:
	          myState.selection.y = my;
	          myState.selection.h += oldy - my;
	          break;
	        case 2:
	          myState.selection.y = my;
	          myState.selection.w = mx - oldx;
	          myState.selection.h += oldy - my;
	          break;
	        case 3:
	          myState.selection.x = mx;
	          myState.selection.w += oldx - mx;
	          break;
	        case 4:
	          myState.selection.w = mx - oldx;
	          break;
	        case 5:
	          myState.selection.x = mx;
	          myState.selection.w += oldx - mx;
	          myState.selection.h = my - oldy;
	          break;
	        case 6:
	          myState.selection.h = my - oldy;
	          break;
	        case 7:
	          myState.selection.w = mx - oldx;
	          myState.selection.h = my - oldy;
	          break;
	      }
	      
	      myState.valid = false; // Something's dragging so we must redraw
	    }
	  
	    // if there's a selection see if we grabbed one of the selection handles
	    if (myState.selection !== null && !myState.resizeDragging) {
	      for (i = 0; i < 8; i += 1) {
	        // 0  1  2
	        // 3     4
	        // 5  6  7
	        
	        cur = myState.selectionHandles[i];
	        
	        // we dont need to use the ghost context because
	        // selection handles will always be rectangles
	        if (mx >= cur.x && mx <= cur.x + myState.selectionBoxSize &&
	            my >= cur.y && my <= cur.y + myState.selectionBoxSize) {
	          // we found one!
	          myState.expectResize = i;
	          myState.valid = false;
	          
	          switch (i) {
	            case 0:
	              this.style.cursor='nw-resize';
	              break;
	            case 1:
	              this.style.cursor='n-resize';
	              break;
	            case 2:
	              this.style.cursor='ne-resize';
	              break;
	            case 3:
	              this.style.cursor='w-resize';
	              break;
	            case 4:
	              this.style.cursor='e-resize';
	              break;
	            case 5:
	              this.style.cursor='sw-resize';
	              break;
	            case 6:
	              this.style.cursor='s-resize';
	              break;
	            case 7:
	              this.style.cursor='se-resize';
	              break;
	          }
	          return;
	        }
	        
	      }
	      // not over a selection box, return to normal
	      myState.resizeDragging = false;
	      myState.expectResize = -1;
	      this.style.cursor = 'auto';
	    }
	  }, true);
	  canvas.addEventListener('mouseup', function(e) {
	    myState.dragging = false;
	    myState.resizeDragging = false;
	    myState.expectResize = -1;
	    if (myState.selection !== null) {
	      if (myState.selection.w < 0) {
	          myState.selection.w = -myState.selection.w;
	          myState.selection.x -= myState.selection.w;
	      }
	      if (myState.selection.h < 0) {
	          myState.selection.h = -myState.selection.h;
	          myState.selection.y -= myState.selection.h;
	      }
	    }
	  }, true);
	  // double click for making new shapes
	  canvas.addEventListener('dblclick', function(e) {
	    var mouse = myState.getMouse(e);
	    myState.addShape(new Shape(myState, mouse.x - 10, mouse.y - 10, 20, 20, 'rgba(0,255,0,.6)'));
	  }, true);
	  
	  // **** Options! ****
	  
	  this.selectionColor = '#CC0000';
	  this.selectionWidth = 2;  
	  this.selectionBoxSize = 6;
	  this.selectionBoxColor = 'darkred';
	  this.interval = 30;
	  setInterval(function() { myState.draw(); }, myState.interval);
	
} 

CanvasState.prototype.addShape(){};

CanvasState.prototype.clear(){};

CanvasState.prototype.draw(){};

CanvasState.prototype.getMouse(){};


function init(){
  "use strict";
  var s = new CanvasState(document.getElementById('canvas1'));
  // add a large green rectangle
  s.addShape(new Shape(s, 260, 70, 60, 65, 'rgba(0,205,0,0.7)'));
  // add a green-blue rectangle
  s.addShape(new Shape(s, 240, 120, 40, 40, 'rgba(2,165,165,0.7)'));  
  // add a smaller purple rectangle
  s.addShape(new Shape(s, 5, 60, 25, 25, 'rgba(150,150,250,0.7)'));
};