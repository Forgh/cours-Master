function drawLines(){
	var canvas = document.getElementById('myCanvas');

  	if (canvas.getContext){
    	var ctx = canvas.getContext('2d');

		ctx.strokeStyle="black";

	  	//dessiner abscisse
	  	var abs=new Path2D();
		abs.moveTo(0,300);
		abs.lineTo(800,300);
		ctx.stroke(abs);
	    
	    //dessiner ordonnée
	  	var ord=new Path2D();
		ord.moveTo(400,0);
		ord.lineTo(400,600);
		ctx.stroke(ord);
	}
}

function draw(fn) {
	var canvas = document.getElementById('myCanvas');

  	if (canvas.getContext){
    	var ctx = canvas.getContext('2d');

		ctx.strokeStyle="red";

	  	var val = 0;
	  	if(fn == "cos"){
	    	for(var i=-400; i < 400; i++){
	    		
	    		var point=new Path2D();
	    		
	    		point.moveTo(i-1+400,300-val);
	    		
	    		val = Math.cos(i);

	    		point.lineTo(i+400,300-val);
	    		ctx.stroke(point);


	    	}
    	} else if (fn == "x2"){
			for(var i=-400; i < 400; i++){
	    		
	    		var point=new Path2D();
	    		
	    		point.moveTo(i-1+400,300-val);
	    		
	    		val = i*i;

	    		point.lineTo(i+400,300-val);
	    		ctx.stroke(point);
	    	}

    	}
	}


}