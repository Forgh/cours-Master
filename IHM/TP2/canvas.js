function draw() {
  var canvas = document.getElementById('myCanvas');
  if (canvas.getContext){
    var ctx = canvas.getContext('2d');

	ctx.fillStyle = 'gray';
   	ctx.fillRect(10,105,600, 50);	
    
    var tri1=new Path2D();
    tri1.moveTo(0,100);
    tri1.lineTo(100,100);
    tri1.lineTo(100,200);
	ctx.fillStyle = 'black';
    ctx.fill(tri1);

    var tri2=new Path2D();
    tri2.moveTo(20,110);
    tri2.lineTo(90,110);
    tri2.lineTo(90,180);
    ctx.fillStyle = 'gray';
    ctx.fill(tri2);

	var tri3=new Path2D();
    tri3.moveTo(95,160);
    tri3.lineTo(195,160);
    tri3.lineTo(145,100);


    tri3.moveTo(145,110);
    tri3.lineTo(180,155);
    tri3.lineTo(110,155);

    var gradient = ctx.createLinearGradient(110,110,110,160);
    gradient.addColorStop(0,"black");
    gradient.addColorStop(1,"white");
    
    ctx.fillStyle = gradient;

		ctx.shadowOffsetX = 5;
		ctx.shadowOffsetY = 5;
		ctx.shadowBlur = 5;
		ctx.shadowColor = 'black';

ctx.fill(tri3);
  }
}