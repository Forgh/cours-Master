function drawSquares(){
	var canvas = document.getElementById('myCanvas');

  	if (canvas.getContext){
    	var ctx = canvas.getContext('2d');

		ctx.strokeStyle="black";

	  	//dessiner abscisse
	  	var abs=new Path2D();
		abs.moveTo(0,300);
		abs.lineTo(800,300);
		ctx.stroke(abs);
	    
	    //dessiner ordonnée
	  	var ord=new Path2D();
		ord.moveTo(400,0);
		ord.lineTo(400,600);
		ctx.stroke(ord);
	}
}

