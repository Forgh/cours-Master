(function () {
    var app = angular.module('autocomplete-custom', []);

    app.directive("autocompleteSearch", function() {
        return {
            restrict: 'E',
            templateUrl: "views/autocomplete.html",
            scope: {
                maxResults: "=maxResults"||5
            },
            controller: ['$scope', '$http', function($scope,$http) {
                var index = 0;

                $scope.suggestions = [];
                $scope.val = "";

                $scope.change = function(){
                    if(!$scope.val) $scope.suggestions = []; //input is empty
                    else {
                        $http.get("response.json").success(function(data){
                            $scope.suggestions = data.filter(function (obj) {   //filtering
                                return Object.keys(obj)
                                    .some(function (k) {
                                        return obj[k].toLowerCase().indexOf($scope.val.toLowerCase()) !== -1; //not case-sensitive
                                    })
                            }).slice(0,$scope.maxResults); //we only want the X first values
                        });
                    }

                };

                $scope.key = function(event){
                    switch(event.keyCode) {
                        case 13:                                                                        //enter
                            $scope.val = $scope.suggestions[index].label;
                            $scope.suggestions =[];
                            break;
                        case 38:
                            if (index > 0)
                                index--;
                            break;
                        case 40:
                            if (index < $scope.suggestions.length-1)     //arrow down
                                index++;
                            break;
                        default:
                    }
                };

                $scope.click = function(value){
                    $scope.val = value.label;
                    $scope.suggestions =[];
                };

                $scope.isActive = function(val){
                    if(val == index) return 'active';
                }
            }],
            controllerAs: "autocomplete"
        };
    });
})();
