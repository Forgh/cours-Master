$.widget("fg.autocompleteJQuery", {
    options: {
        target: "response.json",
        maxResults: 5,
        refreshRate: 300,
        width: 300,
        class: "custom-autocomplete",
        classField: "custom-autocomplete-field form-control",
        classResult: "custom-autocomplete-item",
        classMenu: "custom-autocomplete-menu list-group",
        classMenuItem: "custom-autocomplete-item list-group-item"
    },

    //constructor
    _create: function () {

        this.element.addClass(this.options.class);

        this.index = 0;
        this.count = 0;

        var that=this;
        var timer;


        //we create the input field that'll be autocompleted
        this.field = $( "<input>", {
            type: "text",
            class: that.options.classField,
            width: that.options.width,
            placeholder: "Search",
        }).appendTo( this.element );


        //this "menu" will act as the list of suggestion
        this.menu = $("<div>", {
                class: that.options.classMenu,
                width: that.options.width
            }
        ).insertAfter(this.field);



        //we now add a listener to make sure that, everytime something is typed,
        //the input's value will be looked for.
        //oninput is the most "sensitive" one: it also detects when the user pastes something.
        this.field.on("input",function(){
            clearInterval(timer);  //if something was typed up, we clear the previous timer
            timer = setTimeout(function() { //we give the AJAX call a certain amount of time to be fired
                that._menuReset();
                that._searchAJAX(that.field.val());
            }, that.options.refreshRate);


        });


        //listener for keyboard navigation
        this.field.on("keypress", function(e){
            var menuitem = "."+that.options.classResult;

            switch(e.keyCode) {
                case 13:                                                                        //enter
                    that._menuSelect($(menuitem).eq(that.index).text());
                    break;
                case 38:
                    if(that.index > 0) {
                        $(menuitem).eq(that.index).removeClass("active");                       //arrow up
                        that.index--;
                        $(menuitem).eq(that.index).addClass("active");
                    }
                    break;
                case 40:
                    if(that.index < that.options.maxResults &&  that.index < that.count-1){     //arrow down
                        $(menuitem).eq(that.index).removeClass("active");
                        that.index++;
                        $(menuitem).eq(that.index).addClass("active");
                    }
                    break;
                default:
            }
        });
    },



    _searchAJAX: function (value) {
        var that = this;

        //the search bar is empty
        if(!value)
            return;


        $.ajax({
            dataType: "json",
            url: that.options.target
        }).done(function (response) {
            //response is filtered by JavaScript (helps with JSON-only source).
            var filtered = that._filterValuePart(response,value);
            //rendering the menu
            that._renderMenu(that._normalize(filtered));
            $(".custom-autocomplete-item").eq(that.index).addClass("active")
        });
    },



    //go through the JSON file and convert it to a JQuery map format
    //the user should either modify this method to suit the JSON file
    //Or change the way the JSON file is written
    _normalize: function (items) {

        return $.map(items, function (item) {
            if (typeof item === "string") {         //if there's only one result
                return {
                    label: item,
                    value: item
                };
            }
            return $.extend({}, item, {             //if there's more than one
                label: item.label || item.value,
                value: item.value || item.label
            });
        });
    },

    //rendering every suggestion into a menu
    _renderMenu: function (items) {
        var that = this;
        $.each(items, function (index, item) {
            that._renderItemData(item);
            that.count++;
        });
    },


    //adding the menu item class(es)
    _renderItemData: function (item) {
        return this._renderItem(item).addClass(this.options.classMenuItem);
    },


    //append to a menu item its label and listenener
    _renderItem: function (item) {
        var that=this;
        return $("<a href='#'>").text(item.label)
                        .click(function() {             //click listener: will add the value to input
                            that._menuSelect(item.label);
                        })
                        .appendTo(this.menu);
    },

    _destroy: function () {
        this.element
            .removeClass("custom-autocomplete")
            .text("");
    },

    //select a value
    _menuSelect: function(label){
        this.field.val(label); //the user has made its
        this._menuReset();
    },

    //emptying the menu and resetting values
    _menuReset: function(){
        this.menu.empty();
        this.index=0;
        this.count=0;
    },

    //filter the received JSON to suit the requested terms
    _filterValuePart: function(arr, part) {
        part = part.toLowerCase();

        return arr.filter(function (obj) {
            return Object.keys(obj)
                .some(function (k) {
                    return obj[k].toLowerCase().indexOf(part) !== -1; //not case-sensitive
                });
        }).slice(0,this.options.maxResults); //we only want the X first values
    }

});
