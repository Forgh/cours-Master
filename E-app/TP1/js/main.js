$(document).ready(function() {
    "use strict";

    var tp1 = {
        q11: function(){
            let q1 = $('#tp1-1');
            q1.find('ul li').eq(0).css('text-decoration','bold');
            q1.find('.description').insertBefore(q1.find("ul"));
        },
        q12: function(){
            let q2 = $('#tp1-2').find("input");
            q2.focusout(function(){
                console.log(q2.val());
            });
        },
        q13: function(){
            $(".todo").fadeOut(800);
        },
        q14: function(){
            $("#chuck").load("chuck.html");
        }
    };

    tp1.q11();
    tp1.q12();
    tp1.q13();
    tp1.q14();






    //$.ema();

});
