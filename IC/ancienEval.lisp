(require "lisp2liV2.lisp")

(defun eval-li (expr env)
 	(case (car expr)
 		(:CONST (cdr expr))
 		(:LIT 
 			(cdr expr))
  
 		(:VAR
 			(aref env (- (cadr expr) 1)))

 		(:SET-VAR 
 			(setf 
 			(aref env (cadr expr)) 
 			(eval-li (cddr expr) env))
		)
		(:IF 
			(if (eval-li (second expr) env)
				(eval-li (third expr) env)
				(eval-li (fourth expr) env)
 			)
 		)
 		(:CALL 
 			(if (eq (cadr expr) 'set-defun )
             	(apply (second expr) (eval-li (caddr expr) env) (cdddr expr))
 				(apply (second expr) (map-eval-li (cddr expr) env))
        	)
		)
		;`(:CALL set-defun (:LIT.,test) (:lit.(:LAMBDA.,0.,(:CALL + (:LIT . 1) (:LIT . 1)))))
		(:MCALL 
			(let ((fun (get-defun (second expr))) (args (map-eval-li (cddr expr) env)) (nbParam (caddr (get-defun (second expr)))))
			 	(eval-li (fourth fun) ; third fun -> cddr fun
		 			(make-env-evalLI nbParam args)
			 	)
			)

		)
 		(:UNKNOWN 
 			(let (( nexpr (lisp2li (second expr) (cddr expr))))
 				(if (eq (car nexpr :UNKNOWN)
					(warn "eval-li : ~s" expr)
 					(eval-li (displace expr nexpr)))
 				)
 			)
		)
		(:progn 
      		(car (last (map-eval-li  (car expr) env)))
  		) 
		(t
			(warn "eval-li : ~s n'est pas dans le LI" (car expr))
		)
	)
)

(defun displace (cell1 cell2)
	(setf (car cell2) (car cell1)
		(cdr cell2) (cdr cell1))
	cell2)

(defun make-env-evalLI (taille liste)
	 (let ((tab (make-array taille)))
        (loop for x from 0 to (- taille 1)
            do
            (setf (aref tab x) (nth x liste))
        )
        tab
    )
)

(defun map-eval-li (lexpr env)
	(if (atom lexpr)
		()
		(cons (eval-li (car lexpr) env) (map-eval-li (cdr lexpr) env))
	)
)

(trace eval-li)
(print (eval-li (lisp2li '(defun fa (x) (if (<= x 0) 1 (* x (fa (- x 1))))) ()) ()))
;(eval-li (lisp2li '(fa 5) ()) ())