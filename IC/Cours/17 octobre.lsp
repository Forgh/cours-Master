;expr évaluée de LISP --> expression LI
; 1 --> (:LIT . 1)
; x --> (:VAR . 1)   --> 1 est la position de la var x dans l'environnement
; (IF _) --> (:IF _)
; (foo _) --> (:call _)


; 2 fonctions lisp1li | map-lisp2li

; <glue> = cons
; env = l_iste de variables
; expr = expression lisp

; cas litéral (atom+constantp) -> 
(cons :LIT expr))

; cas variable
(cons :VAR (position expr env))

;cas d'une fonction  
(cons :CALL (cons fun (map-lisp2li args)))

; cas de QUOTE
`(:LIT . ,(first args ))


(IF e1 e2 e3) --> (IF e1 e2 . e3)

;cas IF
(cons IF 
	(cons (first args))
		(cons(lisp2li(second args)) (lisp2li(third args)))
)

(list* :IF (lisp2li(first args))
			(lisp2li(second args))
			(lisp2li(third args)))

`(:IF  ,(lisp2li(first args))
	   ,(lisp2li(second args))
	  .,(lisp2li(third args)))
			
			
;petites fonctions LISP de base
(cons e1 
	(cons e2
		(cons e3 ())))

(list e1 e2 e3)

(list 1 2 3)           =          '(1 2 3)
;construite par EVAL		construite par READ

(list* 1 2 3) = '(1 2 . 3)

;quote
'(1 2 3) --(READ)--> (QUOTE (1 2 3))

;backquote
`( ,  ) <-- squelette
;   |__ valeur d'expression à insérer

`(1 , x 2, y 3, 7)
; construction d'une expression par READ cons + LIST + APPEND
(list '1 x '2 y '3 7)

; construction de la liste soustraite par EVAL



;cas UNKNOWN
; cas à 
(symbolp fun) --> vrai
(fboundp fun) --> faux
==> `(:UNKNOWN ,expr3 .,env)  ;--> vrai (dans l'analyse par cas)
;Valeur fonctionnelle du LI 
; expr LI du corps
; +
; nb de paramètre
(get-defun 'fact) --> (:LAMBDA 1 . (:IF _))

(eval-li (lisp2li (defun fact ...))) -->refaire
(get-defun 'fact) --> (:lambda 1 . (if _))
									  ;|->:UNKNOWN la première fois
									  ; La deuxième fois -> :MCALL

;version provisoire
; l'effet de bord en LISP
`(set-defun ,(first args (:LAMBDA _)))									  
(eval (lisp2li '(defun _))) 



(lisp2li `(f'(= n 0) 
			1 
			(x n (fact (- n 1)))) 
			'(n))
			
--> (:IF (:CALL = (:VAR . 1) (:LIT 0)) (:LIT . 1)
		(:CALL * (:VAR . 1) (:UNKNOWN (fact (- n 1)) . (n))))

;1  -que faire de defun ?
(defun bar (x y z) ...)
(defun foo(...) (bar ...) env)

(lisp2li '(defun fact __) ())
; |--> fonctionnel => pas d'effet de bord

;cas defun dans lisp2li
--> `(:CALL SET-DEFUN ,(first args)
			(:LAMBDA ,(length (second args))
					 ,(lisp2li(third args) (second args)))
					 ; /!\ -> corrigé !

;2 - qu'est ce que fait defun ?
; il construit une valeur fonction
fact(lambda(n) (if _))
;+ associe la valeur fonction au symbol fact 


;3 - de quoi a-t-on besoin en matière de déf de fonction ?
;fonctions connus par LISP uniquement 
first, rest, cons, +...$ ; -> fonctions "prédéfinis"

;fonctions connus par notre env d'exécution
;--> fonctions méta-définis
					
					
						

					
;cas des fonctions
;2 cas 

;prédéfini
(:CALL 

;méta-défini 
(:MCALL


;associer qqchose à autre chose 
;--> principe du hachage (hashmap)

;associer qqchose à un symbole LISP
;listes de propriétés des symboles
;un symbole = 	type
;				structure de données
;					- son nom
;					- sa valeur fonctionnelle
;					- list de propriété

;une propriété = symbole 

		;	 |->propriété
(get `toto `titi) ;--> NIC par défaut ou la valeur associée
	 ; |->symbole

(get 'fact :defun)


(defun get-defun (symb)
		get symb . defun)
		
(defun set-defun (symb expr-lambda)
		(setf(get symb :defun)
			expr-lambda))
			
			
;affectation en LISP
;form syntaxique SETF
				 ; |-> expression évaluable évaluée
;(SETF <place> <valeur>)
;		|->  expr évaluable mais pas évaluée, un variable a bien la lecture d'un "champ"




