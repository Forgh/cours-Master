(defun parcours-lisp1 (expr env) ;environnement : ensemble de variables et + si affinités
	(if (atom expr)
		(if (literalp expr)
			1
			(if (member expr env) ;vérifier que le symbole a été déclaré comme une variable 
				; cas des variable
				(warn "~s n'est pas une variable" expr)
			) 	; dans toutes les fonctions et leurs appels 
		)		; parcours-lisp*
		(let (fun (first expr) (args (rest expr))))
	)			; map-parcours-lisp*
)				; rajouter le paramètre d'environnement


;cas defun
(if (eq 'defun fun)
	(parcours-lisp1 (third args) (second args))
	;cas λ-expressions 
	(<glue> (parcours-lisp1(third fun)(append (second fun) env))
			(map-parcours-lisp1 args env))
		(parcours-lisp1 '(defun length ...))
)

;2 fonctions parcours-lisp map-parcours-lisp
;- <glue> +, cons, appen, progn
;- traiter (tester) tous les cas (y compris les cas d'erreurs)		
;- un seul usage réel : transformation vers Langage Interprété

;langage intermédiaire
; - Langage imbriqué coommon LISP (et non pas un lalngage linéaire comme un lanage machine)
; - tous les cas d'expression évaluable de LISP
;		- simplifiés, regroupés
;		- excluant les cas d'erreurs statiques
; - tags pour identifier les cas
; - se débarasser des nums de variables -> position dans l'environnement

;les cas du LI 
; - litéraux
; - lecture de variable
; - affectation de variable
; - appels de fonctions
; - IF
; - cas UNKNOWN


; '(1 2 3) = literal
; defun foo -> 	1 - génère le code du LI du corps de foo
;				2 - en faire une "valeur fonctionnelle"
;				3 - associer au symbole foo

;syntaxe du LI BNF 
(:LIT . <espriLISp>) ; litéraux, syntaxiquement correct mais pas forcément évaluable
(:VAR . <int>) ; variables
(:SET-VAR . <int> . <exprLI>) ; affectation
(:CALL <fun> <exprLI>* ) ; appel de fonction
(:IF <expr> <exprLI> . <exprLI>) ; IF

(:UNKNOWN <exprOvalLisp> . <env>)

(:UNKNON (foo x y) . (x y))

<exprLI> := <exprLIT> | <exprVAR> | <exprSET-VAR> | <exprIF> | <exprCALL>  | <exprUNKNON>

(:IF (:CALL (:VAR . 1)(:LIT . 0))
	(:LIT . 1)
	(:CALL * (:VAR . 1)
		(:UNKNOWN (fact (- n 1)) . (n)))))


(defun parcours-LI1 (exprli env)
	(assert (consp exprli))
	(case (car exprli)
		(:LIT ____)
		(:VAR ____)
		(:SET-VAR ____)
		(:IF ____)
		(:CALL ____)
		(:UNKNOWN ____)))
		
		
		
