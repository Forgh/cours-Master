(defun li2asm (expr param)
	(case (first expr)
		(:LIT 
			(list
				(list 
					'move
					expr 
					'R0
				)
				(list
					'push 
					'R0
				)
			)
		)
		(:VAR
			
		)
		(:SET-VAR 
			
		)
		(:IF 
			
		)
		(:CALL 
			(case (cadr expr)
				(+
					(plus (cddr expr) param)
				)
				(-
					(moins (cddr expr) param)
				)
				(*
					(mult (cddr expr) param)
				)
				(/
					(div (cddr expr) param)
				)
				(t
					(warn "li2asm : ~s parametre du call inconnu" (cadr expr))
				)
			)
		)
		(:MCALL 
			
		)
		(:UNKNOWN 
			
		)
		(:LET
			
        )
		(:progn 
      		
  		) 
		((t)
			(warn "li2asm : ~s n'est pas dans le ASM" (car expr))
		)
	)
)

(defun plus (expr param)
	(cons
		(cons
		(li2asm (car expr) param)
		(li2asm (cadr expr) param)
		)
		(list
			'(pop R0)
			'(pop R1)
			'(add R1 R0)
			'(push R0)
		)
	)
)

(defun moins (expr param)
	(list
		(li2asm (car expr) param)
		(li2asm (cadr expr) param)
		(list
			'(pop R0)
			'(pop R1)
			'(sub R1 R0)
			'(push R0)
		)
	)
)

(defun mult (expr param)
	(list
		(li2asm (car expr) param)
		(li2asm (cadr expr) param)
		(list
			'(pop R0)
			'(pop R1)
			'(mul R1 R0)
			'(push R0)
		)
	)
)

(defun div (expr param)
	(list
		(li2asm (car expr) param)
		(li2asm (cadr expr) param)
		(list
			'(pop R0)
			'(pop R1)
			'(div R1 R0)
			'(push R0)
		)
	)
)

(trace li2asm)

; (li2asm '(:call + (:lit . 1) (:lit . 2)) ())
; (li2asm '(:call + (:lit . 1) (:call + (:lit . 2) (:lit . 3))) ())
; (li2asm '(:call + (:call + (:lit . 1) (:lit . 2)) (:lit . 3)) ())
(li2asm '(:call + (:call + (:lit . 1) (:lit . 2)) (:call + (:lit . 3) (:lit . 4))) ())