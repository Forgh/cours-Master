(require "lisp2liV2.lisp")

(defun eval-li (expr env)
 	(case (car expr)
 		(:CONST (cdr expr))
 		(:LIT 
 			(cdr expr))
  
 		(:VAR
 			(aref env (- (cadr expr) 1)))

 		(:SET-VAR 
 			(setf 
 			(aref env (cadr expr)) 
 			(eval-li (cddr expr) env))
		)
		(:IF 
			(if (eval-li (second expr) env)
				(eval-li (third expr) env)
				(eval-li (fourth expr) env)
 			)
 		)
 		(:CALL 
 			;(if (eq (cadr expr) 'set-defun )
 			;	(apply (second expr) (eval-li (caddr expr) env) (cdddr expr))
 				(apply (second expr) (map-eval-li (cddr expr) env))
        	;)
		)
		(:MCALL 
			(let* ((fun (get-defun (second expr))) (args (map-eval-li (cddr expr) env)) (nbParam (cadr fun)))
			 	(eval-li (third fun) ; third fun -> cddr fun
		 			(make-env-evalLI nbParam args)
			 	)
			)

		)
 		(:UNKNOWN 
 			(let ((nexpr (lisp2li (second expr) (cddr expr))))
 				(if (eq (car nexpr) :UNKNOWN)
					(error "eval-li : ~s" expr)
 					(eval-li (displace expr nexpr) env)
 				)
 			)
		)
		(:progn 
      		(car (last (map-eval-li  (car expr) env)))
  		) 
		(t
			(warn "eval-li : ~s n'est pas dans le LI" (car expr))
		)
	)
)

(defun displace (c1 c2)
	(setf (car c1) (car c2))
	(setf (cdr c1) (cdr c2))
	c1
)

(defun make-env-evalLI (taille liste)
	 (let ((tab (make-array taille)))
        (loop for x from 0 to (- taille 1)
            do
            (setf (aref tab x) (nth x liste))
        )
        tab
    )
)

(defun map-eval-li (lexpr env)
	(if (atom lexpr)
		()
		(cons (eval-li (car lexpr) env) (map-eval-li (cdr lexpr) env))
	)
)

;(eval-li (lisp2li '(defun fact (x) (if (<= x 0) 1 (* x (fact (- x 1))))) ()) ())
;(print(eval-li (lisp2li '(fact 50) ()) ()))