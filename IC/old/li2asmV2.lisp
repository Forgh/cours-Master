(load "eval-li.lisp")

(defun map-li2svm(lexpr)
        (if (null lexpr)
                ()
                (append (li2svm (car lexpr)) (map-li2svm (cdr lexpr)))
        )
)

(defun map-li2svm-progn (lexpr)
    (if (atom (cdr lexpr))
        (li2svm (car lexpr))
        (progn 
            (li2svm (car lexpr))
            (map-li2svm-progn (cdr lexpr))
        )
    )
)

(defun li2svm (expr)
        (ecase (car expr)

                ; littéral : on retourne (:LIT valeur)
                (:LIT (list expr))

                (:VAR (list expr))
                
                (:CVAR (list expr))
               
                (:SET-VAR (append (li2svm (caddr expr)) (list (cons ':SET-VAR (cadr expr)))))

                (:SET-CVAR (append (li2svm (caddr expr)) (list (cons ':SET-CVAR (cadr expr)))))

                (:IF (append 
                        (li2svm (cadr expr))    ;condition
                        (let ((cond1 (li2svm (caddr expr))))
                            (append 
                                (list (cons ':SKIPNIL (+ 1 (length cond1))))
                                cond1      ; si cond  vérifiée
                            )
                        )
                        (let ((cond2 (li2svm (cadddr expr))))
                            (append
                                (list (cons ':SKIP (length cond2)))
                                cond2
                            )
                        )             
                    )
                )

                (:CALL  
                    (if (eq (cadr expr) 'set-defun)
                        (append
                           (list (cons :LABEL (cdaddr expr)))
                           (li2svm (cadddr expr))
                        )

                        (append 
                            (map-li2svm (cddr expr))                ; traduction liste des paramètres
                            (list (cons ':LIT (- (length expr) 2)))
                            (list (cons ':CALL (cadr expr)))
                        )
                    )

                )
                

                (:MCALL (let ((fun (get-defun (second expr))))
                            (append
                                (map-li2svm (cddr expr))        ; traduction liste des paramètres
                                (list (cons ':CALL (cadr expr)))
                            )
                        )
                )

                (:UNKNOWN (let ((nexpr (lisp2li (second expr) (third expr))))
                                (if (eq :UNKNOWN (car nexpr))
                                    (error "fonction inconnue ~s" (second expr))
                                    (li2svm (displace expr nexpr))
                                )
                            )
                )
        )
)



