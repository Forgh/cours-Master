(defun get-defun (symb)
  (get symb :defun))

(defun set-defun (symb expr-lambda)
  (setf (get symb :defun)
	expr-lambda))

(defun lisp2li (expr env)
  (if (atom expr)
      (if (constantp expr)
	  `(:LIT .,expr)
	(if (member expr env)
	   `(:VAR .,(+ (position expr env) 1))
	  (progn (warn "~s n'est pas une variable de l'environnement" expr) 0)))
      (let ((fun (first expr))
	    (args (rest expr)))
	(if (consp fun)
	   ;cas lambda-expr (pas encore traité
	    (if (eq 'lambda (first fun))
		();;`(:LAMBDA, (length (second fun)), (lisp2li (third fun) env))
	      (progn  (warn "~s n'est pas une lambda-fonction" fun) 0))
	  (if (not (symbolp fun))
	      (progn (warn "~s n'est pas un symbole" fun) 0)
	    (if (get-defun fun)
		;cas MCALL
		`(:MCALL ,fun .,(map-lisp2li args env))
	      (if (not(fboundp fun))
		   ;cas unknown
		  `(:UNKNOWN ,expr .,env)
		(case fun
		      (if `(:IF ,(lisp2li (first args) env) 
				,(lisp2li (second args) env)
				.,(lisp2li (third args) env)))
		      (quote `(:LIT .,(first args)))
		      (defun `(:CALL set-defun 
			      (:LIT .,(first args))
			      (:LIT :LAMBDA 
				 ,(length (second args))
				 .,(lisp2li (third args) (second args)))))
		      (setf `(:SET-VAR ,(+ (position (first args) env) 1) .,(lisp2li (second args) env)))
		      ;(let) pas encore traité
		      (progn `(:PROGN .,(map-lisp2li args env)))
		      (t (if (macro-function fun)
			     (lisp2li (macroexpand-1 expr) env)
			   (if (special-form-p fun)
			       (progn (warn "~s n'est pas traitée" fun) 0)
			     `(:CALL, fun .,(map-lisp2li args env)))))))))))))

(defun map-lisp2li (expr env)
  (if (atom expr)
      ()
    (cons (lisp2li (car expr) env) (map-lisp2li (cdr expr) env))))

(trace lisp2li)
(trace map-lisp2li)

;;(lisp2li '(defun add (x) (+ x x)) '())
;;(lisp2li '(defun fact2 (n) (if (= n 0) 1 (* n (fact2 (- n 1))))) '())
;;(lisp2li '(defun test (x y) (* x y)) '())
;;(lisp2li ''(1 2 3) ())
;;(lisp2li '(defun test2 (x y) (if (> x 0) y (setf y 1))) '())
;;(lisp2li '((lambda (x y) (* x y)) 4 5) '(x y))

(defun construct-array (array indiceDeb args)
  (when args
    (setf (aref array indiceDeb) (car args))
    (construct-array array (+ 1 indiceDeb) (cdr args)))
  array)

(defun make-env-eval-LI (taille args)
 (if (atom args)
     ()
   (construct-array (make-array (+ taille 1)) 1 args)))

(trace construct-array)
(trace make-env-eval-LI)


(defun eval-LI (expr env)
  (ecase (car expr)
	 (:LIT (cdr expr))
	 (:VAR (aref env (cdr expr)))
	 (:SET-VAR (setf (aref env (cadr expr)) (eval-LI (cddr expr) env)))
	 (:IF (if (eval-LI (second expr) env) 
		  (eval-LI (third expr) env) 
		(eval-LI (cdddr expr) env)))
	 (:CALL (apply (second expr) (map-eval-LI (cddr expr) env)))
	 (:MCALL (let ((fun (get-defun (second expr)))
		       (args (map-eval-LI (cddr expr) env)))
		   (eval-LI (cddr fun) (make-env-eval-LI (second fun) args))))
	 (:UNKNOWN (let ((nexpr (lisp2li (second expr) (cddr expr))))
		     (if (eq (car nexpr) :UNKNOWN)
			 (error "eval-LI : ~s" expr)
		       (eval-LI (displace expr nexpr) env))))
	 (:PROGN (map-eval-LI-PROGN (cdr expr)))))

(defun map-eval-LI (expr env)
  (if (atom expr)
      ()
    (cons (eval-LI (car expr) env) (map-eval-LI (cdr expr) env))))

(trace eval-LI)
(trace map-eval-LI)
;; (trace make-env-eval-LI)
;; (trace map-eval-LI-PROGN)

;; (defun fibo (n) (if (<= n 1) n (+ (fibo (- n 1)) (fibo (- n 2)))))
;; (trace fibo)

;; (fibo 4)

;;test set-var:
;;(eval-LI (lisp2li '(if (< a b) (setf a b) (setf b a)) '(a b)) #(nil 10 20)) 
;;test lit:
;; (eval-LI (lisp2li ''(1 2 3) '()) '())

;;(lisp2li '(defun add (x y) (+ x y)) '())
;;(eval-LI (lisp2li '(defun add (x) (+ x x)) '()) '())
;; (eval-LI (lisp2li 'a '(b a)) '(b a))

;;test-defun:
;;(trace fibo)
;;(eval-LI (lisp2li '(defun fibo (n) (if (<= n 1) n (+ (fibo (- n 1)) (fibo (- n 2))))) '()) #(nil 4))
;;(eval-LI (lisp2li '(fibo n) '(n)) #(nil 4))
(eval-LI (lisp2li '(defun add (x) (+ x 1)) '(x)) #(nil 4))
(eval-LI (lisp2li '(add x) '(x)) #(nil 4))
;;(eval-LI (lisp2li '(defun fibo2 (n) (if (<= n 1) n (+ (fibo2 (- n 1)) (fibo2 (- n 2))))) '()) #(nil 4))
;;(eval-LI (lisp2li '(fibo n c) '(n c)) #(nil 1 2))

;; (eval-LI (lisp2li '(defun fact (n) (if (= n 0) 1 (* n (fact (- n 1))))) '()) '())
;; (eval-LI (lisp2li '(if (eq 1 1) 1 0) '()) '())

;; (lisp2li '(defun fact (n) (if (= n 0) 1 (* n (fact (- n 1))))) '())

  
