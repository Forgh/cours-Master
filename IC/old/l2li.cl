(defun lisp2li (expr env)
  ;si expr est un atome
  (if (atom expr)
    ;alors on teste si c'est une constante
    (if (constantp expr)
      (cons :lit expr)
      ;ou une variable de l'environnment
      (let ((pos (position expr env)))
        (if pos
          (list :var (+ 1 pos)))))
    ;si expr n'est pas un atome
    (let ((fun (car expr)) (args (cdr expr)))
      (cond
        ;cas du defun
        ((eq 'defun fun)
          (setf (get (first args) :defun)
            (list
	     :call 'set_defun
              (cons :lit (first args))
	      (list
	      :lambda
	      (length (second args))
              (lisp2li (third args) (second args))))))
	;cas du setf : affectation de variable
        ((eq 'setf fun)
          (set-defun expr env))
	;cas des macro-function
	((macro-function fun)
	 (lisp2li (macroexpand-1 expr) env))
	;cas de quote
        ((eq 'quote fun)
          (list :lit (first args)))
	;cas de if
        ((eq 'if fun)
          (cons
            :if
            (map-lisp2li args env)))
	;cas de progn
        ((eq 'progn fun)
          (list
            :progn
            (map-lisp2li args env)))
	;cas de let : declaration de variable (+ affectation)

        ;gestion du reste des appels de fonctions
        ((not (null (get-defun fun)))
          ;fonction utilisateur definie dans l'environnement
          (list
            :mcall
            fun
            (map-lisp2li args env)))
        ((not (fboundp fun))
          ;fonction inconnue ou premiere apparition de la fonction dans l'environnement
          (list
            :unknown
	    (cons :lit fun)
            (map-lisp2li args env) env))
        (t
          ;fonction definie dans clisp
          (list* :CALL fun
            (map-lisp2li (cdr expr) env)))
            ))))



;rien de special
(defun map-lisp2li (lexpr env)
  (if (atom lexpr)
    ()
    (cons
      (lisp2li (first lexpr) env)
      (map-lisp2li (rest lexpr) env))))

;verifie si fun est associe a quelque chose dans l'environnement
(defun get-defun (fun)
  (get fun :defun))

;redefinition du set
(defun set-defun (expr env)
  (if (symbolp (second expr))
      (list
       :set-var
       (lisp2li (second expr) env)
       (lisp2li (third expr) env))
      (list
       :setf
       (lisp2li (second expr) env))))
