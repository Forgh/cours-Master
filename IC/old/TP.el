(defun s_max (a b)
  (if (> a b) a b))

;;==> le nombre d'expression evaluable dans cette fonction est entre 4 et 5. (d'apres le prof).
;; les expressions evaluable sont : ">" "a" "b" dans le if
;;									"a" et "b"
;;									ainsi que "defun s_maw (a b)"
 
;;;;;;;;
;;fboundp -> true si c'est une fonction, defun, if, cond, ...
;;		     false si c'est un une variable, une fonction non definie ou autre chose.
;;SPECIAL-FORM-P -> true c'est un if, cond, case, ...
;;					false sinon



(defun parcours-lisp1 (exp env)
	(if (atom exp)
		(if (constantp exp)
			(progn  (format t "~a est une constante" exp)
					1)
			(progn (format t "~a n'est une constante" exp)
				(if (s_member exp env)
					exp
					(warn "~s n'est pas une variable" exp)
					)
				)
			)
		
		(progn 
			(format t "~a n'est pas un atome" exp)
			(let ((fun  (car exp))
			  	  (args (cdr exp)))

				(if (consp fun)
					(if (eq 'lambda (first fun))
						(progn 
							(format t "~a lambda" fun)
							(+ (parcours-lisp1 (first fun) env) (parcours-lisp1 args env))
						)
						(+ (parcours-lisp1 fun env) (parcours-lisp1 args env))
					)
					


					(progn
						(format t "~a n'est pas un cons" fun)
						(if (not (listp fun))		;listp is true if its argument is a cons or the empty list ()
							(if (fboundp fun) 		;fboundp is true if the symbol has a global function definition
								(progn 
									(format t "~a est une fonction" fun)
									(parcours-lisp1 args env))
								(progn
									(format t "~a n'est pas une fonction" fun)
									(if (SPECIAL-FORM-P fun)  						; verifie si fun est une special form : "if" ...
										(+ 1 (parcours-lisp1 args env))
										(progn 
											(format t "~a n'est pas un mot cle" fun)
											(parcours-lisp1 args env)
										)
									)
								)
							)
							(+ (parcours-lisp1 fun env) (parcours-lisp1 args env))
						)
					)
				)
			)
		)
	)
)

#|
	fonction: (s_member <exp> <env>)
	brief: 
		fonction qui me dit si un element exp se 
		trouve dans env et retourne la valeur de exp
|#
(defun s_member (exp env)
	(if (eql () env)
		()
		(if (eql exp (caar env))
			(cdar env)
			(s_member exp (cdr env))
		)
	)
)
;;;;(print (s_member 'hte '((c . 25)(G . 2)(b . 23)(b . 23)(b . 23)(b . 23)(a . 42) (b . 23)(b . 23)(b . 23)(b . 23)(b . 23)(b . 23))))
;;;;==> 23

;(print (parcours-lisp1 '(defun soka (e) e) 1))
;(print (parcours-lisp1 '(defun soka (e) e) (list (cons 'e 42)) ))
;(print (parcours-lisp1 '(s_max a b) (list (cons 'a 42) (cons 'b 23)) ))
;(print (parcours-lisp1 'val   (list (cons 'val 42) ) ))
;(print (parcours-lisp1 '(defun s_max (a b) (if (> a b) a b)) (list (cons 'a 42) (cons 'b 23)) ))
;(print (parcours-lisp1 '((lambda (a) a) 42) ))
;(lisp2li '(defun s_max (a b) (if (> a b) a b)) ())