(defun parcours-lisp1 (expr env) ;environnement : ensemble de variables et + si affinités
	(if (atom expr)
		(if (constantp expr)
			1
			(if (member expr env :test #'eql :key #'car) ;vérifier que le symbole a été déclaré comme une variable 
				1
				(warn "~s n'est pas une variable" expr)
			) 	; dans toutes les fonctions et leurs appels 
		)		; parcours-lisp*
		
		(if (consp (car expr))				;(let (fun (first expr) (args (rest expr))))
			(if (eq 'lambda (caar expr))
				
					(map-parcours-lisp1 (cdr expr) env)
					(map-parcours-lisp1 (cddar expr) env)
				
				
				;(warn "une expression évaluable ne commence pas par (( : ~s" expr)

			)
		)
	;(warn "~s pas atom" expr)
	)			; map-parcours-lisp*

)				; rajouter le paramètre d'environnement


(defun map-parcours-lisp1 (expr env)
	(if (atom expr)
		0
		(+ (parcours-lisp1 (car expr) env) (parcours-lisp1 (cdr expr) env))
	)
)
