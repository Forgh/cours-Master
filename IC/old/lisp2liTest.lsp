(defun lisp2li (expr env)
  (if (atom expr)
      (if (constantp expr)
	  `(:LIT .,expr)
	(if (member expr env)
	   `(:VAR .,(+ (position expr env) 1))
	  (progn (warn "~s n'est pas une variable de l'environnement" expr) 0)))
      (let ((fun (first expr))
	    (args (rest expr)))
	(if (consp fun)
	   ;cas lambda-expr (pas encore traité
	    (if (eq 'lambda (first fun))
		();;`(:LAMBDA, (length (second fun)), (lisp2li (third fun) env))
	      (progn  (warn "~s n'est pas une lambda-fonction" fun) 0))
	  (if (not (symbolp fun))
	      (progn (warn "~s n'est pas un symbole" fun) 0)
	    (if (get-defun fun)
		;cas MCALL
		`(:MCALL ,fun .,(map-lisp2li args env))
	      (if (not(fboundp fun))
		   ;cas unknown
		  `(:UNKNOWN ,expr .,env)
		(case fun
		      (if `(:IF ,(lisp2li (first args) env) 
				,(lisp2li (second args) env)
				.,(lisp2li (third args) env)))
		      (quote `(:LIT .,(first args)))
		      (defun `(:CALL set-defun 
			      (:LIT .,(first args))
			      (:LIT :LAMBDA 
				 ,(length (second args))
				 .,(lisp2li (third args) (second args)))))
		      (setf `(:SET-VAR ,(+ (position (first args) env) 1) .,(lisp2li (second args) env)))
		      ;(let) pas encore traité
		      (progn `(:PROGN .,(map-lisp2li args env)))
		      (t (if (macro-function fun)
			     (lisp2li (macroexpand-1 expr) env)
			   (if (special-form-p fun)
			       (progn (warn "~s n'est pas traitée" fun) 0)
			     `(:CALL, fun .,(map-lisp2li args env)))))))))))))


;rien de special
(defun map-lisp2li (lexpr env)
  (if (atom lexpr)
    lexpr
    (cons
      (lisp2li (car lexpr) env)
      (map-lisp2li (cdr lexpr) env))))

;verifie si fun est associe a quelque chose dans l'environnement
(defun get-defun (fun)
  (get fun :defun))

;redefinition du set
(defun set-defun (fun expr)
  (setf (get fun :defun) expr)
  (warn "set defun : ~s" fun)
)

(defun letlisp2li (lexpr env)
  ;(print env)
  ;(print lexpr)
  (let* ((expr (car lexpr)) (pos (position (car expr) env)))
  (if (atom lexpr)
    ()
    (cons
      `(:let
        ,(+ 1 pos)
        .,(lisp2li (cadr expr) env))
        ;(list (car expr) (cadr expr)))
      (letlisp2li (cdr lexpr) env)))))

(defun addtoenv (lexpr env)
  ;(print :addtoenv)
  ;(print lexpr)
  (let ((expr (car lexpr)))
          (if (atom lexpr)
            env
            (addtoenv (cdr lexpr) (append env (list (car expr)))))))

;(eval-li (lisp2li '(defun fact (x) (if (= x 0) 1 (* x (fact (- x 1))))) ())())
