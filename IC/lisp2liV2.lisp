(defun lisp2li (expr env)
  ;si expr est un atome
  (if (atom expr)
    ;alors on teste si c'est une constante
    (if (constantp expr)
      (cons :lit expr)
      ;ou une variable de l'environnment
      (let ((pos (position expr env)))
        (if pos
          (list :var (+ 1 pos))
          (list :inc expr))))
    
    ;si expr n'est pas un atome
    (let ((fun (car expr)) (args (cdr expr)))
      (cond
        ;cas du defun
        ((eq 'defun fun)
          ;(setf (get (first args) :defun)
            (list :call 'set-defun
              (cons :lit (first args))
              (cons 
                :lit
                (list 
                  :lambda
                  (length (second args)) 
                  (lisp2li (third args) (second args)))));)
        )
        
        ;cas du setf : affectation de variable
        ((eq 'setf fun)
          (set-defun expr env))
        
        ;cas de quote
        ((eq 'quote fun)
          (list :lit (first args)))
        
        ;cas de if
        ((eq 'if fun)
          (cons
            :if
            (map-lisp2li args env)))
        
        ;cas de progn
        ((eq 'progn fun)
          (list
            :progn
            (map-lisp2li args env)))

        ;cas de let : declaration de variable (+ affectation)
        ((eq (car expr) 'let)
					   (list :LET                   
							;nbParams
							(length (cadr expr))
							;corps
							(append 
								;variables (:set-var (1 . 1) (:LIT . 2)) par exemple
								(lisp2li 
									(cons 'progn (map-let (cadr expr)))														
									(append 
										(if (null env)
											'((()))
											env)                                  
										(list (lvarlocales (cadr expr)))))
								
								;corps (identique sans le progn implicite)
								(map-lisp2li 
									(cddr expr)
									(append 
										(if (null env)
											'((()))
											env)
										(list (lvarlocales (cadr expr))))))))

		;cas du loop
		((eq (car expr) 'loop)
			(if (eq (cadr expr) 'while)
				; loop while
				(list :LOOP (lisp2li (third expr) env) (lisp2li (fifth expr) env))
			)
		)
		
        ;cas des macro-function
        ((macro-function fun)
          (lisp2li (macroexpand-1 expr) env))

		
        ;gestion du reste des appels de fonctions
        ((not (null (get-defun fun)))
          ;fonction utilisateur definie dans l'environnement
          (list*
            :mcall
            fun
            (map-lisp2li args env)))
        ((not (fboundp fun))
          ;fonction inconnue ou premiere apparition de la fonction dans l'environnement
          `(:UNKNOWN ,expr .,env))
        (t
          ;fonction definie dans clisp
          (list* :CALL fun
            (map-lisp2li (cdr expr) env)))
            ))))



;rien de special
(defun map-lisp2li (lexpr env)
  (if (atom lexpr)
    lexpr
    (cons
      (lisp2li (car lexpr) env)
      (map-lisp2li (cdr lexpr) env))))

;verifie si fun est associe a quelque chose dans l'environnement
(defun get-defun (fun)
  (get fun :defun))

;redefinition du set
(defun set-defun (fun expr)
  (setf (get fun :defun) expr)
)

(defun letlisp2li (lexpr env)
  (let* ((expr (car lexpr)) (pos (position (car expr) env)))
  (if (atom lexpr)
    ()
    (cons
      (list 
        :let
        (+ 1 pos)
        (lisp2li (cadr expr) env))
        ;(list (car expr) (cadr expr)))
      (letlisp2li (cdr lexpr) env)))))

(defun addtoenv (lexpr env)
  (let ((expr (car lexpr)))
          (if (atom lexpr)
            env
            (addtoenv (cdr lexpr) (append env (list (car expr)))))))

(defun lvarlocales (liste)
    (if (atom liste)
        ()
        (cons (caar liste) (lvarlocales (cdr liste)))
    )
)
(defun map-let (lvars)
    (if (atom lvars)
        ()
        (cons `(setf ,(caar lvars) ,(cadar lvars))
                (map-let (cdr lvars))
        )
    )
)

;(print (lisp2li '(defun fact (x) (if (= x 0) 1 (* x (fact (- x 1))))) ()))

