(defun fact (n)
	(if (< n 1)
		1
		(* n (fact(- n 1)))
	)
) 

(defun fibo (n)	(if (< n 2)	1 (+ (fibo (- n 1)) (fibo (- n 2)))))

;4

(defun myMember (x l)
	(if (eq nil l)
		nil
		(if(= x (first l))
			l
			(myMember x (rest l))
		) 
	)
)

(defun myLength (l)
	(if (eq nil l)
		0
		(+ 1 (myLength (rest l)))
	)
)

(defun mylast (l)
	(if (atom (rest l))
		l
		(last (rest l))
	)
)

(defun copylist-rt (l r)
	(if (atom l)
		r
		(copylist-rt (rest l) (cons (first l) r))
	)
)

(defun makelistDesc (n)
	(if (eql n 0)
		nil
		(cons n (makelist (- n 1)))
	)
)

(defun makelistAsc (n)
	(copylist-rt (makelistDesc n) '()) 
)

(defun myappend (l lf)
	(if (eq nil l)
		lf
		(cons (first l) (append (rest l) lf))
	)
)

(defun size (tree)
	(if (atom tree)
		1
		(map-size-tree tree)
	)
)

(defun map-size-tree(tree)
	(if(atom tree)
		0
		(+(size(first tree))(map-size-tree(rest tree)))
	)
)


(defun my-copy-tree (tree)
	(if (atom tree)
		tree
		(mymapcopytree tree)
	)
)

(defun mymapcopytree (tree)
	(if(atom tree)
		nil
		(cons(my-copy-tree (first tree))(mymapcopytree(rest tree)))
	)
)

(defun subst-tree(x y tree)
	(if (atom tree)
		tree
		(map-subst-tree x y tree)
	)
)

(defun map-subst-tree (x y tree)
	(if (atom tree)
		nil

		(if (eql (first tree) x)
			(cons y (map-subst-tree x y (rest tree)))
			(cons (subst-tree x y (first tree)) (map-subst-tree x y (rest tree)))
		)
	)
)
	
(defun tree-leaves (tree)
	(if (atom tree)
		(cons tree (cons (tree-leaves (first tree)) (tree-leaves (rest tree))))
		(tree-leaves (rest tree))
	)
)
